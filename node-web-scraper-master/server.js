var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();
//var sqlite3 = require('sqlite3').verbose();
//var db = new sqlite3.Database('/Users/Jarvis/Documents/Projects/Vincubo/vincubo/db/development.sqlite3');
var geocoder = require('geocoder');

var pg = require("pg");
//var conString = "pg://postgres:123$%@54.187.57.74:5432/vincubo";
var conString = "pg://postgres:123@localhost:5432/vincubo";
var client = new pg.Client(conString);
client.connect();


function insertAdvert(title, published, price, description, urlImage, url, email, rooms, bathrooms, originalSource, negotationType, houseType, originalSite, location, phone){
	if (( price.toString().toLowerCase() == 'nan' || price == NaN  ))
		return false;


	try{
		geocoder.geocode(location, function ( err, data ) {
			
			try{
				var town = location.split(',')[0];
				var state = (location.split(',')[1]).replace(/^\s+|\s+$/g, '');

		  		var lat =  (Number(data.results[0].geometry.viewport.northeast.lat) + Number(data.results[0].geometry.viewport.southwest.lat))/2 ;
				var lng = (Number(data.results[0].geometry.viewport.northeast.lng) + Number(data.results[0].geometry.viewport.southwest.lng))/2 ;
				console.log("-");
				var townQuery = "select t.id from towns t inner join states s ON t.state_id = s.id  where t.name = '" +  town  +  "'  AND s.name = '" +  state +"';"
				var query = client.query(townQuery);
				console.log("City loaded");
				query.on("row", function (row, result) {
					console.log("Before load the adverts");
					var townid =  row["id"];
				    var insertStatement = "INSERT INTO adverts (originaldate, price, description, urlimage, rooms, bathroom, originalSource, negotationtype, housetype, originalsite, latitude, longitude, town_id, phone, ispublished, fromexcel, created_at )   VALUES ('" + published + "'," + price + ",'" + description + "','" + (urlImage == undefined ? "" : urlImage)   + "'," + ((rooms.toString() == "NaN") ? 0 : rooms)   + "," + bathrooms + ",'" + url + "','" + negotationType + "','" + houseType + "','" + originalSite + "'," + lat +"," + lng + "," + row["id"] +",'" + phone + "', false, false, '" + published +"' );";
				    console.log(insertStatement);
				    try{
				    var queryInsert = client.query(insertStatement);
					}catch(exce){
						console.log("no contenia precio");
					}
					try{
					    query.on("row", function (row2, result2) {
					    	console.log(result2);
					    });
					}catch(exce){
						console.log("no contenia precio");
					}
				});
			}catch(e){console.log(e);}

		});
	} catch(e){console.log("fail inserting");}
}




app.get('/scrape', function(req, res){

	var counter = 1;
	while(counter <= 250){
		var url = 'http://www.olx.com.sv/viviendas-locales-cat-16-p-' + counter;
		var adverts = [];

		request(url, function(error, response, html){
			if(!error){
				var $ = cheerio.load(html);

				var title, release, rating;
				
				var json = "";
				var $ = cheerio.load(html);

				var cells = $(".the-list .li h3 a");
				for(var i = 0; i< cells.length ; i++){
					var advertURL = $(cells)[i].attribs.href;
					var published;

					try{
						published = getDate($($(".fourth-column-container.table-cell")[i]).text());
					}catch(e){
						published = getDate("Hoy");
					}

					try{
						request(advertURL, function(error, response, xhtml){
							var tmpurl = advertURL;
							try{
								tmpurl = response.socket._httpMessage.path;
							}catch(e){

							}
							
							var newURL = "http://www.olx.com/sv" + tmpurl;
							
							
							console.log(">>Verificando: " + advertURL);
							var cuartos = 0;
							var banios = 0;

							try{
								var jQuery = cheerio.load(xhtml);


								for(var i=0; i< jQuery(".item-optionals .item-optional-label").length; i++){
									var x = jQuery(jQuery(".item-optionals .item-optional-label")[i])[0];

								  if( jQuery(jQuery(jQuery(jQuery(".item-optionals .item-optional-label")[i]))[0]).text() == "Cuartos:" )
								    cuartos =  jQuery(jQuery(jQuery(jQuery(".item-optionals .item-optional-value")[i]))[0]).text();
								}

								for(var i=0; i< jQuery(".item-optionals .item-optional-label").length; i++){
								  if( jQuery(jQuery(jQuery(jQuery(".item-optionals .item-optional-label")[i]))[0]).text() == "Cuartos de baño:" )
								    banios =  jQuery(jQuery(jQuery(jQuery(".item-optionals .item-optional-value")[i]))[0]).text()
								}
								var advert = {
									"title": (jQuery("#olx_item_title .h1").text()).replace(/^\s+|\s+$/g, '') ,
									"published": published,
									"location": (jQuery(".st_location").text()).replace(/^\s+|\s+$/g, ''),
									"price": parseToNumber( (jQuery(".item-highlights.price strong").text() == null || jQuery(".item-highlights.price strong").text() == ""  ) ? 0 : jQuery(".item-highlights.price strong").text()  ),
									"description": (jQuery("#description-text div").text()).replace("'","").replace('"','')  ,
									"urlImage": jQuery(".big-image img").attr("src"),
									"url": newURL,
									"email":   extractEmails(jQuery("#description-text div").text()),
									"originalSource": "OLX.com",
									"houseType": getHouseType( jQuery("#firstpath2").text() ),
									"negotationType": getNegotaionType( jQuery("#firstpath2").text() ),
									"rooms": parseToNumber( (cuartos == NaN || cuartos == undefined) ? 0 : cuartos ),
									"bathrooms": parseToNumber(  (banios == NaN || banios == undefined) ? 0 : banios   ),
									"phone": jQuery(jQuery(".item-data span")[1]).text(),
								}
								console.log("Procesando: " + advert.title + " - " + advert.url + " - En pagina: " + counter  );
								try{
									if(advert.price.toString() != 'NaN' && advert.price != NaN && advert.price > 0  && advert.bathrooms != NaN && advert.rooms != NaN)
										var z=0;
										insertAdvert(advert.title, advert.published, advert.price, 
											advert.description.replace(/["']/g, ""), advert.urlImage, 
											advert.url, advert.email, advert.rooms, advert.bathrooms, 
											advert.originalSource, advert.negotationType, advert.houseType, 
											advert.originalSource, advert.location, advert.phone);
								} catch(e){console.log(e);}
								//adverts.push(advert);
							} catch(e){console.log(e);}
						})
					}catch(ex){console.log(e)}
				}
			}

			
	        
		});
	//console.log("elementos enviados");
	counter+=1;
	}

	/*fs.writeFile('output.json', json, function(err){
	        	console.log('File successfully written! - Check your project directory for the output.json file');
	        	res.send('Check your console!')
	        })*/
	res.send('Check your console!');
});



function extractEmails (text)
{
	var result = text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
	return ((result == null) ? "" : result);
}

function extractPhone (text)
{
	var result = text.match(/([0-9]{4}-[0-9]{4})/gi);
	return ((result == null) ? "" : result);
}

function getNegotaionType(type){
	type = type.toLowerCase();

	if(type.indexOf("alquiler") > -1 )
		return "Alquiler";
	
	return "Venta";
}

function getDate(str){
	//console.log(str);
	if(str==undefined)
		return "";
	if(str.indexOf("Hoy") > -1  ){
		var date = new Date();
		return "" + (date.getMonth() + 1) + "-" + (date.getDate() + 1) + "-"  + (date.getYear() + 1900) + " " + " 00:00:00"
	} else if ( str.indexOf("Ayer") > -1 ){
		date = new Date();
		date.setDate(date.getMonth() - 1);
		return "" + (date.getMonth() + 1) + "-" + (date.getDate() + 1) + "-"  + (date.getYear() + 1900) + " " + " 00:00:00"		
	} else {
		if (str.split[1] == undefined)
			return "" + (date.getMonth() + 1) + "-" + (date.getDate() + 1) + "-"  + (date.getYear() + 1900) + " " + " 00:00:00"
		return Months(str.split(" ")[1]) + "-" + (str.split(" ")[0]) + "-" + str.split(" ")[2] + " 00:00:00";
	}
}

function Months(month){
	month = month.toLowerCase();
	if (month.indexOf("ene") > -1 )
		return 1;
	if (month.indexOf("febr") > -1 )
		return 2;
	if (month.indexOf("marz") > -1 )
		return 3;
	if (month.indexOf("abr") > -1 )
		return 4;
	if (month.indexOf("may") > -1 )
		return 5;
	if (month.indexOf("jun") > -1 )
		return 6;
	if (month.indexOf("jul") > -1 )
		return 7;
	if (month.indexOf("ago") > -1 )
		return 8;
	if (month.indexOf("sep") > -1 )
		return 9;
	if (month.indexOf("oct") > -1 )
		return 10;
	if (month.indexOf("nov") > -1 )
		return 11;
	if (month.indexOf("dic") > -1 )
		return 12;
}

function getHouseType(type){
	type = type.toLowerCase();
	if(type.indexOf("terreno") > -1 )
		return "Terreno";
	if(type.indexOf("apartamento") > -1 )
		return "Apartamento";
	if(type.indexOf("oficina") > -1 )
		return "Oficina/Clinica";
	if(type.indexOf("casa") > -1 )
		return "Casa";
	if(type.indexOf("bodega") > -1 )
		return "Bodega";
	if(type.indexOf("comercial") > -1 )
		return "Local Comercial";
	if(type.indexOf("recreo") > -1 )
		return "Casa de recreo";
	return type;
}

function parseToNumber(str){
	try{
		value = Number(str.replace("$","").replace(/ /g,''));
		return value;
	}catch(e){
		return 0;
	}
}



app.listen('8081')
console.log('Magic happens on port 8081');
exports = module.exports = app; 
