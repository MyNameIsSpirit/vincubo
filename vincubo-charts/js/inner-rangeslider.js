$(document).ready(function(){

					$("#rangePrices").noUiSlider({
						start: [ 0, 1080590 ],
						range: {
							'min': 0,
							'max': 1200000
						},
						connect: true,
						serialization: {
							lower: [
								$.Link({
									target: $("#value"),
									method: "text"
								}),
								$.Link({
									target: $(".inputClass"),
									format: {
										prefix: '\u0024',
										decimals: 0,
										postfix: ''
									}
								})
							],
							upper: [
								$.Link({
									target: "inputName"
								}),
								$.Link({
									target: $(".inputClass2"),
									format: {
										// Prefix the value with an Euro symbol
										prefix: '\u0024',
										// Write the value using 1 decimal.
										decimals: 0,
										postfix: ''
									}
								})
							],
							format: {
								decimals: 1
							}
						}
					});
					$("#rangeConstruction").noUiSlider({
						start: [ 0, 1652 ],
						range: {
							'min': 0,
							'max': 1800
						},
						connect: true,
						serialization: {
							lower: [
								$.Link({
									target: $("#value"),
									method: "text"
								}),
								$.Link({
									target: $(".inputClass3"),
									format: {
										prefix: '',
										decimals: 0,
										postfix: 'm2'
									}
								})
							],
							upper: [
								$.Link({
									target: "inputName"
								}),
								$.Link({
									target: $(".inputClass4"),
									format: {
										// Prefix the value with an Euro symbol
										prefix: '',
										// Write the value using 1 decimal.
										decimals: 0,
										postfix: 'm2'
									}
								})
							],
							format: {
								decimals: 1
							}
						}
					});
					$("#rangeTerreno").noUiSlider({
						start: [ 0, 2000 ],
						range: {
							'min': 0,
							'max': 2200
						},
						connect: true,
						serialization: {
							lower: [
								$.Link({
									target: $("#value"),
									method: "text"
								}),
								$.Link({
									target: $(".inputClass5"),
									format: {
										prefix: '',
										decimals: 0,
										postfix: 'm2'
									}
								})
							],
							upper: [
								$.Link({
									target: "inputName"
								}),
								$.Link({
									target: $(".inputClass6"),
									format: {
										// Prefix the value with an Euro symbol
										prefix: '',
										// Write the value using 1 decimal.
										decimals: 0,
										postfix: 'm2'
									}
								})
							],
							format: {
								decimals: 1
							}
						}
					});
					$("#rangeHab").noUiSlider({
						start: [ 4 ],
						connect: "lower",
						step: 1,
						range: {
							'min': 1,
							'max': 7
						},

					});
					$("#rangeBa").noUiSlider({
						start: [ 5 ],
						connect: "lower",
						step: 1,
						range: {
							'min': 1,
							'max': 10
						},

					});
					$("#rangePar").noUiSlider({
						start: [ 5 ],
						connect: "lower",
						step: 1,
						range: {
							'min': 1,
							'max': 10
						},

					});
				});