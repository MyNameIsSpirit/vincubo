var mueblesdata = [
				{
					value: 80,
					color:"#808080"
				},
				{
					value : 25,
					color : "#B3B3B3"
				},
				{
					value : 20,
					color : "#E6E6E6"
				},
				{
					value : 15,
					color : "#ffffff"
				}
			
			];
			var mueblesoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#808080",
				segmentStrokeWidth : 1,
			}
			var myDoughnut = new Chart(document.getElementById("muebles").getContext("2d")).Doughnut(mueblesdata,mueblesoptions);

			var propiedadesdata = [
				{
					value: 77,
					color:"#ffffff"
				},
				{
					value : 23,
					color : "#FF7F5E"
				}
			
			];
			var propiedadesoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#FF7F5E",
				segmentStrokeWidth : 1,
			}
			var myDoughnut = new Chart(document.getElementById("propiedades").getContext("2d")).Doughnut(propiedadesdata,propiedadesoptions);

			var zonasdata = [
				{
					value: 40,
					color:"#808080"
				},
				{
					value : 25,
					color : "#B3B3B3"
				},
				{
					value : 20,
					color : "#E6E6E6"
				},
				{
					value : 15,
					color : "#ffffff"
				}
			
			];
			var zonasoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#808080",
				segmentStrokeWidth : 1,

			}
			var myDoughnut = new Chart(document.getElementById("zonas").getContext("2d")).Doughnut(zonasdata,zonasoptions);

			var line2data = {
				labels : ["Ene.","Feb.","Mar.","Abr.","May.","Jun.","Jul.","Ago."],
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						data : [15,30,10,40,30,69,60,90]
					},
					{
						fillColor : "rgba(34,177,76,0.5)",
						strokeColor : "rgba(34,177,76,1)",
						pointColor : "rgba(34,177,76,1)",
						pointStrokeColor : "#fff",
						data : [70,42,62,48,62,20,38,28]
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(153,217,234,1)",
						pointColor : "rgba(153,217,234,1)",
						pointStrokeColor : "#fff",
						data : [32,10,40,12,55,28,50,65]
					}
				]
			}
			var line2options = {
				bezierCurve : false,
				datasetFill : false,
			}
				

			var linedata = {
				labels : ["Ene.","Feb.","Mar.","Abr.","May.","Jun.","Jul.","Ago."],
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						data : [15,30,10,40,30,69,60,90]
					}
				]
			}
			var lineoptions = {
				bezierCurve : false,
				datasetFill : false,
			}
				
				
				