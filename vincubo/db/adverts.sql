BEGIN TRANSACTION;
DROP TABLE IF EXISTS "adverts";
CREATE TABLE "adverts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "title" text, "features" text, "description" text, "areadescription" text, "street" text, "rooms" integer, "address" text, "local" text, "aditionaladdress" text, "negotationtype" text, "bathroom" integer, "price" integer, "housetype" text, "subtype" text, "originalSource" text, "urlImage" text, "originalSite" text, "originaldate" datetime, "image_file_name" varchar(255), "image_content_type" varchar(255), "image_file_size" integer, "image_file_updated_at" datetime, "areamts" float, "areavrs" float, "availability" integer, "parking" integer, "construction" integer, "areasize" float, "furnished" boolean, "pantry" boolean, "security" boolean, "garden" boolean, "nearbymalls" boolean, "kitchen" boolean, "preservation" boolean, "pool" boolean, "jacuzzi" boolean, "level" boolean, "basicservice" boolean, "cistern" boolean, "water" boolean, "heater" boolean, "air" boolean, "telephoneline" boolean, "visitorparking" boolean, "mezzanine" boolean, "recreation" boolean, "study" boolean, "terrace" boolean, "cellar" boolean, "parkland" boolean, "latitude" text, "longitude" text, "youtube1" text, "youtube2" text, "user_id" integer, "ispublished" boolean, "created_at" datetime, "updated_at" datetime, "town_id" integer);
INSERT INTO "adverts" VALUES(1,'Terreno Ideal Para Gasolinera',NULL,'Vendo terreno ideal para Gasolinera, tiene 16,321 V2, y se encuentra ubicado en el kilómetro 45 sobre la carretera panamericana, jurisdicción de la Villa de Santo Domingo, departamento de San Vicente.
Con Todos los permisos para Instalación de la Gasolinera, excepto el permiso de Medio Ambiente
Para mayor información con Karla de Bonilla al 2262-4820/ 7083-2671
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,850000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images04.olx-st.com/ui/9/19/33/1401817034_655031633_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70832671,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6405872','-88.7839214',NULL,NULL,NULL,'false',NULL,NULL,163);
INSERT INTO "adverts" VALUES(2,'!! vendo casa residencial rosalia remodelada en pasaje privado¡¡¡',NULL,'casa de una planta remodelada en pasaje cerrado,al costado izquierdo del cementerio de ayutuxtepeque, tres dormitorios un baño
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,36000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74374881,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7438577','-89.1992928',NULL,NULL,NULL,'false',NULL,NULL,101);
INSERT INTO "adverts" VALUES(3,'Amplia z/San Mateo y 49.av. $125mil',NULL,'3 dorm., ppal. C/baño',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,125000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images01.olx-st.com/ui/22/65/58/1401816190_655025758_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71098058,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(4,'Vendo hermosa casa en residencial buena vista 1, santa tecla. privado doblemente.',NULL,'

EXCELENTE PLUSVALIA,
Casa en Buena Vista 1 SANTA TECLA
Pje. Y Residencial privado
cochera techada para 2 vehiculos
2 salas,
3 banos,
3 habitaciones,
area de servicio completa
patio y jardin.
área de construcción es de 170 Mts
274 V2.
8 de frente.


PRECIO $111,000 NEGOC.


EXCELENTE PLUSVALIA,
Casa en Buena Vista 1 SANTA TECLA
Pje. Y Residencial privado
cochera techada para 2 vehiculos
2 salas,
3 banos,
3 habitaciones,
area de servicio completa
patio y jardin.
área de construcción es de 170 Mts
274 V2.
8 de frente.


PRECIO $111,000 NEGOC.',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,111000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images03.olx-st.com/ui/21/21/59/1401816978_655031259_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74505090,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(5,'Cuyultitan. vendo casa grande',NULL,'CUYULTITAN. Vendo casa grande nueva de 4 niveles más mirador.
Frente al km25 carretera antigua a Zacatecoluca, cerca de Olocuilta. Ideal para hotel, colegio, casa de retiros, hospital, etc.
3 accesos,
cochera para 8 vehículos,
amplios salones hasta 10 dormitorios,
a 20 minutos de San Salvador o el Aeropuerto de Comalapa, área verde,
amplio patio,
pisos cerámicos
área de servicios completa e independiente.
Tel. 2221-0084, 7030-4047 y 7001-3535 PRECIO $275,000 NEG.
',NULL,NULL,10,NULL,NULL,NULL,'Venta',0,275000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images04.olx-st.com/ui/22/83/83/1401816523_655028083_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70013535,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.54949535','-89.0943682',NULL,NULL,NULL,'false',NULL,NULL,133);
INSERT INTO "adverts" VALUES(6,'Ganga, Casa en Arcos de Santa Elena',NULL,'Ganga, Vendo casa Arcos de Santa Elena:
Privado con vigilancia las 24 horas.
Casa de dos niveles
Sala
Comedor
Cocina con pantrie
4 habitaciones
4 baños
Jardín
Area de servicio completa
Cochera techada para 3 vehículos.
250 mt2 de construcción
400 v2 de terreno
Precio $ 232,000 Neg.
INF. 7888-3886; 7210-1694
',NULL,NULL,4,NULL,NULL,NULL,'Venta',4,230000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/9/34/31/1401817140_652703331_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78883886,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(7,'Lotes urbanizados 3 formando un solo cuerpo',NULL,'VENDO 3 LOTES QUE FORMAN UN SOLO CUERPO EN
BRISAS DE ZARAGOZA, totalmente planos , con factibilidad
de instalar todos los servicios básicos, están por el punto de
los microbuses son 298 V2 $ 17,000 inf. al 7183-8649
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,17000,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71838649,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.587552','-89.2882218',NULL,NULL,NULL,'false',NULL,NULL,96);
INSERT INTO "adverts" VALUES(8,'Bonita casa en venta en Santa Tecla, San Antonio Las Palmeras',NULL,'Área de Terreno: 144
V2
Área de construcción:
125 Mts2
De dos Niveles
Descripción:
1er Nivel
•
Sala Amplia
•
baño social y compartido de las habitaciones
•
Habitación amplia
•
3 habitaciones Jr.
•
Comedor 
•
Cocina
•
Terraza

2do. Nivel
•
Habitación con su baño
•
Área de lavandería

•
OTROS: Pasaje privado con parqueo para un vehículo, piso cerámica
•
PRECIO DE VENTA:
$ 65,000

•
Sala Amplia•
baño social y compartido de las habitaciones•
Habitación amplia•
3 habitaciones Jr.•
Comedor •
Cocina•
Terraza

2do. Nivel
•
Habitación con su baño
•
Área de lavandería

•
OTROS: Pasaje privado con parqueo para un vehículo, piso cerámica
•
PRECIO DE VENTA:
$ 65,000
•
Habitación con su baño•
Área de lavandería
•
OTROS: Pasaje privado con parqueo para un vehículo, piso cerámica•
PRECIO DE VENTA:
$ 65,000',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,65000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images03.olx-st.com/ui/21/14/10/1401817000_655031410_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22838317,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(9,'Se Vende Rancho en Costa del Sol',NULL,'SE VENDE:
Rancho en Costa del Sol en Lotificacion Jardines del Pacifico. El Rancho esta ubicado en el kilometro 53 carretera a Costa del Sol.
- Precio de venta $170,000 negociable
- En muy buen estado rodeada de muchos arboles frutales
- Con vigilancia
- Casa de 2 niveles
- 3 dormitorios
- 3 Baños
- Piscina
- Amplia Area de Cocina con ceramica
- Amplia cochera techada para carros y lanchas
- Caseta de Vigilancia
- con todos los servicios de agua, luz, aguas negras.
Para mayor información comunicarse al cel 78620504
',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,170000,'Apartamento',NULL,'http://lalibertadcity.olx.com.sv/bonita-casa-en-playa-san-diego-iid-654979617','http://images04.olx-st.com/ui/21/78/30/1401726764_654507830_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78620504,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.5418708','-89.03788095',NULL,NULL,NULL,'false',NULL,NULL,148);
INSERT INTO "adverts" VALUES(10,'Bonita casa en Playa San Diego',NULL,'ÁREA DE TERRENO: 7,851 V2
CONSTRUCCIÓN: 606 M2
Casa de 1 planta
Sala
Comedor
Cocina con pantrie
4 dormitorios
Área de servicio
1 sanitario
3 terrazas
Otros
Paredes ladrillo de barro
Revestimiento: repellado, afinado y pintado con enchape en baños.
Cielo falso
Amplios jardines
Casa para cuidandero
Piscina para adultos / piscina para niños
Rancho en área de piscina
OTROS: La residencia se encuentra ubicada en 3ra
fila hacia el mar. Cuenta con servicios básico: energía eléctrica, agua potable y transporte público.

PRECIO DE VENTA: $175,000 Neg
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,175000,'Apartamento',NULL,'http://lalibertadcity.olx.com.sv/bonita-casa-en-playa-san-diego-iid-654979617','http://images01.olx-st.com/ui/9/03/17/1401809098_654979617_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22838317,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.49109765','-89.31703685',NULL,NULL,NULL,'false',NULL,NULL,75);
INSERT INTO "adverts" VALUES(11,'Zona Monumento A La Constitución, Privado',NULL,'Casa de Una sola Planta, en zona totalmente segura y tranquila, con seguridad las 24 horas, ademas la casa consta de:
160 metros cuadrados
Cochera para 1 vehiculo
Cisterna de 9 metros cubicos con bomba.
2 Habitaciones
1 baño compartido con ducha.
Habitación de servicio con su baño
Area de Lavanderia
Cocina
Sala amplia
comedor grande
2 patios
La habitacion principal con aire acondicionado
Precio Negociable...
Para Mayor Información: 2262-4820 y 70 83 26 71
Le Atendemos De Lunes a Domingo
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,110000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images03.olx-st.com/ui/22/89/10/1401816642_655028910_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70832671,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(12,'ciudad versalles, burdeos',NULL,'Vendo casa en Ciudad Versalles Burdeos con terreno extra, 11 metros de frente por 14 metros de largo, total 220 v2, consta de 2 dormitorios, 2 baño, cochera con porton, gran parte del patio construido como para taller o bodega, $ 37,000 o se vende el derecho en $ 13,000 más información al tel.: 7627-1762 o al 2530-7475
',NULL,NULL,2,NULL,NULL,NULL,'Venta',2,37000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/21/79/99/1401817854_655037099_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76271762,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(13,'Bonito Terreno con Casa de Campo en Carretera Panamericana',NULL,'Descripción:
Área del Terreno: 32,000 V2
Ubicado sobre Carretera Panamericana
Casa de Habitación
Tanque de captación de aguas lluvias
Electricidad
Totalmente forestado con arboles frutales y maderables
Calle interna
Vista panorámica al lago de Ilopango
OTROS: El terreno esta muy bien ubicado, ideal para casa de campo

PRECIO DE VENTA: $480,000
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,480000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images04.olx-st.com/ui/9/47/21/1401816024_655024621_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22838317,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7503845','-89.05757905',NULL,NULL,NULL,'false',NULL,NULL,111);
INSERT INTO "adverts" VALUES(14,'la casa de tus suenos',NULL,'venta
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,20000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78443644,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(15,'Casa en Alquiler en Nuevo Cuscatlán',NULL,'Terreno: 800 v2
Construcción: 400 m2

4 habitaciones / 4 baños

Estudio con baño completo

Amplia sala familiar en segunda planta

Cocina con top de cuarzo

Nueva, a estrenar.

Alquiler: $2,000

Informes al 7842-2880
',NULL,NULL,4,NULL,NULL,NULL,'Alquiler',4,2000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images04.olx-st.com/ui/22/33/22/1401815945_655023122_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78422880,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.649448','-89.2666627',NULL,NULL,NULL,'false',NULL,NULL,85);
INSERT INTO "adverts" VALUES(16,'La casa tiene 3 cuartos,un baño,sala,comedor lote con casa grande .',NULL,'Quiero cambiarme de departamento x eso nececito vender mi casa.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,35000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70048239,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71032485','-89.73001955',NULL,NULL,NULL,'false',NULL,NULL,26);
INSERT INTO "adverts" VALUES(17,'terreno altos de san ramon',NULL,'en altos de san Ramón buena ubicación excelente para una casa
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,9000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74911280,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(18,'alquilo recidencial en quintas carretera los planes quitas del bosque',NULL,'alguilo recidencial en quintas de 1000 varas para usted q le gusta vivir en medio de la naturaleza con 5 abitaciones 3 baños are de oficina estudio parqueo para 12 vehiculos muy bonita lugar carrtera alos planes de renderos recidencial quintas del bosque $750 tels 79316446-25179584
',NULL,NULL,5,NULL,NULL,NULL,'Alquiler',2,750,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images02.olx-st.com/ui/22/00/77/1401816706_655029377_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25179584,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(19,'Casa en pje. privado',NULL,'SE VENDE CASA EN URB. DOLORES IV, Mejicanos son
3 dormitorios, en 2 de ellos hay closet, encielado es nuevo,
cocina independiente, patio enrejado, baño con azulejos y
puerta de vidrio, verja al frente de hierro. primeras del pje.
y con portón privado. $ 30,000 INF. AL 7183-8649
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,30000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71838649,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(20,'Casa en alquiler en Santa Tecla',NULL,'Casa de 1 planta (Residencial privado con vigilancia las 24 horas)

3 Habitaciones
3 Baños
Cochera techada para 2 vehiculos,
Baño social,
Sala,
Comedor,
Terraza techada,
Jardin,
Cocina,
Area de laundry,
Cuarto de empleada con baño,
1 Habitación ppal. con w/c , a/c y baño,
2 Habitaciones junior
1 Baño completo (para habs. junior)
Cisterna.

Alquiler $ 850

Informes al 7842-2880
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',3,850,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images04.olx-st.com/ui/21/03/96/1401815312_655019796_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78422880,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(21,'Amplio apartamento en venta',NULL,'calle 5 de noviembre San Salvador
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,19899,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'7170 2416',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(22,'Venta casa grande 5 hab. 3 banos, garage para 2, $72000 neg',NULL,'EN VENTA CASA GRANDE Y AMPLIA DE DOS NIVELES $72,000.00 NEGOCIABLES. LLAME Y HAGA SU CITA PARA VERLA, !!!!!!!!!SE SORPRENDERA!!!!

UBICACION ADECUADA PARA PONER UN NEGOCIO Y ACCESIBLE A CENTROS COMERCIALES, CENTRO DE LA CIUDAD DE MEJICANOS Y ZACAMIL, PLAZA METROPOLIS, ESCUELAS PUBLICAS, COLEGIOS PRIVADOS, SELECTOS, DESPENSAS, WALMART, CANCHAS DE FUTBOL Y BASQUETBALL, ETC
',NULL,NULL,5,NULL,NULL,NULL,'Venta',3,72000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images02.olx-st.com/ui/9/56/43/1401817589_655035343_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78865435,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(23,'Vendo Casa en Residencial Utila, sobre Boulevard Sur.',NULL,'Vendo Casa en Residencial Utila
-Sobre Boulevard Sur
-Sala grande
-Estudio
-Comedor amplio
-Cocina con pantrie
-3 habitaciones la principal con baño y tina
-Sala familiar
-Terraza
-Area de lavado y tendedero
-Area de servicio completa
-4 baños en total
-Estacionamiento para 2 vehículos techado
-Fuente
-Jardín alrededor de la casa
-Cielo falso de madera
-Piso cerámico
-Bodega.
-360 mt2 de construcción
-891.75 v2 de terreno
-$300,000 Neg.
INF. 7210-1694; 7888-3886
',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,300000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/8/70/40/1400862737_644787040_1-vendo-casa-en-residencial-utila-sobre-boulevard-sur-utila.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(24,'Vendo casa de esquina de una sola planta, bonita y amplia',NULL,'Vendo casa de esquina de una sola planta, bonita y amplia, 116 Mts cuadrados, 3 habitaciones, sala amplia, comedor amplio, cocina, todo independiente, 2 baños, patio interno y externo, el externo enrrejado, Closet en 2 habitaciones, remodelada y pintada, piso ceramico, cielo falso, no tiene cochera pero tiene espacio para contruirle, todo lo tiene muy accesible e inmediato; precio $47,000 negociable,TAMBIEN PUEDEN APLICAR RESIDENTES EN LOS EE.UU QUE PUEDAN INVERTIR, dejar mensaje solo.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',2,4700,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images02.olx-st.com/ui/9/91/31/1401816655_655029031_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(25,'Cuyultitan. casas en venta',NULL,'CUYULTITAN. Casas en venta,
km 25 carretera antigua a Zacatecoluca, a 20 minutos de San Salvador o el Aeropuerto de Comalapa, cerca de Olocuilta. Área 64.39V²,
2 habitaciones,
corredor,
piso cerámico,
pasaje privado.
Precio $16,000.
Financiamiento disponible.
Tel. 2221-0084, 7030-4047 y 7001-3535
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,16000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images01.olx-st.com/ui/21/45/16/1401817370_655033916_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70013535,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.54949535','-89.0943682',NULL,NULL,NULL,'false',NULL,NULL,133);
INSERT INTO "adverts" VALUES(26,'casa  tipo quinta 1200 varas',NULL,'5 minutos de catedral
',NULL,NULL,5,NULL,NULL,NULL,'Venta',3,150,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','http://images02.olx-st.com/ui/22/58/11/1401808495_654975711_1-Pictures-of--casa-tipo-quinta-1200-varas.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74731555,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(27,'Terreno en zona alta de la escalon, 55526 vr2!!!',NULL,'
TERREN0 RURAL DE 55526 VR2
UBICADO A 10 MIN DEL REDONDEL MASFERRER
PUNTO DE REFERENCIA URBANIZ.VISTAS DEL CARMEN.
ZONA CON UN CLIMA FRESCO TODO EL AÑO.
TERRENO ARRIBA DE LA COTA MIL.
APTO PARA LA CONSTRUCCION DE UNA QUINTA
TERRENO CON UNA VISTA PRECIOSA A S.S.
UN SOLO DUEÑO
LIBRE DE GRAVAMEN
SE VENDE EN SU TOTALIDAD NO LOTES
PRECIO TOTAL $ 950.000
PRECIO NEGOCIABLE!!!
SE RECIBEN OFERTAS O SE PUEDE MOSTRAR EN HORAS DE OFICINA. TELS:78257359 61211270
LLAMAR SOLO INTERESADOS
URGE VENDER POR MOTIVO DE VIAJE!!

',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,950000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,61211270,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(28,'Gangaaa! Vendo al credito',NULL,'LOTIFICACION SAN ANTONIO, POLIG.M SONSONATE
PRECIO $ 64,197.00
MEDIDAS 600.94 V2
PARA MAYOR INF. 7877-6864 VICENTE CARBALLO
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,64197,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78776864,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71032485','-89.73001955',NULL,NULL,NULL,'false',NULL,NULL,26);
INSERT INTO "adverts" VALUES(29,'Vendo Lote con Casita Con Agua y Luz',NULL,'El Lote tiene 10 Metros de Ancho por 35 de Largo, tiene construida una Casita que al arreglarla que bonita y buen espacio para expender o sembrar, Tiene Luz y Agua, Lista de habitar en una Emergencia, URGE Vender
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,9500,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','http://images03.olx-st.com/ui/21/32/51/1401808094_654973151_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72162606,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7503845','-89.05757905',NULL,NULL,NULL,'false',NULL,NULL,111);
INSERT INTO "adverts" VALUES(30,'Remato Quinta Recreativa en Ciudad de Santa Ana.',NULL,'Remato Quinta Recreativa en Ciudad de Santa Ana, consta de:
-3 habitaciones.
-3 piscinas.
-Desvestideros.
-Duchas.
-Terraza.
-Cancha de futbol medidas oficiales.
-Palco.
-Chalet.
-Area total: 14,887.12 v2
Precio: $ 280,000 Negociables.
INF. 7210-1694 y 7888-3886.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,280000,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/8/52/29/1400862898_644784329_1-fotos-de-remato-quinta-recreativa-en-ciudad-de-santa-ana.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(31,'Vendo 2 Lotes en Residencial Florida, Nuevo Cuscatlán.',NULL,'Vendo 2 Lotes en Residencial Florida, Nuevo Cuscatlán.
-800 v2 cada uno.
-Precio: $165 v2.
-Complejo con amplias calles y avenidas.
-Plano.
-Gran plusvalía y excelente ubicación.
INF. 7210-1694 y 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,165,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images03.olx-st.com/ui/8/35/80/1400862928_644783580_1-vendo-2-lotes-en-residencial-florida-nuevo-cuscatlan-residencial-florida.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(32,'Doy en alquiler | alquilo un terreno en olocuilta',NULL,'Alquilo un Terreno en Olocuilta
Se alquila para, Parqueo, Taller, llanteria, Vivero, Granjas, o cualquier negocio que desee, ya que esta en un lugar especifico para todo negocio, el terreno tiene 1,600 vrz, esta Ubicado en Km 25 1/2 de Autopista a Comalapa, en La Colonia san Jose, Olocuilta La Paz, 2 kilometros abajo de gasolinera Montelimar, contiguo a Taller.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,450,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images01.olx-st.com/ui/22/90/24/1401815131_655018624_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,23105417,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.5700108','-89.1152572',NULL,NULL,NULL,'false',NULL,NULL,137);
INSERT INTO "adverts" VALUES(33,'Bonita y acogedora casa los sauces 1',NULL,'LOS SAUCES 1
SE VENDE
Casa 146.03 v2 / 102.06 M2 Descripción: * Espacio 2 vehiculos
*2 niveles *Sala *comedor *cocina con pantry *baño social completo *3 dormitorios con closet *1 baño para los dormitorios
La casa cuenta con Cisterna aunque el agua no falta en la zona.
Venta Precio $58,000.00
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,58000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','http://images04.olx-st.com/ui/9/01/95/1401813778_655009695_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78594828,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(34,'Vendo Quinta Recreativa en San Marcos, en Colonia El Milagro.',NULL,'Vendo Quinta Recreativa en San Marcos, en Colonia El Milagro, consta de 4 habitaciones la principal con baño y jacuzzi, estudio, amplia sala principal, sala circular, amplia cocina principal con pantrie, terraza techada, área de estar, piscina, kiosco, des vestideros y ducha en el área de la piscina, área de barbacoa, casa estilo hacienda con pasillos techados alrededor con bellos arcos y columnas, jardines amplios alrededor de la casa, encielado de madera, piso cerámico, estacionamiento techado además de amplio estacionamiento al aire libre, dormitorio de servidumbre, cocina secundaria para servidumbre, baños y área de comedor para servidumbre, área de planchado, área de lavar, tendedero, cisterna y equipo de bombeo y bodega. $1,200,000.00 Neg.
560.61 mt2 de construcción 5,208.26 v2 de terreno
INF. 7210-1694; 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,1200000,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images01.olx-st.com/ui/8/90/34/1400862706_644788634_1-fotos-de-vendo-quinta-recreativa-en-san-marcos-en-colonia-el-milagro.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6590186','-89.1731475',NULL,NULL,NULL,'false',NULL,NULL,110);
INSERT INTO "adverts" VALUES(35,'Amplia z/San Mateo y 49.av. $125mil inf. 71098058',NULL,'3 dorm., ppal. C/baño',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,125000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images03.olx-st.com/ui/22/20/77/1401815560_655021377_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71098058,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(36,'la casa que uste necesita',NULL,'negociable
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,20000,'Apartamento',NULL,'http://lalibertadcity.olx.com.sv/bonita-casa-en-playa-san-diego-iid-654979617','http://images02.olx-st.com/ui/22/51/02/1401811439_654994902_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78443644,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(37,'se alquila habitación',NULL,'se alquila habitacion en santa tecla! con su respectivo baño a
personas solteras responsables!
mayor informacion
76149729 ricardo alvarado
70226330 haydee alvarado
',NULL,NULL,1,NULL,NULL,NULL,'Alquiler',1,125,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','http://images01.olx-st.com/ui/21/57/03/1401808445_654975403_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76149729,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(38,'Apartamentos de Lujo en La Castellana.',NULL,'Apartamento de Lujo en La Castellana:
Espacios amplios, sala, comedor, cocina, 3 habitaciones, aire acondicionado, área de servicio completa, incluye línea blanca, 2 estacionamientos, bodega.
El condominio cuenta con: Canchas de Tenis, cancha de Básquet, Piscina, Gimansio, Jacuzzi, Area verde para grandes y pequeños, Track para correr o salir a caminar, Area social Techada para eventos.
196 mt2
Canon: $ 1,800 con mantenimiento y seguridad.
Inf. 7210-1694
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',3,1800,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/8/19/24/1400863064_644781524_1-apartamentos-de-lujo-en-la-castellana-la-castellana.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(39,'Ganga!! vendo casa al credito',NULL,'LOTIFICACION REY DAVID, FINAL AV. PRINCIPAL Y PASAJE 1, BLOCK 3 MUNICIPIO DE SANTA CRUZ MICHAPA, CUSCATLAN
MEDIDAS 467.22 V2
CARACTERISTICAS:
Inmueble de forma trapezoidal y de uso habitacional con un Área de construcción de 183.41 m2, consta de 5 dormitorios, corredor, Área de oficios, patio.
PARA MAYOR INF. 7877-6864 VICENTE CARBALLO
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,20840,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images01.olx-st.com/ui/21/42/12/1401815940_655024012_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78776864,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7396092','-88.9830822',NULL,NULL,NULL,'false',NULL,NULL,129);
INSERT INTO "adverts" VALUES(40,'El terreno es grande y bonito',NULL,'El terreno es de 4 manzanas como se ve en las fotos ya consta con una galera para lo que desee tiene agua,luz 3 cisternas, por que tenia proyecto para piscinas.Caceta de seguridad el terreno esta apropiado para centro de retiros o de recreo tiene nacimiento de agua.ETC
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,295000,'Terreno',NULL,'http://lalibertadcity.olx.com.sv/bonita-casa-en-playa-san-diego-iid-654979617','http://images04.olx-st.com/ui/21/90/67/1401810446_654988367_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,77853040,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7239289','-88.90331985',NULL,NULL,NULL,'false',NULL,NULL,118);
INSERT INTO "adverts" VALUES(41,'Alquilo 2 Bodegas sobre carretera a Lourdes.',NULL,'Alquilo 2 Bodegas sobre carretera a Lourdes:
-1,470 mt2 cada una.
-Piso industrial.
-1 muelle.
-2 baños.
-1 oficina.
-Precio: $ 4,500 cada una.
INF. 7210-169 y 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,4500,'Oficina/Clinica',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images01.olx-st.com/ui/13/36/47/1400862939_644782947_1-alquilo-2-bodegas-sobre-carretera-a-lourdes-lourdes.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(42,'Alquilo Casona Zona Parque de La Familia, Planes de Renderos.',NULL,'Alquilo Hermosa y Amplia Casa
Zona Parque de la Familia Planes de Renderos.
Placentera y preciosa vista al Oriente del pais.
Clima fresco, Seguro
Cuenta: 3 Niveles bien iluminados, ventilados.
Estudio, 3 habitaciones Gdes, 2 salas, comedor, A/servicio Completa, Cocina/Pantry, Cisterna, Cochera 6/8 vehiculos, 2 terrazas/vista
Area verde en terrazas
Valor $ 650.00 Neg.
',NULL,NULL,4,NULL,NULL,NULL,'Alquiler',3,650,'Apartamento',NULL,'http://lalibertadcity.olx.com.sv/bonita-casa-en-playa-san-diego-iid-654979617','http://images02.olx-st.com/ui/22/84/56/1401810366_654987856_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78290333,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.612006','-89.17784625',NULL,NULL,NULL,'false',NULL,NULL,97);
INSERT INTO "adverts" VALUES(43,'Se Vende Amplia Bodega',NULL,'Bodega en Venta, consta de:
550 metros de construccion (300 primera planta y 250 2da. planta)
Oficina en primera y segunda planta.
1 cuarto frio de 5 x 3.5 metros en la planta baja
1 cuarto frio de 4 x 3.5 metros en la planta alta
Parque para tres vehículos zona de carga y descarga.
Precio de Venta $120,000.00
Para mayor información: 2262-4820 y 70 83 26 71
Le Atendemos De Lunes a Domingo
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,120000,'Oficina/Clinica',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images04.olx-st.com/ui/9/04/04/1401816793_655030004_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70832671,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.75346915','-89.1586547',NULL,NULL,NULL,'false',NULL,NULL,103);
INSERT INTO "adverts" VALUES(44,'Apartamento',NULL,'Se vende apartamento en zona de Universidad Nacional. No intermediarios, $23,500 negociables. Información por correo electrónico.
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,23500,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Ubicación',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(45,'Se Vende Casa Amplia y Bonita',NULL,'Bonita casa en Venta en La Miralvalle, con seguridad las 24 horas, tiene ademas cuarto de servicio con su baño, Amplio Jardín en la parte Trasera, Jardin en la parte frontal, cochera para 2 vehículos, 457 Varas Cuadradas.
Para Mayor información: 2262-4820 y 70 83 26 71
Le Atendemos De Lunes a Domingo
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,140000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images01.olx-st.com/ui/21/71/96/1401816308_655026596_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70832671,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(46,'Amplia casa en Bosques de Santa Elena',NULL,'LInda Casa en Bosques de Santa Elena, seguridad las 24 horas, cochera para 4 vehiculos, sala comedor, teraza amplia, jardin amplio, cocina, area de servicio completa.
Segunda planta:
tres habitaciones, dos baños,
principal con walking closet.
Sala familiar.
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',3,1000,'Apartamento',NULL,'http://lalibertadcity.olx.com.sv/bonita-casa-en-playa-san-diego-iid-654979617','http://images04.olx-st.com/ui/21/19/02/1401809425_654981702_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78416698,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(47,'Vendo Casa frente a Villa Olímpica, por la Gloria, en privado.',NULL,'Vendo Casa por Residencia Villa Olímpica.
-Zona de La Gloria.
-Residencial Privado.
-Vigilancia 24 horas.
-Sala.
-Comedor.
-Cocina.
-3 habitaciones.
-Estudio.
-Baño.
-Patio.
-Cielo falso.
-Area de servicio.
-Amplio estacionamiento.
-100 mt2 de construcción.
-77.26 v2 de terreno.
-$57,000 Neg.
INF. 7210-1694; 7888-3886
',NULL,NULL,4,NULL,NULL,NULL,'Venta',1,57000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images01.olx-st.com/ui/8/54/70/1400862875_644785470_1-fotos-de-vendo-casa-frente-a-villa-olimpica-por-la-gloria-en-privado.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(48,'Ganga Vendo Casa contiguo a Centro comercial Unicentro Lourdes.',NULL,'Ganga Vendo Casa contiguo a Centro comercial Unicentro Lourdes, consta de:
-Sala.
-Comedor.
-Cocina.
-2 habitaciones.
-2 baños.
-Patio grande.
-Parqueo 1 vehículo.
-Con piso cerámico.
-110 mt2 de construcción.
-261 v2 de terreno
Precio: $35,000
INF. 7210-1694 y 7888-3886.
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,35000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images02.olx-st.com/ui/9/09/21/1401817497_644780821_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(49,'Vendo 4 Lotes contiguo a Centro comercial Unicentro Lourdes.',NULL,'Vendo 4 Lotes contiguo a Centro comercial Unicentro Lourdes, consta de:
-260 v2 cada uno.
-Calle pavimentada.
-Agua potable.
-Energía Eléctrica.
-Permiso de construcción.
-Precio $12,000 cada uno.
INF. 7210-1694 y 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,12000,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/8/28/89/1400863021_644781989_1-vendo-4-lotes-contiguo-a-centro-comercial-unicentro-lourdes-lourdes.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(50,'Lotificacion a la venta, con todos los permisos',NULL,'Por no poder atender, vendo lotificación legalizada ubicada en la Periferia de La Palma. Son casi 9 Mz de terreno y 32 lotes grandes de 2,000 V2 cada uno. Cerca del pueblo. Topografía inclinada. Acceso vehículo 4x4, Clima fresco, rodeado de pinos. Excelente vista. Los lotes son totalmente rústicos y las medidas del lote tipo son de 25 m de frente por 60 metros de largo. La escritura es de inmediato. Ideal para desarrollo de casas de campo. Precio $160,000.00 Neg. Mayor información al (503)7938-9141
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,160000,'Terreno',NULL,'http://sanmartin.olx.com.sv/ganga-vendo-terreno-con-financiamiento-25-629-v2-iid-654574692','http://images02.olx-st.com/ui/9/75/27/1401737816_654576827_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25140531,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'14.31570195','-89.16918045',NULL,NULL,NULL,'false',NULL,NULL,54);
INSERT INTO "adverts" VALUES(51,'Lotes planos, urbanos, sin prima.',NULL,'VENDO LOTES EN LOTIFICACION SANTA MARIA.

LOCALIZACIÓN: Departamento de San Salvador, Municipio de Ilopango, al costado Oeste de la Colonia San Bartolo TICSA- (A 200 mts. de la Calle de Oro)
¿Cómo LLEGAR?: Ruta de buses y microbuses 29, bajarse en parada del Colegio Jerusalén, luego caminar al Norte sobre Calle Primavera (la que se inicia donde está una llantería), seguir por la tercera entrada a la izquierda, y se llega a Lotificación. (Aproximadamente 150 mts. Al Nor-Oeste, entre el Colegio Jerusalén y la Lotificación Santa María).
FACTIBILIDAD DE
SERVICIOS: Energía eléctrica (primario y secundario), agua potable, transporte colectivo, centros educativos.

DIMENSIONES
DEL LOTE TIPO: Estos tienen 6 mts. de frente por 12 mts. de fondo, para un área total de 72 mts. equivalentes a 103.02 varas cuadradas.

CONDICIONES
FINANCIERAS: Letra corrida desde $ 85.00 hasta $150.00, a un plazo máximo de 20 años (puede optar por un plazo menor), con una tasa de intereses del 11% anual. Su valor de contado va desde $ 9,500 hasta $ 15,500.

CONTACTENOS: Concerte una cita con nosotros, a los números telefónicos: 77611594 y 22902214
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,9500,'Terreno',NULL,'http://sanmartin.olx.com.sv/ganga-vendo-terreno-con-financiamiento-25-629-v2-iid-654574692','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,77611594,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6902395','-89.111048',NULL,NULL,NULL,'false',NULL,NULL,106);
INSERT INTO "adverts" VALUES(52,'Valle Lourdes, Privado',NULL,'Vendo casa en Valle de Lourdes, en las primeras sendas, privado, 3 dormitorios, cochera techada, cielo falso, piso de cerámica, baño chapado, ventanas francesas, patio de atrás cerrado, $ 29,000 más información al tel.: 7627-1762 o al 2530-7475
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,29000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images02.olx-st.com/ui/21/58/56/1401817576_655035256_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76271762,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(53,'Ganga vendo casa en una sona segura de ticsa sanbartolo 12500 negosiable',NULL,'GANGA VENDO CASA EN UNA DE LAS SONAS MAS SEGURAS DE TICSA SANBARTOLO CON EL ACSESO MAS INMEDIATO A HOSPITAL ,CENTROS EDUCATIVOS Y TRANSPORTES COLECTIVOS 12500 NEGOSIABLE INTERESADOS LLAMAR AL TELEFONO 79929326 O AL 22950654
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,12500,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images02.olx-st.com/ui/22/71/35/1401749623_654626635_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79929326,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6902395','-89.111048',NULL,NULL,NULL,'false',NULL,NULL,106);
INSERT INTO "adverts" VALUES(54,'Magnifica Casa en Zona totalmente Privada y centrica de Santa Tecla, Buen Acceso.',NULL,'Casa con amplio jardín, 200V2 de terreno. Bonitos acabados y Excelentes condiciones.
2 veh grandes,
3 baños
área de serv con baño,
terraza techada
3 hab con closeth
todos los espacios independientes.
',NULL,NULL,4,NULL,NULL,NULL,'Venta',3,89000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images01.olx-st.com/ui/9/56/76/1401752026_654635076_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78515464,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(55,'Camino a Cinquera',NULL,'Suchitoto, Quezalapa carretera a Cinquera, hermoso terreno de 3 mz de terreno con rio de por medio
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,90000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','http://images04.olx-st.com/ui/22/71/01/1401744304_654607001_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70937711,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.93446275','-89.02395485',NULL,NULL,NULL,'false',NULL,NULL,130);
INSERT INTO "adverts" VALUES(56,'Casa ideal para remodelar en Col. Flor Blanca',NULL,'Áreas:
Terreno 398 v2
Construcción 200 m2
Casa de una planta ideal para oficina o negocio.
la casa cuenta con 12 ambientes y 3 baños.
Precio de venta $85, 000
Informes al 7842-2880
',NULL,NULL,0,NULL,NULL,NULL,'Venta',3,85000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images03.olx-st.com/ui/21/61/71/1401814743_655016071_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78422880,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(57,'ganga terreno de 44 manzanas',NULL,'Vendo terreno de 44 manzanas tiene nacimientos de agua y un pequeño rio 8000 x manzana precio negociable',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,352000,'Terreno',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70522968,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(58,'Ganga Vendo Terreno en Complejo La Ventana, en Playa El Cuco',NULL,'Ganga Vendo Terreno en Complejo La Ventana, en Playa El Cuco, consta de:
-Complejo Privado.
-Piscinas infinity de adultos y chicos.
-A corto plazo de Campo de Golf.
-Hoteles frente al mar.
-Academia de Golf.
-Hotel de Montaña.
-Bosques y zonas verdes.
-Senderos para cabalgar.
-Terreno de 1,250 v2 en total que incluye acceso solo para el terreno.
-Terreno en el que se puede construir es de 450 v2
-Precio $ 36,000 Negociables.
Inf. 7210-1694: 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,36000,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images04.olx-st.com/ui/8/67/98/1400863193_644775998_1-ganga-vendo-terreno-en-complejo-la-ventana-en-playa-el-cuco-el-cuco.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.4772755','-88.168583',NULL,NULL,NULL,'false',NULL,NULL,199);
INSERT INTO "adverts" VALUES(59,'Ganga vendo casa o derecho en una sona segura de ticsa sanbartolo 12500 y el derecho 8500',NULL,'GANGA VENDO CASA O DERECHO EN UNA DE LAS SONAS MAS SEGURAS DE TICSA SANBARTOLO Y CON EL ACSESO MAS DIRECTO A CENTROS EDUCATIVOS ,,HOSPITAL Y TRANSPORTES COLECTIVOS 12500 Y POR EL DERECHO 8500 EL DERECHO ES POR EL LOTE LA CASA ESTA LIKIDADA INTERESADOS LLAMAR AL TELEFONO 79929326 O AL 22950654
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,12500,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','http://images03.olx-st.com/ui/9/13/44/1401807750_654970944_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79929326,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6902395','-89.111048',NULL,NULL,NULL,'false',NULL,NULL,106);
INSERT INTO "adverts" VALUES(60,'SE ALQUILA, bodega sobre calle al Puerto de La Libertad de 1,650 mts2.',NULL,'1,650.00 mts2. de nave
970.00 mts2. de área sin techar y piso industrial
150.00 mts2. de oficinas
2 muelles
Entrada de furgón a la nave
altura de 10 metros
piso industrial
Precio $14,500.00 más I.V.A. negociable
Información al 6114-8219
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,14500,'Oficina/Clinica',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,61148219,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(61,'Casa amplia sobre calle principal',NULL,'VENDO CASA EN COL. GUADALCANAL, de Cdad. Delgado
son 308 V2 consta de 4 dormitorios, 3 baños, T/independiente
cochera p/2 veh. cerrada y techada, área de servicio, 3 plantas
encielada, pintura en buen estado, cocina con lava trastos , la
3ª planta con terraza y barandal de hierro, casa bonita.
$ 78,0000 MÁS INF. AL 7183-8649
',NULL,NULL,4,NULL,NULL,NULL,'Venta',3,78000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71838649,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.75346915','-89.1586547',NULL,NULL,NULL,'false',NULL,NULL,103);
INSERT INTO "adverts" VALUES(62,'Campos verdes 1, se vende casa',NULL,'CAMPOS VERDES 1, SE VENDE CASA REMODELADA, HUBICADA EN PASAJE PRIVADO, TRES CUARTOS, SALA , COMEDOR , AREA DE COCINA, Y LAVADERO, BIEN HUBICADA, PISO CERAMICO, ENCIELADA, ARBOLES FRUTALES, COCHERA TECHADA, BIEN SEGURA, CON OTRAS EXTRAS MAS, PRECIO NEGOCIABLE, SE ACEPTAN COTIZANTES FSV Y OTROS TRAMITES.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,23000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/escalon-norte-600-en-privado-iid-654693445','http://images02.olx-st.com/ui/9/41/27/1401797693_654903427_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,73715922,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(63,'Residencial los Girasoles Santa Tecla Seguridad las 24 horas',NULL,'BUEN ZONA RESIDENCIAL LOS GIRASOLES SANTA TECLA SEGURIDAD LAS 24 HORAS
4 habitaciones
2 baños
Cochera 1 vh techada
Sala
Comedor
Cocina C/pantry
Patio amplio
Residencial privada con seguridad las 24 horas
Terreno : 200 v2
$ 72.000
Cel.7430-4659
',NULL,NULL,4,NULL,NULL,NULL,'Venta',2,72000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','http://images03.olx-st.com/ui/9/07/41/1401806311_654960641_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74304659,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(64,'!! casa residecial jardines delas magnolias paqueo comun privado¡¡',NULL,'casa de una planta jardines de las magnolias, privado con parque comun, al costado oriente de cementerio de ayutuxtepeque, sobre la calle que va a dar a la callemariona
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,28000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74374881,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7438577','-89.1992928',NULL,NULL,NULL,'false',NULL,NULL,101);
INSERT INTO "adverts" VALUES(65,'Alquilo penthouse en condominio privado en santa elena, antiguo cuscatlan',NULL,'ALQUILO PENTHOUSE EN CONDOMINIO PRIVADO EN SANTA ELENA, ANTIGUO CUSCATLAN
Precio: $1,200 Dolares mensuales

Condominio privado con areas comunes: Area de piscina, gimnasio completo, salon de reuniones, personal de mantenimiento y seguridad.
Caracteristicas: Vestibulo, sala - comedor amplia, cocina equipada, area de servicio y lavanderia compelta, terraza amplia.
Dos habitaciones amplias con a/c y closet, bano completo compartido. Habitacion principal amplia con a/c, closet y bano completo. Este apartamento cuenta con muebles de primera, espacios amplios y ventilados, excelente mantenimiento.

MAYOR INFORMACION LLAMANDO AL 7985-8505
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',4,1200,'Apartamento',NULL,'http://sanmartin.olx.com.sv/ganga-vendo-terreno-con-financiamiento-25-629-v2-iid-654574692','http://images04.olx-st.com/ui/21/76/08/1401739752_654586808_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79858505,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(66,'Vendo terreno de 14,000 Mts2 en lourdes colon frente al poliedro, La Libertad',NULL,'VENDO TERRENO DE 14.000 MTS CUADRADOS, FRENTE AL POLIEDRO, DE GRAN PLUSVALIA, SOBRE LA PANAMERICANA, a orilla de calle, CERCA DE UNICENTRO DE LOURDES COLON, POR EL PRECIO DE 1,200.000 COMPLETAMENTE NEG, LLAMAR SOLO INTERESADOS AL 70900788, PARA GRANDE PROYECTOS DE COMERCIO.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,1200000,'Terreno',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images03.olx-st.com/ui/22/57/94/1401760119_654665394_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70900788,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(67,'Se alquila exelente ubicacion para oficina o negocio',NULL,'Cochera para 4 vehículos


Sala amplia
Baño
Patio techado
2 habitaciones amplias para dormitorios
2 habitaciones enfrente con entradas individules
ideal para oficinas o negocios



Cochera para 4 vehículos


Sala amplia
Baño
Patio techado
2 habitaciones amplias para dormitorios
2 habitaciones enfrente con entradas individules
ideal para oficinas o negocios




Sala amplia
Baño
Patio techado
2 habitaciones amplias para dormitorios
2 habitaciones enfrente con entradas individules
ideal para oficinas o negocios


Sala amplia
Baño
Patio techado
2 habitaciones amplias para dormitorios
2 habitaciones enfrente con entradas individules
ideal para oficinas o negocios
',NULL,NULL,2,NULL,NULL,NULL,'Alquiler',1,600,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images01.olx-st.com/ui/21/55/68/1401757382_654654768_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74355971,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.5090328','-88.87310745',NULL,NULL,NULL,'false',NULL,NULL,132);
INSERT INTO "adverts" VALUES(68,'Casa de Playa en Residencial Turistico Las Olas, Barra de Santiago',NULL,'Casa de Playa ubicada en segunda fila, bajo sistema de Condominio goza de Ranchos Hamaqueros, piscinas infantiles y para adultos, seguridad privada las 24 horas del día en la mejor Playa de El Salvador.
Casa bien acondicionada, con finos acabados, cuenta con bomba de agua y sistema de letrina tipo francesa.
',NULL,NULL,4,NULL,NULL,NULL,'Venta',3,45000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','http://images03.olx-st.com/ui/21/68/59/1401744045_654605959_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50377970412,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.92906755','-89.8436594',NULL,NULL,NULL,'false',NULL,NULL,1);
INSERT INTO "adverts" VALUES(69,'Excelente plusvalia, casa en buena vista 1 santa tecla',NULL,'
EXCELENTE PLUSVALIA,
Casa en Buena Vista 1 SANTA TECLA
Pje. Y Residencial privado


cochera techada para 2 vehiculos
2 salas,
3 banos,
3 habitaciones,
area de servicio completa
patio y jardin.
área de construcción es de 170 Mts
274 V2.
8 de frente.


PRECIO $111,000 NEGOC.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,111000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-para-oficina-en-col-escalon-iid-655012694','http://images02.olx-st.com/ui/9/26/45/1401817109_655032145_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74505090,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(70,'7 mz de terreno en Tonacatepeque',NULL,'
Tonacatepeque, 7 mz de terreno carretera a San José Las Flores

',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,145000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70937711,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7795846','-89.1182054',NULL,NULL,NULL,'false',NULL,NULL,115);
INSERT INTO "adverts" VALUES(71,'casa a estrenar en st. rosa',NULL,'Alquilo casa en residencial condado santa rosa casa estilo F constituida por.
AREA DE TERRENO 450.00 V2
AREA DE CONSTRUCCION 200.00 M2
PRECIO DE ALQUILER $ 1,300.00 INLUYENDO EL MANTENIMIENTO
CARACTERISTICAS:
Ø COMPLEJO PRIVADO
Ø CON AMPLIAS ZONAS VERDES EN AREAS COMUNES 
Ø DOS PLANTAS BLOOK F
Ø SEGURIDAD LAS 24 HORAS DEL DIA
Ø COCHERA PARA 4 VEHICULOS DOS TECHADOS Y DOS SIN TECHAR
Ø SALA PRINCIPAL SALA FAMILIAR
Ø 
COMEDOR AMPLIO FRENTE AL JARDIN
Ø TERRAZA
Ø JARDIN ENGRAMADO 
Ø COCINA CON MUEBLE DE LUJO
Ø BAÑO SOCIAL
Ø 3 HABITACIONES CADA UNA CON BAÑO LA PRINCIPAL W/CLOSET
Ø AREA DE SERVICIO COMPLETA CON INSTALACIONES PARA LAVADORA Y SECADORA
Ø BODEGUITA
Ø CISTERNA CON BOMBA 
Inf. 2373-3185, 7790-0634
',NULL,NULL,0,NULL,NULL,NULL,'Alquiler',0,1300,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images01.olx-st.com/ui/21/57/29/1401749116_654624829_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,77900634,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(72,'casa en colonia escalon',NULL,'Vendo casa super bonita de lujo en colonia escalon 2 cuartos 3, 2baños, cochera precio negociable',NULL,NULL,3,NULL,NULL,NULL,'Venta',2,98000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70522968,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(73,'SE VENDE, casa en Calle Cuisnahuat de Ciudad Merliot, 570 vrs2., excelente zona comercial.',NULL,'570.00 vrs2.
3 habitaciones
3 baños
sala
comedor
cocina con pantrie
garaje 2 vehículos
jardín
Sobre Calle Cuisnahuat, de alta fluidez vehicular y peatonal.
Propia para vivienda, sala de ventas, comercio, despacho jurídico, contable, clínica dental, inversión, etc.
Precio $280,000.00 poco negociable
Información al 6114-8219
',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,280000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,61148219,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(74,'Vendo apartamento amplio de primer nivel en col. zacamil',NULL,'VENDO APARTAMENTO DE PRIMERA PLANTA EN COLONIA ZACAMIL CONSTA DE SALA COMEDOR AMPLIOS COCINA INDEPENDIENTE AREA DE LAVADEROS 3 DORMITORIOS AMPLIOS 1 BAÑO CON AZULEJOS PATIO GRANDE CERRADO Y TECHADO Y ESPACIO PARA PARQUEAR UN VEHICULO A LA PAR DE LA ENTRADA PRINCIPAL DEL APARTAMENTO CERCA UNIVERSIDAD NACIONAL. ZONA TRANQUILA, PRECIO NEGOCIABLE.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,22500,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74677488,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(75,'Vendo terreno 9.602.02 MTS CUADRADOS, en ciudad triunfo, Jucuapa, usulutan',NULL,'VENDO TERRENO DE 9.602.02 MTS CUADRADOS, EQUIVALENTE A 1 MANZANA MAS UNA CUARTA DE TERRENO, PROPICIO PARA TODO TIPO DE COSECHA, A 2 CUADRAS DE LA PANAMERICANA Y 6 DEL DESVIO LA CIUDAD EL TRIUNFO, USULUTAN, POR EL PRECIO DE $24,000 NEG. LLAMAR SOLO INTERESADOS, AL 70900788
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,24000,'Terreno',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images01.olx-st.com/ui/21/19/31/1401759228_654661831_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70900788,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.51643955','-88.38333615',NULL,NULL,NULL,'false',NULL,NULL,185);
INSERT INTO "adverts" VALUES(76,'Santa Ana Norte',NULL,'Bonita casa a pocos metros de la carretera a metapan, construcción en la parte delantera con plafón y garaje para un vehiculo, baño amplio.
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,24000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images01.olx-st.com/ui/22/37/75/1401751499_654633275_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(77,'Alquilo Edificio Corporativo en Escalón, 500 mt2 de oficina 300 mt2 de parqueo.',NULL,'Alquilo Edificio Corporativo en Escalón, 500 mt2 de oficina + 300 mt2 de parqueo:
Primer Nivel: Recepción, 3 Oficinas, Salón grande o Bodega, 1 Baño, Parqueo techado, Parqueo exterior, Parqueo total 25 vehículos.
Segundo Nivel: Recepción, 1 área abierta para Oficinas de 250 mt2, 2 Baños, 1 Aire acondicionado grande.
Tercer Nivel: Recepción, 2 Oficinas VIP con Aire Acondicionado, 2 Salones grandes con aire acondicionado grande, 2 Baterías de baños.
Canon Mensual: $ 5,500 mas iva.
INF. 7210-1694 y 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,5500,'Oficina/Clinica',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images01.olx-st.com/ui/8/52/36/1400863451_637554636_1-alquilo-edificio-corporativo-en-escalon-500-mt2-de-oficina-300-mt2-de-parqueo-escalon.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(78,'Preciosa Casa Grande en Residencial San Francisco.',NULL,'Preciosa Casa Grande en Residencial San Francisco, consta de:
Lobby.
Sala con chimenea.
Comedor.
Cocina con pantrie.
4 habitaciones.
5 baños.
Jardín.
Cochera para 4 vehículos.
Area de servicio completa.
700 mt2 de construcción.
800 v2 de terreno.
Precio $ 340,000 Neg.
INF. 7210-1694; 78883886
',NULL,NULL,4,NULL,NULL,NULL,'Venta',5,340000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images03.olx-st.com/ui/13/80/84/1400863129_644777684_1-preciosa-casa-grande-en-residencial-san-francisco-san-francisco.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(79,'Se renta habitacion a hombre solo.de buenos principios y responsable..',NULL,'Rento habitacion a hombre solo.
de buenos principios y muy responsable..
sevicios incluidos..
Agua,Lúz,Cable e inter..
$125.por mes.mas un depocito
contrato de seis meces requerido.
cel.71300992
',NULL,NULL,1,NULL,NULL,NULL,'Alquiler',0,125,'
        alquiler temporario - vacaciones
    ',NULL,'http://sanmartin.olx.com.sv/ganga-vendo-terreno-con-financiamiento-25-629-v2-iid-654574692','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71300992,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(80,'vendo anfiteatro del antiguo poliedro de 14,000mts 2, lourdes colon, la libertad',NULL,'VENDO INMUEBLE DE 14.000 MTS CUADRADOS, CONOCIDO POR EL POLIEDRO,PROPICIO PARA IGLESIAS, UNIVERSIDADES, INSTITUCIONES GUBERNAMENTALES Y NO GUBERNAMENTALES, INSTITUCIONES INTERNACIONALES, POR EL PRECIO DE $1.200.000 COMPLETAMENTE NEGOCIABLES, LLAMAR SOLO INTERESADOS AL 70900788.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,1200000,'Terreno',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images01.olx-st.com/ui/9/48/61/1401759954_654664761_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70900788,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(81,'¡Ganga! Vendo lote de 1,600 metros cuadrados con acceso a calle pavimentada Atiquizaya',NULL,'¡Ganga!
Vendo lote de 1,600 metros cuadrados, está ubicado en La con acceso a calle pavimentada, agua potable, aguas negras, energía eléctrica en $ 30,000 negociables... más información al teléfono 7642-1825
Vtro
Vtro',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,30000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/escalon-norte-600-en-privado-iid-654693445','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76421825,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9743259','-89.7551734',NULL,NULL,NULL,'false',NULL,NULL,3);
INSERT INTO "adverts" VALUES(82,'Vendo bonito lote de 10 x 20.listo para construir',NULL,'Vendo terreno listo para construir pido 4500 inf al 79065132
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,4500,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/escalon-norte-600-en-privado-iid-654693445','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79065132,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'14.0227061','-88.92093655',NULL,NULL,NULL,'false',NULL,NULL,42);
INSERT INTO "adverts" VALUES(83,'Bonita casa ciudad versailles villa monaco',NULL,'Vendo bonita casa en Ciudad Versailles Villa Mónaco, poligono 32 de dos cuartos, sala, comedor, cocina, área de lavadora independientes. Construcción en la parte de adelante y enrrejada por el patio.
$ 20,000 dolares Negociable.
Inf. Cel: 7057-3056 ó 2294-6836
',NULL,NULL,2,NULL,NULL,NULL,'Venta',1,20000,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images01.olx-st.com/ui/22/37/16/1401759547_654663116_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70573056,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(84,'Remato 5,000 v2 en Suchitoto.',NULL,'Remato 5,000 v2 en Suchitoto.
Ubicada a orilla de calle para accesar al Malecón
Precio: $35,000
INF. 7210-1694 y 7888-3886
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,35000,'Terreno',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images03.olx-st.com/ui/8/59/09/1400862885_644785009_1-fotos-de-remato-5-000-v2-en-suchitoto.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.93446275','-89.02395485',NULL,NULL,NULL,'false',NULL,NULL,130);
INSERT INTO "adverts" VALUES(85,'Carretera a Cinquera',NULL,'Hermoso terreno Suchitoto, Quezalapa carretera a Cinquera, 4 mz con rio por medio, orilla de calle
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,100000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','http://images04.olx-st.com/ui/21/66/55/1401744092_654606155_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70937711,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.93446275','-89.02395485',NULL,NULL,NULL,'false',NULL,NULL,130);
INSERT INTO "adverts" VALUES(86,'casa tipo quintq',NULL,'5 minutos de catedral
',NULL,NULL,5,NULL,NULL,NULL,'Venta',3,150,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-frente-al-mar-en-los-cobanos-salinitas-en-venta-iid-654952133','http://images03.olx-st.com/ui/21/81/71/1401805916_654958071_1-Pictures-of--casa-tipo-quintq.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74731555,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(87,'Vendo 2 casas en Lomas de San Antonio.',NULL,'Vendo 2 casas en Lomas de San Antonio, Sonsonate.
-Residencial Privado.
-Vigilancia las 24 horas.
-3 habitaciones.
-1 baño.
-Sala.
-Comedor.
-Cocina.
-Patio.
-Cochera 1 vehículo.
-Patio.
- 65 mt de construcción.
-123.27 v2 de terreno.
-$40,000 Negociables cada una.
INF. 7210-1694 y 7888-3886
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,40000,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images02.olx-st.com/ui/13/29/04/1400863007_644782504_1-fotos-de-vendo-2-casas-en-lomas-de-san-antonio.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71032485','-89.73001955',NULL,NULL,NULL,'false',NULL,NULL,26);
INSERT INTO "adverts" VALUES(88,'Se alquila! Apartamento con 2 habitaciones, patio y en residencia cerrada con seguridad! $',NULL,'Se alquila! Apartamento con 2 habitaciones, patio y en residencia cerrada con seguridad! $110 neg, arriba de plaza mundo, col. Los ángeles, antes de llegar a monte carmelo, zona accesible, R31 Cerca de Plaza Mundo
',NULL,NULL,2,NULL,NULL,NULL,'Alquiler',4,110,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/escalon-norte-600-en-privado-iid-654693445','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,73277878,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71045','-89.14355165',NULL,NULL,NULL,'false',NULL,NULL,114);
INSERT INTO "adverts" VALUES(89,'ciudad pariso 1',NULL,'Vendo casa en ciudad paraiso 1 con 3 cuartos 1 baño garage para 2 vehiculos precio negociable',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,26000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70522968,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(90,'3mz de terreno',NULL,'Se vende 3mz de terreno, Carretea a opico a 10 min de la fabrica Kimberly Clark, y a 2min de la calle principal que conduce a San Juan opico a la altura del cantón agua escondida por la entrada de la bomba. posee servicio de energía eléctrica y agua potable. El terreno es perfecto para lotificación, Nave Industrial, Bodegas o cultivo ya que esta en una zona industrializada y poblada. el precio es de 35mil por manzana Negociable. Tel 7707932 o 23282402
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,105000,'Terreno',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images01.olx-st.com/ui/9/58/31/1401749365_654625731_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,77007932,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(91,'Local en Antiguo Cuscatlan, ideal para restaurante, cafe, gimnacio, etc.',NULL,'Local ubicado en Antiguo cuscatlan, de 200 mts.2 , a estrenar, con parqueo amplio, ideal para oficinas, gimnacios, restaurantes, etc.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,2000,'Oficina/Clinica',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images03.olx-st.com/ui/21/06/66/1401758775_654660066_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78416698,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(92,'Vendo casa de esquina en Santa Lucia Ilopango..',NULL,'Vendo casa de esquina en Santa Lucia Ilopango,por escuela Venezuela ala par de el parqueo tiene verja construida sala grande area de cocina con pantry 4,habitaciones con closets encielada piso de ceramica espacio para cochera etc,por ser de esquina esta casa es mas grande que las de pasaje precio $ 42.000 un poco negociable tel. 76358804
',NULL,NULL,4,NULL,NULL,NULL,'Venta',0,42000,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images03.olx-st.com/ui/21/33/26/1401761983_654672726_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76358804,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6902395','-89.111048',NULL,NULL,NULL,'false',NULL,NULL,106);
INSERT INTO "adverts" VALUES(93,'Quinta 1200 varas',NULL,'Hermosa quina y muy moderna con mucho espacio tiene 5 cuartos super grandes con closet todos sala comedor y cosían de lujo una sala super grande todo tiene muchas ventanas dentra mucha luz tres baños dos terrazas preciosas y un rancho una pequeña piscina tiene comumpios para niños un jardín muy grande y en gramado cochera piara 3 vínculos área de lavado fuera de los ojos de las visitas con todo lo servicios solo interesados no intermediario personas serias que cuenten con recursos para comprarla grasias 7473 1555 presión negosiable
',NULL,NULL,5,NULL,NULL,NULL,'Venta',3,150,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/escalon-norte-600-en-privado-iid-654693445','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74731555,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(94,'vendo casa en colonia vella vista san marcos',NULL,'vendo casa en colonia vella vista san marcos a unos 500 mts arriba de alcaldia sobre calle principal cochera un vehiculo dos cuartos un baño patio interior sala comedor cocina 8 mts de frente x 29 de fondo $22,000 ng. 21307878 79391600
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,22000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images01.olx-st.com/ui/21/95/87/1401750266_654628887_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,21307878,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6590186','-89.1731475',NULL,NULL,NULL,'false',NULL,NULL,110);
INSERT INTO "adverts" VALUES(95,'Dos Navio Industrial en San Marcos, zona de maquilas',NULL,'INMUEBLE: Contiene Dos Bodegas (navio industrial)

Área de 4,173.42 M²
Area de Oficinas 150 mt2
Capacidad para cuatro furgones.
2 muelles de carga
Ubicación:
Antigua Calle San Marcos ZONA DE MAQUILAS
Precio de alquiler:$ 14,000 neg. Mas IVA
INFO Sra de Garcia
7872 6187
7069 6913
2130 3752
7018 6452
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,14000,'Oficina/Clinica',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images02.olx-st.com/ui/9/64/08/1401763128_654675608_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,21303752,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6590186','-89.1731475',NULL,NULL,NULL,'false',NULL,NULL,110);
INSERT INTO "adverts" VALUES(96,'Urge vender residencial bella santa ana',NULL,'RES BELLA SANTA ANA, Accesible a Salida Principal, 3 habitaciones, 1 baño, Energia 220 v, para conexiones de Agua Caliente y Secadora, Cochera para 1 vehiculo cerrada, piso Ceramico Blanco, area de Servicio parcial techado y enrrejado. Excelente ubicacion. Precio 36,000 neg. informacion al correo
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,36000,'Apartamento',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(97,'Vendo casa remodelada! precio accesible!',NULL,'vendo casa en Soyapango, remodelada totalmente!!!! vendo casa en urb lomas del rio,soyapango, recien remodelada, consta de 3 cuartos, sala, comedor, cocina, area para lavadora, puertas de madera, ventanas frontales francesas, piso ceramico, zocalos, instalacion electrica nueva completamente. 25163141 hagamos la cita, la observa y hablamos de precio. acepto FSV, BANCOS, EFECTIVO, CAMBIOS, etc.... con escrituras en mano.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,20000,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images04.olx-st.com/ui/22/30/98/1401751215_654632298_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25163141,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71045','-89.14355165',NULL,NULL,NULL,'false',NULL,NULL,114);
INSERT INTO "adverts" VALUES(98,'Bonito terreno en Res. Quintas de la Hacienda.',NULL,'Bonito terreno en residencial de gran nivel con estricta seguridad, parques, club social, canchas de tenis y fútbol entre otros.
El terreno es completamente plano y cuenta con un área de 2,700 v2 ideales para una hermosa residencia.
precio $48,000 neg
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,48000,'Terreno',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images03.olx-st.com/ui/21/56/69/1401749100_654624769_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78427662,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.564733','-89.2622852',NULL,NULL,NULL,'false',NULL,NULL,89);
INSERT INTO "adverts" VALUES(99,'Casa de Playa en Costa Azul.',NULL,'Casa completamente engramada, piscina, cuartos con A/C, segura, cerca de la Playa.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',2,33000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,77827034,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71032485','-89.73001955',NULL,NULL,NULL,'false',NULL,NULL,26);
INSERT INTO "adverts" VALUES(100,'Casa muy amplia y Muy Accesible GANGA !!!',NULL,'Es una casa muy amplia de 1 planta con 4 habitaciones con mucho espacio sala y cocina individual
. con jardin y pila muy grande pasaje ancho que permite estacionar un auto enfrente de de la casa zona tranquila y la colonia tiene puesto policial para su mayor tranquilidad
EL precio es NEGOCIABLE
',NULL,NULL,4,NULL,NULL,NULL,'Venta',1,20000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images03.olx-st.com/ui/21/11/51/1401658570_654121051_1-casa-muy-amplia-y-muy-accesible-ganga-col-santa-eduviges.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70042601,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71045','-89.14355165',NULL,NULL,NULL,'false',NULL,NULL,114);
INSERT INTO "adverts" VALUES(101,'Gasolinera en Ahuachapan. De ganga !!!!',NULL,'Gasolinera en Ahuachapan. De ganga !!!! Atención inversionistas de Santa Ana, Ahuachapan, Sonsonate y San Salvador. ¿Busca una gasolinera a precio de ganga? No busque mas. Se vende gasolinera lista para comenzar a operar. Ubicada en Carret. a frontera Las Chinamas Km. 95, Cantón Llano Doña María, Ahuachapan, contiguo a Parque Acuatico Acualandia. Cuenta con un terreno amplio y un área construida que incluye isla con 3 bombas dobles para el despacho de combustible y tienda de conveniencia. Ya cuenta con los permisos respectivos. Por la amplitud del terreno también puede utilizarse como plantel para autobuses o vehículos de carga pesada. Precio $350 mil negociables. Si necesita información adicional o fotografías puede comunicarse al 7877-3988 o al correo.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,350000,'Oficina/Clinica',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images03.olx-st.com/ui/22/18/75/1401681283_654191375_1-fotos-de-gasolinera-en-ahuachapan-de-ganga.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78773988,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.92906755','-89.8436594',NULL,NULL,NULL,'false',NULL,NULL,1);
INSERT INTO "adverts" VALUES(102,'Vendo bonita casa en san ramon de los altos privado con vigilancia',NULL,'VENDO BONITA CASA EN SAN RAMON DE LOS ALTOS EN PRIVADO LA CASA CONSTA DE COCHERA CERRADA Y TECHADA UN VEHICULO AFUERA CABE OTRO, SALA COMEDOR COCINA INDEPENDIENTE CON LAVATRASTOS Y GABINETE 1 BAÑO CON AZULEJOS 2 DORMITORIOS CON CLOSETH Y POSIBILIDAD DE UN TERCER DORMITORIO PATIECITO PILA GRANDE PISO CERAMICO ENCIELADA SE ENCUENTRA EN UN PASAJE CERRADO CON VIGILANCIA 24 HORAS PRECIO NEGOCIABLE. SE ENCUENTRA CERCA BOULEVARD CONSTITUCION Y EL NUEVO WALMART
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,61000,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79737529,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(103,'venta de casa muy amplia',NULL,'Se vende una casa de dos plantas, con sótano, tiene 5 habitaciones, 2 salas muy amplias, comedor y cocina muy grandes, patio, 2 terrazas, una al frente de la casa y otra al fondo de la vivienda. tiene garage para 2 vehiculos, la casa estuvo alquilada por 18 años y actualmente esta desocupada, pero se le han quitado los servicios de telefonia, agua y energia electrica. la casa es muy grande. se vendia en $35000 ya pintada y arreglada solo para habitar, pero por liquidacion se vende al mejor postor precio base y negociable.
',NULL,NULL,5,NULL,NULL,NULL,'Venta',2,28000,'Apartamento',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images02.olx-st.com/ui/9/31/25/1401679034_654182625_1-venta-de-casa-muy-amplia-colonia-primavera.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,73534028,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.75270835','-89.18363225',NULL,NULL,NULL,'false',NULL,NULL,102);
INSERT INTO "adverts" VALUES(104,'Casa a reparar en Venta en Santiago Nonualco terreno 2,625V2',NULL,'CASA A REPARAR EN VENTA EN SANTIAGO NONUALCO, LA PAZ.
Área del terreno: 2,625V2 / 1,881.80m2
Descripción: Casa sistema mixto, 4 amplias habitaciones, 2 bañoos, cocina, corredor en terreno semi-urbano de topografía plana a orilla de carretera pavimentada (Calle Hoja de Sal) cuenta con servicios de agua potable y Energía Eléctrica, amplia vegetación, zona con alto potencial de desarrollo, Municipio incluido en la Ruta Turística denominada “Los Nonualcos” a tan solo 15 minutos de Aeropuerto Internacional de Comalapa.
Posibles usos: Habitar, Casa de Retiros, Restaurante, Quinta Recreacional, etc
Dirección: Entre Carretera Litoral y Carretera Antigua a Zacatecoluca, en el Barrio San Juan, Santiago Nonualco, La Paz.
Precio: $73,000 negociables.
Inf. 7864-8935
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,73000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images02.olx-st.com/ui/21/72/58/1401656977_654116458_1-casa-a-reparar-en-venta-en-santiago-nonualco-terreno-2-625v2-santiago-nonualco.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78648935,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.51060835','-88.9391327',NULL,NULL,NULL,'false',NULL,NULL,152);
INSERT INTO "adverts" VALUES(105,'Vendo Casa Residencial San Pedro Mejicanos',NULL,'Vendo Casa Residencial San Pedro

Informacion cel 7669 1683
Vendo casa de 69 metros cuadrados de construccion,$ 32,500 fijo, consta de:
2 plantas
2 habitaciones con opcion ampliar a 4 hab ( uno con closet )
Sala y comedor amplio
Cocina con pantry y lavatrastos
Ventanas francesas y encielada
Pila grande,
pizo ceramico
1 Baño individual ( ducha caliente ), 2 sanitarios individuales
Coneccion para lavadora
Pequeña sisterna area
Jardin con cerca y mesa de cemento enfrente de la casa
Ubicada frente a parqueo
Porton privado y seguridad las 24 horas,
Cerca de colegios, bus de la ruta uno y microbus 53,
y waltmark

',NULL,NULL,2,NULL,NULL,NULL,'Venta',0,32500,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images01.olx-st.com/ui/21/11/20/1401658591_654121120_1-fotos-de-vendo-casa-residencial-san-pedro-mejicanos.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76691683,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(106,'Vendo Bonita Casa en CIMA I Zona Privada',NULL,'
Casa de 1 planta - ampliada
Distribución: 4 dormitorios (2 con closet y su propio baño / y 2 dormitorios sin closet y con baño compartido)
1 cuarto de servicio completo
Sala
Comedor
Cocina con pantry y desayunador
Jardín con terraza (con vista a la ciudad) muy panorámico
Piso de cerámica
Parqueo para 1 vehículo
Seguridad las 24 horas del día (totalmente privado)

Importante: actualmente la casa esta habitada por la dueña.
Precio: $120,000 (Poco Negociable)
',NULL,NULL,4,NULL,NULL,NULL,'Venta',3,120000,'Apartamento',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images02.olx-st.com/ui/22/48/20/1401684108_654204820_1-fotos-de-vendo-bonita-casa-en-cima-i-zona-privada.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78905153,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(107,'Casa a estrenar Condado Santa Rosa, finos acabados.',NULL,'Casa a estrenar en Condado Santa Rosa, de 2 niveles, finos acabados.
-Sala
-Comedor
-Cocina con pantrie
-3 habitaciones, la principal con walkin closet y baño
-Terraza
-Area de servicio completa
-Estacionamiento 2 vehículos
-3 baños
-Jardín
-Cisterna
-150 mt2 de construcción
-286 v2 de terreno
Precio $ 800
INF. 7210-1694
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',3,800,'Apartamento',NULL,'http://antiguocuscatlan.olx.com.sv/ganga-casa-en-arcos-de-santa-elena-iid-652703331','http://images02.olx-st.com/ui/13/51/29/1400863402_644774229_1-casa-a-estrenar-condado-santa-rosa-finos-acabados-condado-santa-rosa.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72101694,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(108,'Se alquila casa en san antonio abad',NULL,'CASA EN SAN ANTONIO ABAD. CON LOCAL INDEPENDIENTE.
AREA DE TERRENO 250V2 Y 230M2 DE CONSTRUCCION.
CASA EN CALLE SECUNDARIA, VIGILANCIA LAS 24 HORAS, CUENTA CON LOCAL INDEPENDIENTE CON SU SANITARIO, COCHERA PARA 2 VEHICULOS, SALA, COMEDOR, COCINA CON PANTRY Y BARRA DESAYUNADORA, TODO INDEPENDIENTE, TERRAZA, JARDIN, AREA DE SERVICIO COMPLETA, CISTERNA DE 9M3, SALA FAMILIAR AMPLIA, 3 HABITACIONES CON CLOSET, PRINCIPAL CON BAÑO COMPLETO Y 2 CON BAÑO COMPARTIDO, PISO DE CERAMICA
ALQUILER $750.00
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',3,750,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','http://images03.olx-st.com/ui/21/07/67/1401814614_655010067_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70654509,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(109,'Total de terreno 3000v2',NULL,'RESIDENCIA consta de 3000v2 y 500mts de construcción , consta de 3 hab principales con su baño y closet, área de servicio completa , sala fam y principal , comedor cocina con su pantry , lavadora de platos , bodeguitas piscina área de jardín áreas de terrazas
',NULL,NULL,3,NULL,NULL,NULL,'Venta',5,350000,'Apartamento',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images02.olx-st.com/ui/9/67/81/1401682580_654196681_1-total-de-terreno-3000v2-la-hacienda.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,71984207,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.564733','-89.2622852',NULL,NULL,NULL,'false',NULL,NULL,89);
INSERT INTO "adverts" VALUES(110,'Espectacular Apto Full Amueblado Condominio Exclusivo Terraza Hermosa Vista',NULL,'Apartamento Full Amueblado de Lujo
3hb cada uno con baÑo privado A/C
Terraza Vista Espectacular Amplios Espacios
Condominio Exclusivo Piscina Gym
incluye : mantenimiento seguridad 24/7 bodega 2 parqueos exclusivos parqueo para visitas
alquiler $1,500 !!!!!
NOSOTROS TENEMOS LO QUE BUSCA. SERA UN PLACER ATENDERLE.7483-9380 7483-9380
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',4,1500,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','http://images02.olx-st.com/ui/21/86/27/1401813502_655007927_1.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74839380,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(111,'Residencia en Alquiler en Cumbres de La Esmeralda',NULL,'N RESIDENCIAL PRIVADO
CON VISTA
CUMBRES DE LA ESMERALDA , ANTIGUO CUSCATLÁN
Área de terreno 742.37v2
PRIMER NIVEL
-Cochera con portón eléctrico para 3 vehículos
-Área de lobbie
-Amplio estudio
- Amplia sala
-Baño social
-Comedor independiente con doble acceso a la cocina
-Cuarto de despensa/alacena
-Área de servicio completo, Cisterna
-terraza con amplio jardín y vista
SEGUNDO NIVEL:
-Amplia sala familiar
-3 habitaciones cada una con baño y vista
- 1 Habitación principal con aire acondicionado, Walk-in closet , baño con jacuzzi,
PRECIO ALQUILER $3,000
Información: 7246-4284
',NULL,NULL,4,NULL,NULL,NULL,'Alquiler',6,3000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images04.olx-st.com/ui/9/46/25/1401656215_654114125_1-residencia-en-alquiler-en-cumbres-de-la-esmeralda-cumbres-de-la-esmeralda.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,72464284,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.66930795','-89.2510443',NULL,NULL,NULL,'false',NULL,NULL,77);
INSERT INTO "adverts" VALUES(112,'Local ideal para cafe, restaurante, merendero, etc.',NULL,'Lindisimo casa adaptada para negocio en paseo concepcion - paseo el carmen en Santa Tecla, parte baja.
Un salon amplio con baños , cocina, terraza, bodega, baño de servicio, etc. aprox. 120 mts.
Un local adicional con baño y area de oficios.
Precio $ 600.00 mas iva.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,600,'Oficina/Clinica',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images01.olx-st.com/ui/21/38/98/1401762078_654673098_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78416698,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(113,'vendo esta casa',NULL,'2 cuartos 1 sala 1 baño 1 cochera
precio $ 18,000.00 negaciables.
telefono: 74678421
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,18000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images02.olx-st.com/ui/21/35/16/1401655839_654112916_1-vendo-esta-casa-las-margaritas.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74678421,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.4980479','-88.5282719',NULL,NULL,NULL,'false',NULL,NULL,178);
INSERT INTO "adverts" VALUES(114,'Merliot, Casa Santa Teresa, 2 plantas ,remodelada',NULL,'COCHERA 2 VEH CERRADA, SALA Y COMEDOR INDEPENDIENTES,COCINA, DESAYUNADOR, TERRAZA, PATIO,BAÑO SOCIAL,AREA DE SERVICIO COMPL, CISTERNA,
2 PLANTA: 3 DOR, 2 BAÑOS,SALA STAR GRANDE, ESTUDIO.SEGURIDADVALUO BCO $104,000.00 CREDITO PREAPROBADO POR EL 90%, SOLO INTERESADOS NO INTERMEDIARIOS,
',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,104000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','http://images02.olx-st.com/ui/22/14/98/1401745232_654610698_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,22236831,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(115,'Bonita Casa de dos Plantas en Residencial La Gloria',NULL,'Casa en Urbanizacion La Gloria, en pasaje, de dos plantas, piso ceramico en toda la casa, 1er Planta Sala-Comerdor, Baño Completo, Cocina con desayunador con pantri, cuarto de estudio, Traspatio lavadero, alacena y conceccion de lavadora,. 2da Planta tres dormitorios, baño completo, cielo falso, cada cuarto con su closet. Zona super tranquila. Tel 77307382 $65,000
',NULL,NULL,3,NULL,NULL,NULL,'Venta',2,65000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','http://images04.olx-st.com/ui/21/29/21/1401745776_654612821_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,77307382,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7391679','-89.21040265',NULL,NULL,NULL,'false',NULL,NULL,107);
INSERT INTO "adverts" VALUES(116,'Linda Casa ,Segura y Barata',NULL,'La casa esta ubicada carretera a sonsonate yendo para san salvador , esta en una zona privada, su vigilancia las 24 horas , cuenta con parque recreativo. Tiene cochera para dos carros , cisterna,esta lista solo para irse a vivir en ella .
',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,49000,'Apartamento',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images01.olx-st.com/ui/21/29/00/1401676320_654172900_1-fotos-de-linda-casa-segura-y-barata.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70365008,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(117,'Lotificacion',NULL,'Propiedad en venta en San Luis Talpa Cantón La Cuchilla lotificación Miraflores de Comalapa carretera a Zacatecoluca a 4 kmts del desvío del aeropuerto 19 lotes de 240.37 V2, el área total es de 4567 V2 completamente planos, se vende todos los lotes juntos.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,70000,'Terreno',NULL,'http://sansalvadorcity.olx.com.sv/ganga-vendo-casa-en-una-de-las-sonas-mas-seguras-de-ticsa-sanbartolo-12500-negosiable-iid-654605851','','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70937711,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.47105095','-89.0873538',NULL,NULL,NULL,'false',NULL,NULL,145);
INSERT INTO "adverts" VALUES(118,'Terreno Por Gasolinera Texaco Km 201/2, Entrada San Pedro Perulapan',NULL,'Area del Terreno: 855.80 m2 == 1,224.48 v2
Terreno Poterncialmente Residencial
Topografia Plana y Semi Plana
Calle de Acceso de Acceso Vehicular
con cuneta y toda la infraestructura Urbanistica
El lote cuenta con: Acometida de Energia Electrica, Agua Potable, Aguas Lluvias, Aguas Negras, Aseo, Iluminacion, y Telefno, Transporte Colectivo, Porton y Vigilancia Privada,
El Terreno esta ubicado en el km 20 1/2 carretera panamericana, 100 mts despues de la Gasolinera Texaco, Calle a San Pedro Perulapan, ya dejando la Carretera Panamericana, a la izquierda la Calle y 200 mts al fondo de la calle llegan al Terreno y Ubican El Zaguan, y El Rotulo El Pinar,
Precio $ 50,000.00 Negociable
Informacion a los telefonos:
7659-4043
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,49500,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-puesto-en-el-mercado-central-de-santa-tecla-iid-654148476','http://images03.olx-st.com/ui/9/47/48/1401673578_654163948_1-terreno-por-gasolinera-texaco-km-201-2-entrada-san-pedro-perulapan-entrada-a-san-pedro-perulapan.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,76594043,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7503845','-89.05757905',NULL,NULL,NULL,'false',NULL,NULL,111);
INSERT INTO "adverts" VALUES(119,'18.500 Varas Cuadradas a orilla de Autopista',NULL,'SANTA ANA, VENDO TERRENO de 18.500 varas cuadradas, 250 metros de frente, a orilla de AUTOPISTA SANTA ANA-CHALCHUAPA, frente a Complejo privado Villa Verde, plano, con factibilidad de servicios. VALOR NEGOCIABLE $ 10.--(DIEZ DOLARES) Vara Cuadrada, Total $ 185.000.--MAYOR INFORMACION AL TEL. 79881255
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,185000,'Terreno',NULL,'http://santaanacity.olx.com.sv/vendo-ganga-iid-654251732','http://images03.olx-st.com/ui/22/47/78/1401715908_654423978_1-fotos-de-18-500-varas-cuadradas-a-orilla-de-autopista.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79881255,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(120,'Casa Tipo Siena',NULL,'para Mas detalles ingresar a bienes raicessv . eshost . es
',NULL,NULL,4,NULL,NULL,NULL,'Venta',2,90000,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-puesto-en-el-mercado-central-de-santa-tecla-iid-654148476','http://images01.olx-st.com/ui/9/13/43/1401684216_654151043_1-fotos-de-casa-tipo-siena.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78697838,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(121,'Alquilo Ciudad Versalles',NULL,'Alquilo Ciudad Versalles . Villa Burdeos dos cuartos . Cochera cerrada . Escaleras al plafon . 100 $',NULL,NULL,2,NULL,NULL,NULL,'Alquiler',1,100,'Apartamento',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70398831,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(122,'Venta de terreno / lotes / propiedad',NULL,'Descripción:
- 2 lotes de de 8 x 22 metros cada uno
- a 5 minutos del lago de Ilopango en vehiculo
- terrenos amplios y planos
- listo para construir.
Mayor informacion tel 7439-0375, con Wenseslao Diaz
$6,000 cada lote, $12,000 los dos lotes juntos negociable.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,12000,'Terreno',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images02.olx-st.com/ui/22/88/76/1401677993_654178276_1-venta-de-terreno-lotes-propiedad-lotificacion-san-gerardo.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74390375,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6902395','-89.111048',NULL,NULL,NULL,'false',NULL,NULL,106);
INSERT INTO "adverts" VALUES(123,'Vendo casa amplia en Soyapango / Los Santos I',NULL,'Casa amplia, zona segura, tiene 4 dormitorios, un gran terreno construido, amplio patio trasero.
',NULL,NULL,4,NULL,NULL,NULL,'Venta',2,50000,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-puesto-en-el-mercado-central-de-santa-tecla-iid-654148476','http://images04.olx-st.com/ui/10/80/67/1401671521_654157367_1-vendo-casa-amplia-en-soyapango-los-santos-i-los-santos-1.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78545390,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71045','-89.14355165',NULL,NULL,NULL,'false',NULL,NULL,114);
INSERT INTO "adverts" VALUES(124,'Oferta!! Amplia casa moderna casi nueva!!',NULL,'Amplia casa muy moderna, bien construida, reciente, con sus respectivas divisiones cuatro cuartos amplios, dos cuartos con su propio baño, sala super amplia, igual el comedor la cocina con sus respectivas conexiones, un cuarto para lavado especialmente y con sus conexiones de lavadora, pila pequeña en el corredor lateral la casa no esta pegada a la de los vecinos para mayor privacidad, con sus salientes listas para elevarla o instalar alambrado eléctrico, totalmente encielada, piso de cerámica antideslizante, baño de visita, cochera para 2 vehículos, dos amplios patios con sus respectivos reflectores. precio negociable!! info solo por correo.
',NULL,NULL,4,NULL,NULL,NULL,'Venta',3,90000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images03.olx-st.com/ui/22/40/18/1401652901_654103218_1-oferta-amplia-casa-moderna-casi-nueva-montecarmelo-1.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Ubicación',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.71045','-89.14355165',NULL,NULL,NULL,'false',NULL,NULL,114);
INSERT INTO "adverts" VALUES(125,'¡A estrenar! Moderna Casa Nueva en Venta $126,300',NULL,'¡A estrenar! Casa nueva en venta en Residencial Cumbres de Palo Alto $126,300
Terreno 320 V2
Área de construcción 120 M2

Una planta.
Estacionamiento 2 vehículos.
Cisterna.
Porche.
3 habitaciones con closet.
2 baños completos.
Habitación principal con walk in closet y baño privado.
Sala.
Comedor.
Cocina tipo americana con pantry completo e instalación para lavadora de platos.
Terraza.
Amplio jardín.
Área de servicio completa con instalación para lavadora de ropa techada.
Acabados modernos y de lujo.
Residencial privada con caseta de vigilancia, parques recreativos, casa club y amplias zonas verdes.

Construyó y comercializa:
Alternativa Inmobiliaria S.A. de C.V.

Mayor información:
7318-5574
',NULL,NULL,3,NULL,NULL,NULL,'Venta',2,126300,'Apartamento',NULL,'http://sanjosevillanueva.olx.com.sv/bonito-terreno-en-res-quintas-de-la-hacienda-iid-654624769','http://images04.olx-st.com/ui/9/77/25/1401749793_654627225_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,73185574,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.587552','-89.2882218',NULL,NULL,NULL,'false',NULL,NULL,96);
INSERT INTO "adverts" VALUES(126,'Venta de Terreno a 100mts de Centro Urbano de Nahuizalco',NULL,'Terreno de 49,998.10 mts2 (71,537.28v2) a la venta en canton Ixatactec, 100 mts antes del centro urbano de Nahuizalco, Municipio de Nahuizalco, departamento de Sonsonate.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,275000,'Terreno',NULL,'http://santaanacity.olx.com.sv/18-500-varas-cuadradas-a-orilla-de-autopista-iid-654423978','http://images01.olx-st.com/ui/21/61/37/1401723656_654485437_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,62446842,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7804787','-89.7387114',NULL,NULL,NULL,'false',NULL,NULL,33);
INSERT INTO "adverts" VALUES(127,'Locales Comerciales con financiamiento',NULL,'para Mas detalles ingresa bienes raices sv . eshost . es
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,43441,'Oficina/Clinica',NULL,'http://santatecla.olx.com.sv/vendo-puesto-en-el-mercado-central-de-santa-tecla-iid-654148476','http://images04.olx-st.com/ui/9/00/26/1401684471_654149426_1-fotos-de-locales-comerciales-con-financiamiento.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78697838,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(128,'Vendo linda casa en residencial gratamira.',NULL,'VENDO CASA EN RESIDENCIAL GRATAMIRA
EN CARRETERA HACIA SANTA ANA.

3 dormitorios, el principal con baño y walking closet.
2 dormitorios comparten 1 baño.
baño social.
vestibulo
amplio jardin.
sala
comedor
terraza.
area de servicio completa.
cochera para 2 vehiculos
cisterna
agua caliente
Precio $170,000 neg.

',NULL,NULL,3,NULL,NULL,NULL,'Venta',3,170000,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/casa-santa-elena-madreselva-iid-654098744','http://images02.olx-st.com/ui/22/64/75/1401660341_654125975_1-fotos-de-vendo-linda-casa-en-residencial-gratamira.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78867936,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9866054','-89.6780062',NULL,NULL,NULL,'false',NULL,NULL,16);
INSERT INTO "adverts" VALUES(129,'Se alquila casa espectacular en col. Maquilishuat $2800',NULL,'Cuatro habitaciones, dos salas, comedor, dos cocinas, área de servicio con su cuarto, 4 baños, jardín, parqueo para 7 vehículos.
',NULL,NULL,4,NULL,NULL,NULL,'Alquiler',4,2800,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','http://images01.olx-st.com/ui/9/28/42/1401814221_655012642_1-Pictures-of--Se-alquila-casa-espectacular-en-col-Maquilishuat-2800.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78888550,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(130,'Rancho de Playa ideal para descanso',NULL,'Parque Residencial ATAMI
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,27000,'Terreno',NULL,'http://acajutla.olx.com.sv/terreno-antes-de-llegar-a-campana-en-lotificacion-san-juan-carretera-al-puerto-de-acajutl-iid-654167250','http://images02.olx-st.com/ui/21/57/15/1401676878_654175215_1-Pictures-of--Rancho-de-Playa-ideal-para-descanso.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78274081,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.59426815','-89.46954495',NULL,NULL,NULL,'false',NULL,NULL,81);
INSERT INTO "adverts" VALUES(131,'Se alquila apartamento de lujo con línea blanca $1100',NULL,'Tres habitaciones, sala, comedor, cocina, área de servicio, tres baños, terraza, piscina, gimnasio, área de usos múltiples.
',NULL,NULL,3,NULL,NULL,NULL,'Alquiler',3,1100,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','http://images03.olx-st.com/ui/9/45/79/1401812815_655003679_1-Pictures-of--Se-alquila-apartamento-de-lujo-con-linea-blanca-1100.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78888550,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(132,'Casa Tipo Mansion',NULL,'para Mas detalles ingresar a bienes raicessv . eshost . es
',NULL,NULL,4,NULL,NULL,NULL,'Venta',3,188000,'Apartamento',NULL,'http://santatecla.olx.com.sv/vendo-puesto-en-el-mercado-central-de-santa-tecla-iid-654148476','http://images01.olx-st.com/ui/10/37/97/1401683967_654153097_1-fotos-de-casa-tipo-mansion.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78697838,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.8762505','-89.3583689',NULL,NULL,NULL,'false',NULL,NULL,86);
INSERT INTO "adverts" VALUES(133,'2 lotes san Sebastian Salitrillo',NULL,'/ f VENDO 2 SOLARES de 10 por 20 cada unocon arbolitos de coco mango naranjo huertacercados con agua ya instalada con facilidades de luz pasa en frente escuela a una cuadra cancha grande a una cuadracordon cuneta super selectos y gasolinera a 1 km info 78204195 4,500 por los dos
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,4500,'Terreno',NULL,'http://santaanacity.olx.com.sv/vendo-ganga-iid-654251732','http://images01.olx-st.com/ui/21/28/23/1401704112_654332523_1-fotos-de-2-lotes-san-sebastian-salitrillo.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,78204195,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.96736595','-89.6343613',NULL,NULL,NULL,'false',NULL,NULL,22);
INSERT INTO "adverts" VALUES(134,'Vendo terreno en las faldas del volcan de San Salvador de gran plusvalia para Negocio',NULL,'VENDO UN TERRENO EN FALDAS DEL VOLCAN DE SAN SALVADOR DE GRAN PLUSVALIA, SOBRE EL KM 18 Y MEDIO, DE 1,125.00 Mts2, SOBRE LA CALLE PRINCIPAL, POR EL PRECIO DE $65,000 NEG PROPICIO PARA TODO TIPO DE NEGOCIO DE GRAN COMERCIO, Llamar AL 70900788 para MOSTRAR EL TERRENO.LLAMAR SOLO INTERESADOS
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,65000,'Terreno',NULL,'http://santatecla.olx.com.sv/vendo-casa-pinares-de-santa-monica-iid-654651761','http://images03.olx-st.com/ui/22/34/74/1401759536_654663074_1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,70900788,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6723347','-89.2902231',NULL,NULL,NULL,'false',NULL,NULL,76);
INSERT INTO "adverts" VALUES(135,'Se vende casa en campos verdes 1',NULL,'SE VENDE CASA EN CAMPOS VERDES 1, PASAJE PRIVADO, TRES CUARTOS, SALA COMEDOR, AREA DE COCINA Y LAVADERO, COCHERA TECHADA, PISO CERAMICO, ENCIELADA, CASA REMODELADAD, CON ARBOLES FRUTALES, BIEN SEGURA, AREA TRANQUILA, SE ACEPTAN COTIZANTES FSV Y OTROS TRAMITES, PRECIO NEGOCIABLE, INFORMACION AL TEL.
',NULL,NULL,3,NULL,NULL,NULL,'Venta',1,23000,'Apartamento',NULL,'http://santaanacity.olx.com.sv/vendo-ganga-iid-654251732','http://images04.olx-st.com/ui/10/10/32/1401710916_654380832_1-fotos-de-se-vende-casa-en-campos-verdes-1.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,73715922,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.7083268','-89.34829645',NULL,NULL,NULL,'false',NULL,NULL,79);
INSERT INTO "adverts" VALUES(136,'Se vende bonita casa en Urbanización San Dionisio',NULL,'Santa Ana, vendo bonita casa de 2 niveles, ubicada en Urbanización San Dionisio, zona sur, frente a Residencial Villa Real I, 3 habitaciones en Planta Alta, en primer nivel, sala y comedor separados, una habitación, pisos de cerámica, 2 baños, cochera para un vehículo y jardín al fondo.$ 65.000.-- NEGOCIABLE. Mayor información Tel. 79881255
',NULL,NULL,4,NULL,NULL,NULL,'Venta',2,65000,'Apartamento',NULL,'http://santaanacity.olx.com.sv/vendo-ganga-iid-654251732','http://images01.olx-st.com/ui/9/83/71/1401715215_654418271_1-se-vende-bonita-casa-en-urbanizacion-san-dionisio-urbanizacion-san-dionisio.jpg','OLX.com','3-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79881255,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.9837933','-89.5628214',NULL,NULL,NULL,'false',NULL,NULL,13);
INSERT INTO "adverts" VALUES(137,'Apartamento en San Benito con linea blanca',NULL,'Lindo apartamento en 5 piso en San Benito, 2 habitaciones con closets grandes , sala estudio, sala, comedor, cocina área de oficios lavadero y pila. Baño de empleada, refrigeradora,lavadora secadora, horno, cocina, microhonda
',NULL,NULL,2,NULL,NULL,NULL,'Alquiler',4,750,'Apartamento',NULL,'http://sansalvadorcity.olx.com.sv/alquilo-amplia-z-tres-torres-p-oficinas-875-iid-654998480','http://images04.olx-st.com/ui/9/43/96/1401812836_655003796_1-Pictures-of--Apartamento-en-San-Benito-con-linea-blanca.jpg','OLX.com','4-6-2014- 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79216212,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6914782','-89.21469395',NULL,NULL,NULL,'false',NULL,NULL,98);
INSERT INTO "adverts" VALUES(138,'Venta de terreno / lotes / propiedad',NULL,'Descripción:
- 2 lotes de de 8 x 22 metros cada uno
- a 5 minutos del lago de Ilopango en vehiculo
- terrenos amplios y planos
- listo para construir.
Mayor informacion tel 7439-0375, con Wenseslao Diaz
$6,000 cada lote, $12,000 los dos lotes juntos negociable.
',NULL,NULL,0,NULL,NULL,NULL,'Venta',0,12000,'Terreno',NULL,'http://santatecla.olx.com.sv/vendo-puesto-en-el-mercado-central-de-santa-tecla-iid-654148476','http://images02.olx-st.com/ui/22/88/76/1401677993_654178276_1-venta-de-terreno-lotes-propiedad-lotificacion-san-gerardo.jpg','OLX.com','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,74390375,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'13.6902395','-89.111048',NULL,NULL,NULL,'false',NULL,NULL,106);
COMMIT;
