# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140708154554) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adverts", force: true do |t|
    t.text     "description"
    t.text     "areadescription"
    t.text     "street"
    t.integer  "rooms"
    t.text     "address"
    t.text     "local"
    t.text     "negotationtype"
    t.integer  "bathroom"
    t.integer  "price"
    t.text     "housetype"
    t.text     "residence"
    t.text     "subtype"
    t.text     "originalsource"
    t.text     "urlimage"
    t.text     "originalsite"
    t.datetime "originaldate"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_file_updated_at"
    t.integer  "areamts"
    t.integer  "areavrs"
    t.integer  "availability"
    t.integer  "parking"
    t.integer  "construction"
    t.float    "areasize"
    t.boolean  "furnished"
    t.boolean  "pantry"
    t.boolean  "security"
    t.boolean  "garden"
    t.boolean  "nearbymalls"
    t.boolean  "kitchen"
    t.boolean  "preservation"
    t.boolean  "pool"
    t.boolean  "jacuzzi"
    t.boolean  "level"
    t.boolean  "basicservice"
    t.boolean  "cistern"
    t.boolean  "water"
    t.boolean  "heater"
    t.boolean  "air"
    t.boolean  "telephoneline"
    t.boolean  "visitorparking"
    t.boolean  "mezzanine"
    t.boolean  "recreation"
    t.boolean  "study"
    t.boolean  "terrace"
    t.boolean  "cellar"
    t.boolean  "parkland"
    t.text     "latitude"
    t.text     "longitude"
    t.text     "youtube1"
    t.text     "youtube2"
    t.text     "phone"
    t.text     "email"
    t.integer  "user_id"
    t.boolean  "ispublished"
    t.integer  "adverttype"
    t.boolean  "fromexcel"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "town_id"
    t.integer  "zone_id"
  end

  add_index "adverts", ["town_id"], name: "index_adverts_on_town_id", using: :btree
  add_index "adverts", ["user_id"], name: "index_adverts_on_user_id", using: :btree
  add_index "adverts", ["zone_id"], name: "index_adverts_on_zone_id", using: :btree

  create_table "alert_conditions", force: true do |t|
    t.text     "condition"
    t.text     "value"
    t.text     "secondary_value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_alert_id"
    t.text     "name"
  end

  add_index "alert_conditions", ["user_alert_id"], name: "index_alert_conditions_on_user_alert_id", using: :btree

  create_table "assets", force: true do |t|
    t.string   "asset_file_name"
    t.string   "asset_content_type"
    t.integer  "asset_file_size"
    t.datetime "asset_updated_at"
    t.integer  "advert_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
  end

  create_table "favorites", id: false, force: true do |t|
    t.integer  "advert_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mail_templates", force: true do |t|
    t.text     "body"
    t.text     "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.integer "user_id"
    t.integer "original_message_id"
    t.string  "subject"
    t.string  "body"
    t.boolean "sent"
  end

  create_table "notifications", force: true do |t|
    t.text     "negotationtype"
    t.integer  "price"
    t.integer  "rooms"
    t.integer  "bathrooms"
    t.integer  "pricefrom"
    t.integer  "priceto"
    t.text     "housetype"
    t.text     "url"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "photoadverts", force: true do |t|
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.string   "image_file_size"
    t.string   "image_updated_at"
  end

  create_table "reports", force: true do |t|
    t.integer  "user_id"
    t.integer  "advert_id"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.integer  "country_id"
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "towns", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.integer  "state_id"
  end

  add_index "towns", ["state_id"], name: "index_towns_on_state_id", using: :btree

  create_table "uploads", force: true do |t|
    t.string   "upload_file_name"
    t.string   "upload_content_type"
    t.integer  "upload_file_size"
    t.datetime "upload_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_alerts", force: true do |t|
    t.text     "url"
    t.text     "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "user_alerts", ["user_id"], name: "index_user_alerts_on_user_id", using: :btree

  create_table "userfbs", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "lastname"
    t.string   "email"
    t.string   "job"
    t.string   "password"
    t.text     "about"
    t.text     "reference"
    t.string   "phone"
    t.text     "mobile"
    t.boolean  "isclient"
    t.text     "fbid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.string   "image_file_size"
    t.string   "image_updated_at"
    t.text     "token"
  end

  create_table "zones", force: true do |t|
    t.text     "zonename"
    t.integer  "town_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "zones", ["town_id"], name: "index_zones_on_town_id", using: :btree

end
