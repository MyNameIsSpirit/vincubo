class CreateAlertConditions < ActiveRecord::Migration
  def change
    create_table :alert_conditions do |t|
      t.text :condition
      t.text :value
      t.text :secondary_value
      t.timestamps
    end
  end

end
