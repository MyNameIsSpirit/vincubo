class AddUserAlertRefToAlertConditions < ActiveRecord::Migration
  def change
    add_reference :alert_conditions, :user_alert, index: true
  end
end
