class AddCodeToTown < ActiveRecord::Migration
  def change
    add_column :towns, :code, :string
  end
end