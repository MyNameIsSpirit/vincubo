class AddStateRefToTown < ActiveRecord::Migration
  def change
    add_reference :towns, :state, index: true
  end
end