class AddUserRefToUserAlerts < ActiveRecord::Migration
  def change
    add_reference :user_alerts, :user, index: true
  end
end
