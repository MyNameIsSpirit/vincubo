class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :lastname
      t.string :email
      t.string :job
      t.string :password
      t.text :about
      t.text :reference
      t.string :phone
      t.text :mobile
      t.boolean :isclient
      t.text :fbid

      t.timestamps
    end
  end
end