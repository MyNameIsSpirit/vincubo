class AddConditionNameToAlertCondition < ActiveRecord::Migration
  def change
    add_column :alert_conditions, :name, :text
  end
end
