class AddImageToPhotoadvert < ActiveRecord::Migration
  def self.up
    add_column :photoadverts, :image_file_name, :string
    add_column :photoadverts, :image_content_type, :string
    add_column :photoadverts, :image_file_size, :string
    add_column :photoadverts, :image_updated_at, :string
  end

  def self.down
  remove_column :photoadverts, :image_file_name
    remove_column :photoadverts, :image_content_type
    remove_column :photoadverts, :image_file_size
    remove_column :photoadverts, :image_updated_at
  end
end