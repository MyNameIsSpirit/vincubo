class CreatePhotoadverts < ActiveRecord::Migration
  def change
    create_table :photoadverts do |t|
      t.text :comment

      t.timestamps
    end
  end
end