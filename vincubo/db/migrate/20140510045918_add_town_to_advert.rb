class AddTownToAdvert < ActiveRecord::Migration
  def change
    add_reference :adverts, :town, index: true
  end
end
