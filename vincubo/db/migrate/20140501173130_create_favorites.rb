class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites , id:false do |t|
      t.belongs_to :advert
      t.belongs_to :user
      t.timestamps
    end
  end
end