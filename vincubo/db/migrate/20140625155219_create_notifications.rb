class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|


    	t.text :negotationtype
		t.integer :price
		t.integer :rooms
		t.integer :bathrooms
		t.integer :pricefrom
		t.integer :priceto
		t.integer :pricefrom
		t.text :housetype
		t.text :url

		t.references :user, index: true

      t.timestamps
    end
  end
end
