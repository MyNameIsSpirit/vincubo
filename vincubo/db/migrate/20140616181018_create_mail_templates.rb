class CreateMailTemplates < ActiveRecord::Migration
  def change
    create_table :mail_templates do |t|
      t.text :body
      t.text :name

      t.timestamps
    end
  end
end
