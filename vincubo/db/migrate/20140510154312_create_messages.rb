class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :user_id
      t.integer :original_message_id, :null => true
      t.string :subject
      t.string :body
      t.boolean :sent
    end
  end
end
