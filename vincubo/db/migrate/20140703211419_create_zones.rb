class CreateZones < ActiveRecord::Migration
  def change
    create_table :zones do |t|
    	t.text :zonename
      	t.references :town, index: true
    	t.timestamps
    end
  end
end
