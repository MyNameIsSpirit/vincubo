class CreateAddZoneToAdverts < ActiveRecord::Migration
  def change
    add_reference :adverts, :zone, index: true
  end
end
