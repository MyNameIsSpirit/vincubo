class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      #t.text :title
      #t.text :features
      t.text :description
      t.text :areadescription
      t.text :street
      t.integer :rooms
      t.text :address
      t.text :local
      #t.text :aditionaladdress
      t.text :negotationtype
      t.integer :bathroom
      t.integer :price
      t.text :housetype
      t.text :residence
      
      t.text :subtype
      t.text :originalsource
      t.text :urlimage
      t.text :originalsite
      t.datetime :originaldate
      t.string :image_file_name
      t.string :image_content_type
      t.integer :image_file_size
      t.datetime :image_file_updated_at

      t.integer :areamts
      t.integer :areavrs
      t.integer :availability
      t.integer :parking
      t.integer :construction
      t.float :areasize

      t.boolean :furnished
      t.boolean :pantry 
      t.boolean :security 
      t.boolean :garden 
      t.boolean :nearbymalls 
      t.boolean :kitchen 
      t.boolean :preservation 
      t.boolean :pool 
      t.boolean :jacuzzi 
      t.boolean :level 
      #t.boolean :othergeneral
      t.boolean :basicservice 
      t.boolean :cistern
      t.boolean :water 
      t.boolean :heater 
      t.boolean :air 
      t.boolean :telephoneline 
      #t.boolean :otherservices 
      t.boolean :visitorparking 
      t.boolean :mezzanine 
      t.boolean :recreation 
      #t.boolean :otherservice 
      
      t.boolean :study 
      t.boolean :terrace 
      t.boolean :cellar 
      t.boolean :parkland 

      t.text :latitude
      t.text :longitude
      #t.boolean :other

      t.text :youtube1
      t.text :youtube2
      
      t.text :phone
      t.text :email

      t.references :user, index: true
      t.boolean :ispublished

      t.integer :adverttype
      t.boolean :fromexcel

      

      t.timestamps
    end
  end
end