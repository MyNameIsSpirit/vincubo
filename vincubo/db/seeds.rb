#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


c = Country.create({name:'El Salvador',code:'SV'})

s = State.create({country:c,name:'Ahuachapán',code:'AH'})

Town.create([
	{state:s,name:'Ahuachapán', code:'101'},
		{state:s,name:'Apaneca', code:'102'},
		{state:s,name:'Atiquizaya', code:'103'},
		{state:s,name:'Concepción de Ataco', code:'104'},
		{state:s,name:'El Refugio', code:'105'},
		{state:s,name:'Guaymango', code:'106'},
		{state:s,name:'Jujutla', code:'107'},
		{state:s,name:'San Francisco Menéndez', code:'108'},
		{state:s,name:'San Lorenzo', code:'109'},
		{state:s,name:'San Pedro Puxtla', code:'110'},
		{state:s,name:'Tacuba', code:'111'},
		{state:s,name:'Turín', code:'112'}
])


s = State.create({country:c,name:'Santa Ana',code:'SA'})
Town.create([
	{state:s,name:'Santa Ana', code:'201'},
	{state:s,name:'Candelaria de la Frontera', code:'202'},
	{state:s,name:'Coatepeque', code:'203'},
	{state:s,name:'Chalchuapa', code:'204'},
	{state:s,name:'El Congo', code:'205'},
	{state:s,name:'El Porvenir', code:'206'},
	{state:s,name:'Masahuat', code:'207'},
	{state:s,name:'Metapán', code:'208'},
	{state:s,name:'San Antonio Pajonal', code:'209'},
	{state:s,name:'San Sebastián Salitrillo', code:'210'},
	{state:s,name:'Santa Rosa Guachipilín', code:'211'},
	{state:s,name:'Santiago de la Frontera', code:'212'},
	{state:s,name:'Texistepeque', code:'213'}
])


s = State.create({country:c,name:'Sonsonate',code:'SO'})
Town.create([
	{state:s,name:'Sonsonate', code:'301'},
	{state:s,name:'Acajutla', code:'302'},
	{state:s,name:'Armenia', code:'303'},
	{state:s,name:'Caluco', code:'304'},
	{state:s,name:'Cuisnahuat', code:'305'},
	{state:s,name:'Izalco', code:'306'},
	{state:s,name:'Juayúa', code:'307'},
	{state:s,name:'Nahuizalco', code:'308'},
	{state:s,name:'Nahulingo', code:'309'},
	{state:s,name:'Salcoatitán', code:'310'},
	{state:s,name:'San Antonio del Monte', code:'311'},
	{state:s,name:'San Julián', code:'312'},
	{state:s,name:'Santa Catarina Masahuat', code:'313'},
	{state:s,name:'Santa Isabel Ishuatán', code:'314'},
	{state:s,name:'Santo Domingo de Guzmán', code:'315'},
	{state:s,name:'Sonzacate', code:'316'}
])


s = State.create({country:c,name:'Chalatenango',code:'CH'})
Town.create([
	{state:s,name:'Chalatenango', code:'401'},
	{state:s,name:'Agua Caliente', code:'402'},
	{state:s,name:'Arcatao', code:'403'},
	{state:s,name:'Azacualpa', code:'404'},
	{state:s,name:'San José Cancasque', code:'405'},
	{state:s,name:'Citalá', code:'406'},
	{state:s,name:'Comalapa', code:'407'},
	{state:s,name:'Concepción Quezaletepeque', code:'408'},
	{state:s,name:'Dulce Nombre de María', code:'409'},
	{state:s,name:'El Carrizal', code:'410'},
	{state:s,name:'El Paraíso', code:'411'},
	{state:s,name:'La Laguna', code:'412'},
	{state:s,name:'La Palma', code:'413'},
	{state:s,name:'La Reina', code:'414'},
	{state:s,name:'San José Las Flores', code:'415'},
	{state:s,name:'Las Vueltas', code:'416'},
	{state:s,name:'Nombre de Jesús', code:'417'},
	{state:s,name:'Nueva Concepción', code:'418'},
	{state:s,name:'Nueva Trinidad', code:'419'},
	{state:s,name:'Ojos de Agua', code:'420'},
	{state:s,name:'Potonico', code:'421'},
	{state:s,name:'San Antonio de La Cruz', code:'422'},
	{state:s,name:'San Antonio los Ranchos', code:'423'},
	{state:s,name:'San Fernando', code:'424'},
	{state:s,name:'San Francisco Lempa', code:'425'},
	{state:s,name:'San Francisco Morazán', code:'426'},
	{state:s,name:'San Ignacio', code:'427'},
	{state:s,name:'San Isidro Labrador', code:'428'},
	{state:s,name:'San Luis del Carmen', code:'429'},
	{state:s,name:'San Miguel de Mercedes', code:'430'},
	{state:s,name:'San Rafael', code:'431'},
	{state:s,name:'Santa Rita', code:'432'},
	{state:s,name:'Tejutla', code:'433'}
])



s = State.create({country:c,name:'La Libertad',code:'LL'})
Town.create([
	{state:s, name: 'La Libertad', code: '510'},
	{state:s, name: 'Santa Tecla', code: '501'},
	{state:s, name: 'Antiguo Cuscatlán', code: '502'},
	{state:s, name: 'Ciudad Arce', code: '503'},
	{state:s, name: 'Colón', code: '504'},
	{state:s, name: 'Comasagua', code: '505'},
	{state:s, name: 'Chiltiupán', code: '506'},
	{state:s, name: 'Huizúcar', code: '507'},
	{state:s, name: 'Jayaque', code: '508'},
	{state:s, name: 'Jicalapa', code: '509'},
	{state:s, name: 'Nuevo Cuscatlán', code: '511'},
	{state:s, name: 'San Juan Opico', code: '512'},
	{state:s, name: 'Quezaltepeque', code: '513'},
	{state:s, name: 'Sacacoyo', code: '514'},
	{state:s, name: 'San José Villanueva', code: '515'},
	{state:s, name: 'San Matías', code: '516'},
	{state:s, name: 'San Pablo Tacachico', code: '517'},
	{state:s, name: 'Talnique', code: '518'},
	{state:s, name: 'Tamanique', code: '519'},
	{state:s, name: 'Teotepeque', code: '520'},
	{state:s, name: 'Tepecoyo', code: '521'},
	{state:s, name: 'Zaragoza', code: '522'}
])


s = State.create({country:c,name:'San Salvador',code:'SS'})
Town.create([
	{state:s, name: 'Panchimalco', code: '612'},
	{state:s, name: 'San Salvador', code: '601'},
	{state:s, name: 'Aguilares', code: '602'},
	{state:s, name: 'Apopa', code: '603'},
	{state:s, name: 'Ayutuxtepeque', code: '604'},
	{state:s, name: 'Cuscatancingo', code: '605'},
	{state:s, name: 'Delgado', code: '606'},
	{state:s, name: 'El Paisnal', code: '607'},
	{state:s, name: 'Guazapa', code: '608'},
	{state:s, name: 'Ilopango', code: '609'},
	{state:s, name: 'Mejicanos', code: '610'},
	{state:s, name: 'Nejapa', code: '611'},
	{state:s, name: 'Rosario de Mora', code: '613'},
	{state:s, name: 'San Marcos', code: '614'},
	{state:s, name: 'San Martín', code: '615'},
	{state:s, name: 'Santiago Texacuangos', code: '616'},
	{state:s, name: 'Santo Tomás', code: '617'},
	{state:s, name: 'Soyapango', code: '618'},
	{state:s, name: 'Tonacatepeque', code: '619'}
])



s = State.create({country:c,name:'Cuscatlán',code:'CU'})
Town.create([
	{state:s, name: 'Cojutepeque', code:'701'},
	{state:s, name: 'Candelaria', code: '702'},
	{state:s, name: 'El Carmen', code:'703'},
	{state:s, name: 'El Rosario', code:'704'},
	{state:s, name: 'Monte San Juan', code:'705'},
	{state:s, name: 'Oratorio de Concepción', code:'706'},
	{state:s, name: 'San Bartolomé Perulapía', code:'707'},
	{state:s, name: 'San Cristóbal', code:'708'},
	{state:s, name: 'San José Guayabal', code:'709'},
	{state:s, name: 'San Pedro Perulapán', code:'710'},
	{state:s, name: 'San Rafael Cedros', code:'711'},
	{state:s, name: 'San Ramón', code:'712'},
	{state:s, name: 'Santa Cruz Analquito', code:'713'},
	{state:s, name: 'Santa Cruz Michapa', code:'714'},
	{state:s, name: 'Suchitoto', code:'715'},
	{state:s, name: 'Tenancingo', code:'716'}
])


s = State.create({country:c,name:'La Paz',code:'LP'})
Town.create([
	{state:s, name: 'Zacatecoluca', code: '801'},
	{state:s, name: 'Cuyultitán', code: '802'},
	{state:s, name: 'Rosario de La Paz', code: '803'},
	{state:s, name: 'Jerusalén', code: '804'},
	{state:s, name: 'Mercedes La Ceiba', code: '805'},
	{state:s, name: 'Olocuilta', code: '806'},
	{state:s, name: 'Paraíso de Osorio', code: '807'},
	{state:s, name: 'San Antonio Masahuat', code: '808'},
	{state:s, name: 'San Emigdio', code: '809'},
	{state:s, name: 'San Francisco Chinameca', code: '810'},
	{state:s, name: 'San Juan Nonualco', code: '811'},
	{state:s, name: 'San Juan Talpa', code: '812'},
	{state:s, name: 'San Juan Tepezontes', code: '813'},
	{state:s, name: 'San Luis Talpa', code: '814'},
	{state:s, name: 'San Luis la Herradura', code: '815'},
	{state:s, name: 'San Miguel Tepezontes', code: '816'},
	{state:s, name: 'San Pedro Masahuat', code: '817'},
	{state:s, name: 'San Pedro Nonualco', code: '818'},
	{state:s, name: 'San Rafael Obrajuelo', code: '819'},
	{state:s, name: 'Santa María Ostuma', code: '820'},
	{state:s, name: 'Santiago Nonualco', code: '821'},
	{state:s, name: 'Tapalhuaca', code: '822'},
])



s = State.create({country:c,name:'Cabañas',code:'CA'})
Town.create([
	{state:s, name: 'Sensuntepeque', code:'901'},
	{state:s, name: 'Cinquera', code:'902'},
	{state:s, name: 'Dolores', code:'903'},
	{state:s, name: 'Guacotecti', code:'904'},
	{state:s, name: 'Ilobasco', code:'905'},
	{state:s, name: 'Jutiapa', code:'906'},
	{state:s, name: 'San', code:'Isidro	907'},
	{state:s, name: 'Tejutepeque', code:'908'},
	{state:s, name: 'Victoria', code:'909'}
])


s = State.create({country:c,name:'San Vicente',code:'SV'})
Town.create([
	{state:s, name: 'San Vicente',code:'1001'},
	{state:s, name: 'Apastepeque',code:'1002'},
	{state:s, name: 'Guadalupe',code:'1003'},
	{state:s, name: 'San Cayetano Istepeque',code:'1004'},
	{state:s, name: 'San Esteban Catarina',code:'1005'},
	{state:s, name: 'San Ildefonso',code:'1006'},
	{state:s, name: 'San Lorenzo',code:'1007'},
	{state:s, name: 'San Sebastián',code:'1008'},
	{state:s, name: 'Santa Clara',code:'1009'},
	{state:s, name: 'Santo Domingo',code:'1010'},
	{state:s, name: 'Tecoluca',code:'1011'},
	{state:s, name: 'Tepetitán',code:'1012'},
	{state:s, name: 'Verapaz',code:'1013'}
])


s = State.create({country:c,name:'Usulután',code:'US'})
Town.create([
	{state:s, name: 'Usulután', code:'1101'},
	{state:s, name: 'Alegría', code:'1102'},
	{state:s, name: 'Berlín', code:'1103'},
	{state:s, name: 'California', code:'1104'},
	{state:s, name: 'Concepción Batres', code:'1105'},
	{state:s, name: 'Villa El Triunfo', code:'1106'},
	{state:s, name: 'Ereguayquín', code:'1107'},
	{state:s, name: 'Estanzuelas', code:'1108'},
	{state:s, name: 'Jiquilisco', code:'1109'},
	{state:s, name: 'Jucuapa', code:'1110'},
	{state:s, name: 'Jucuarán', code:'1111'},
	{state:s, name: 'Mercedes Umaña', code:'1112'},
	{state:s, name: 'Nueva Granada', code:'1113'},
	{state:s, name: 'Ozatlán', code:'1114'},
	{state:s, name: 'Puerto El Triunfo', code:'1115'},
	{state:s, name: 'San Agustín', code:'1116'},
	{state:s, name: 'San Buenaventura', code:'1117'},
	{state:s, name: 'San Dionisio', code:'1118'},
	{state:s, name: 'San Francisco Javier', code:'1119'},
	{state:s, name: 'Santa Elena', code:'1120'},
	{state:s, name: 'Santa María', code:'1121'},
	{state:s, name: 'Santiago de María', code:'1122'},
	{state:s, name: 'Tecapán', code:'1123'}
])


s = State.create({country:c,name:'San Miguel',code:'SM'})
Town.create([
	{state:s, name: 'San Miguel', code:'1201'},
	{state:s, name: 'Carolina', code:'1202'},
	{state:s, name: 'Ciudad Barrios', code:'1203'},
	{state:s, name: 'Comacarán', code:'1204'},
	{state:s, name: 'Chapeltique', code:'1205'},
	{state:s, name: 'Chinameca', code:'1206'},
	{state:s, name: 'Chirilagua', code:'1207'},
	{state:s, name: 'El Tránsito', code:'1208'},
	{state:s, name: 'Lolotique', code:'1209'},
	{state:s, name: 'Moncagua', code:'1210'},
	{state:s, name: 'Nueva Guadalupe', code:'1211'},
	{state:s, name: 'Nuevo Edén de San Juan', code:'1212'},
	{state:s, name: 'Quelepa', code:'1213'},
	{state:s, name: 'San Antonio', code:'1214'},
	{state:s, name: 'San Gerardo', code:'1215'},
	{state:s, name: 'San Jorge', code:'1216'},
	{state:s, name: 'San Luis de la Reina', code:'1217'},
	{state:s, name: 'San Rafael Oriente', code:'1218'},
	{state:s, name: 'Sesori', code:'1219'},
	{state:s, name: 'Uluazapa', code:'1220'}
])

s = State.create({country:c,name:'Morazán',code:'MO'})
Town.create([
	{state:s, name: 'San Francisco Gotera', code:'1301'},
	{state:s, name: 'Arambala', code:'1302'},
	{state:s, name: 'Cacaopera', code:'1303'},
	{state:s, name: 'Corinto', code:'1304'},
	{state:s, name: 'Chilanga', code:'1305'},
	{state:s, name: 'Delicias de Concepción', code:'1306'},
	{state:s, name: 'El Divisadero', code:'1307'},
	{state:s, name: 'El Rosario', code:'1308'},
	{state:s, name: 'Gualococti', code:'1309'},
	{state:s, name: 'Guatajiagua', code:'1310'},
	{state:s, name: 'Joateca', code:'1311'},
	{state:s, name: 'Jocoaitique', code:'1312'},
	{state:s, name: 'Jocoro', code:'1313'},
	{state:s, name: 'Lolotiquillo', code:'1314'},
	{state:s, name: 'Meanguera', code:'1315'},
	{state:s, name: 'Osicala', code:'1316'},
	{state:s, name: 'Perquín', code:'1317'},
	{state:s, name: 'San Carlos', code:'1318'},
	{state:s, name: 'San Fernando', code:'1319'},
	{state:s, name: 'San Isidro', code:'1320'},
	{state:s, name: 'San Simón', code:'1321'},
	{state:s, name: 'Sensembra', code:'1322'},
	{state:s, name: 'Sociedad', code:'1323'},
	{state:s, name: 'Torola', code:'1324'},
	{state:s, name: 'Yamabal', code:'1325'},
	{state:s, name: 'Yoloaiquín', code:'1326'}
])

s = State.create({country:c,name:'La Unión',code:'LU'})
Town.create([
	{state:s, name: 'La Unión', code:'1401'},
	{state:s, name: 'Anamorós', code:'1402'},
	{state:s, name: 'Bolívar', code:'1403'},
	{state:s, name: 'Concepción de Oriente', code:'1404'},
	{state:s, name: 'Conchagua', code:'1405'},
	{state:s, name: 'El Carmen', code:'1406'},
	{state:s, name: 'El Sauce', code:'1407'},
	{state:s, name: 'Intipucá', code:'1408'},
	{state:s, name: 'Lislique', code:'1409'},
	{state:s, name: 'Meanguera del Golfo', code:'1410'},
	{state:s, name: 'Nueva Esparta', code:'1411'},
	{state:s, name: 'Pasaquina', code:'1412'},
	{state:s, name: 'Polorós', code:'1413'},
	{state:s, name: 'San Alejo', code:'1414'},
	{state:s, name: 'San José La Fuente', code:'1415'},
	{state:s, name: 'Santa Rosa de Lima', code:'1416'},
	{state:s, name: 'Yayantique', code:'1417'},
	{state:s, name: 'Yucuaiquín', code:'1418'}
])


Zone.create([
	{town_id: 99, zonename:	'Aguilares'},
	{town_id: 100, zonename:	'Apopa'},
	{town_id: 100, zonename:	'Colonia Ciudad Futura'},
	{town_id: 100, zonename:	'Reparto Santa Marta'},
	{town_id: 101, zonename:	'Ayutuxtepeque'},
	{town_id: 101, zonename:	'Residencial Santisima Trinidad'},
	{town_id: 103, zonename:	'Ciudad Delgado'},
	{town_id: 103, zonename:	'Colonia Atlacatl'},
	{town_id: 102, zonename:	'Cuscatancingo'},
	{town_id: 102, zonename:	'Sisimiles'},
	{town_id: 102, zonename:	'Ubanizacion Lirios del Norte I'},
	{town_id: 102, zonename:	'Urbanizacion Lirios del Norte II'},
	{town_id: 102, zonename:	'Calle a Mariona'},
	{town_id: 105, zonename:	'Guazapa'},
	{town_id: 106, zonename:	'Boulevard San Bartolo'},
	{town_id: 106, zonename:	'Colonia La Campiña'},
	{town_id: 106, zonename:	'Ilopango'},
	{town_id: 106, zonename:	'Residencial Altavista'},
	{town_id: 107, zonename:	'Boulevard Constitucion'},
	{town_id: 107, zonename:	'Colonia Ciudad Satelite'},
	{town_id: 107, zonename:	'Colonia España'},
	{town_id: 107, zonename:	'Colonia Libertad'},
	{town_id: 107, zonename:	'Colonia Miralvalle'},
	{town_id: 107, zonename:	'Colonia Zacamil'},
	{town_id: 107, zonename:	'Complejo Residencial Australia'},
	{town_id: 107, zonename:	'Condominio Montemaria'},
	{town_id: 107, zonename:	'Mejicanos'},
	{town_id: 107, zonename:	'Reparto Porticos de San Ramon'},
	{town_id: 107, zonename:	'Residencial La Esperanza'},
	{town_id: 107, zonename:	'Residencial Miralvalle Sur'},
	{town_id: 107, zonename:	'Residencial Prados de Miralvalle'},
	{town_id: 107, zonename:	'Residencial San Jose Montebello'},
	{town_id: 107, zonename:	'Urbanizacion Metropolis'},
	{town_id: 107, zonename:	'Urbanizacion La Gloria'},
	{town_id: 107, zonename:	'Urbanizacion La Gloria Canton San Roque'},
	{town_id: 108, zonename:	'Lotificacion La Granjita'},
	{town_id: 108, zonename:	'Metropoli San Gabriel'},
	{town_id: 108, zonename:	'Nejapa'},
	{town_id: 97, zonename:	'Panchimalco'},
	{town_id: 110, zonename:	'Calle San Marcos'},
	{town_id: 110, zonename:	'Calle Santorini'},
	{town_id: 110, zonename:	'Ciudad Dorada'},
	{town_id: 110, zonename:	'Colonia Las Campanitas'},
	{town_id: 110, zonename:	'San Marcos'},
	{town_id: 110, zonename:	'San Martín'},
	{town_id: 98, zonename:	'Alameda Franklin Delano Roosevelt'},
	{town_id: 98, zonename:	'Altamira'},
	{town_id: 98, zonename:	'Autopista Sur'},
	{town_id: 98, zonename:	'Barrio Belen'},
	{town_id: 98, zonename:	'Barrio La Vega'},
	{town_id: 98, zonename:	'Barrio San Jacinto'},
	{town_id: 98, zonename:	'Boulevar de Los Heroes'},
	{town_id: 98, zonename:	'Boulevar del Ejercito Nacional'},
	{town_id: 98, zonename:	'Boulevard Los Proceres'},
	{town_id: 98, zonename:	'Boulevard Venezuela'},
	{town_id: 98, zonename:	'Brisas de Candelaria'},
	{town_id: 98, zonename:	'Calle a Planes de Renderos'},
	{town_id: 98, zonename:	'Centro Comercial Feria Rosa'},
	{town_id: 98, zonename:	'Centro de Gobierno'},
	{town_id: 98, zonename:	'Centro de San Salvador'},
	{town_id: 98, zonename:	'Centro Urbano Candelaria'},
	{town_id: 98, zonename:	'Centro Urbano Monserrat'},
	{town_id: 98, zonename:	'Colonia 5 de Noviembre'},
	{town_id: 98, zonename:	'Colonia Avila'},
	{town_id: 98, zonename:	'Colonia Buenos Aires'},
	{town_id: 98, zonename:	'Colonia Centroamerica'},
	{town_id: 98, zonename:	'Colonia Costa Rica'},
	{town_id: 98, zonename:	'Colonia Cucumacayan'},
	{town_id: 98, zonename:	'Colonia El Rosal'},
	{town_id: 98, zonename:	'Colonia Escalon'},
	{town_id: 98, zonename:	'Colonia Ferrocarril'},
	{town_id: 98, zonename:	'Colonia Flor Blanca'},
	{town_id: 98, zonename:	'Colonia Guatemala'},
	{town_id: 98, zonename:	'Colonia IVU'},
	{town_id: 98, zonename:	'Colonia La Rabida'},
	{town_id: 98, zonename:	'Colonia Layco'},
	{town_id: 98, zonename:	'Colonia Malaga'},
	{town_id: 98, zonename:	'Colonia Maquilishuat'},
	{town_id: 98, zonename:	'Colonia Medica'},
	{town_id: 98, zonename:	'Colonia Miramonte'},
	{town_id: 98, zonename:	'Colonia Modelo'},
	{town_id: 98, zonename:	'Colonia Palacios'},
	{town_id: 98, zonename:	'Colonia Roma'},
	{town_id: 98, zonename:	'Colonia San Benito'},
	{town_id: 98, zonename:	'Colonia San Francisco'},
	{town_id: 98, zonename:	'Colonia San Martin'},
	{town_id: 98, zonename:	'Colonia San Mateo'},
	{town_id: 98, zonename:	'Colonia Santa Victoria'},
	{town_id: 98, zonename:	'Colonia Tazumal'},
	{town_id: 98, zonename:	'Colonia Vista Hermosa'},
	{town_id: 98, zonename:	'Condominio Holanda 2000'},
	{town_id: 98, zonename:	'Distrito El Espino'},
	{town_id: 98, zonename:	'Estadio Cuscatlan'},
	{town_id: 98, zonename:	'La Sultana'},
	{town_id: 98, zonename:	'Las Pulgas Los Heroes'},
	{town_id: 98, zonename:	'Lomas de San Francisco'},
	{town_id: 98, zonename:	'Mercado Central'},
	{town_id: 98, zonename:	'Metrocentro'},
	{town_id: 98, zonename:	'Monumento Divino Salvador del Mundo'},
	{town_id: 98, zonename:	'Pje. Conchalio'},
	{town_id: 98, zonename:	'Reparto Los Heroes'},
	{town_id: 98, zonename:	'Reparto Maquilishuat'},
	{town_id: 98, zonename:	'Residencial Loma Linda'},
	{town_id: 98, zonename:	'Residencial San Luis'},
	{town_id: 98, zonename:	'Residencial Terranova'},
	{town_id: 98, zonename:	'San Antonio Abad'},
	{town_id: 98, zonename:	'San Miguelito'},
	{town_id: 98, zonename:	'San Ramon'},
	{town_id: 98, zonename:	'San Salvador'},
	{town_id: 98, zonename:	'Santa Lucia'},
	{town_id: 98, zonename:	'Terminal de Oriente'},
	{town_id: 98, zonename:	'Urbanizacion Altos del Boulevard'},
	{town_id: 98, zonename:	'Urbanización La Mascota'},
	{town_id: 98, zonename:	'Urbanizacion Lomas de Altamira'},
	{town_id: 98, zonename:	'Villas de San Patricio'},
	{town_id: 98, zonename:	'Zona UCA'},
	{town_id: 98, zonename:	'Zona UES'},
	{town_id: 112, zonename:	'Santiago Texacuangos'},
	{town_id: 113, zonename:	'Santo Tomás'},
	{town_id: 114, zonename:	'Soyapango'},
	{town_id: 114, zonename:	'Colonia Amatepec'},
	{town_id: 114, zonename:	'Pje Arce'},
	{town_id: 114, zonename:	'Urbanizacion Sierra Morena 1'},
	{town_id: 115, zonename:	'Tonacatepeque'}
]);


MailTemplate.create([{body:'',name:'welcome_user'}])



