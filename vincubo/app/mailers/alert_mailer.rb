class AlertMailer < ActionMailer::Base
  default from: "from@example.com"
  layout 'user_mailer_layout'
  def mail_newsletter(user,adverts)
    @user = user
    @adverts = adverts
    mail :to => user.email, :subject => "Alertas Vincubo"
  end

end
