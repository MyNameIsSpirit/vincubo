#encoding: utf-8
class UserMailer < ActionMailer::Base
  default from: "from@example.com"
  layout 'user_mailer_layout'
  def preferences_changed(user,adverts)
    @adverts = adverts
    @user = user
    mail :to => user.email, :subject => "Cambio de preferencias Vincubo"
  end
  def password_changed(user)
    @user = user
    mail :to => user.email, :subject => "Cambio de contraseña Vincubo"
  end
  def information_changed(user)
    @user = user
    mail :to => user.email, :subject => "Edición de información Vincubo"
  end
  def password_change_confirmation(user)
    @user = user
    mail :to => user.email, :subject => "Confirmación de cambio de Contraseña Vincubo"
  end
end
