class WelcomeMailer < ActionMailer::Base
  default from: "from@example.com"
  layout 'welcome_mailer_layout'
  def consumer_registration(user)
    @user = user
    mail :to => user.email, :subject => "Bienvenido a Vincubo!"
  end
  def user_registration(user)
    @user = user
    mail :to => user.email, :subject => "Bienvenido a Vincubo!"
  end
end
