var mueblesdata = [
				{
					value: 80,
					color:"#808080"
				},
				{
					value : 25,
					color : "#B3B3B3"
				},
				{
					value : 20,
					color : "#E6E6E6"
				},
				{
					value : 15,
					color : "#ffffff"
				}
			
			];
			var mueblesoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#808080",
				segmentStrokeWidth : 1,
			}
			var myDoughnut = new Chart(document.getElementById("muebles").getContext("2d")).Doughnut(mueblesdata,mueblesoptions);

			var propiedadesdata = [
				{
					value: 77,
					color:"#ffffff"
				},
				{
					value : 23,
					color : "#FF7F5E"
				}
			
			];
			var propiedadesoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#FF7F5E",
				segmentStrokeWidth : 1,
			}
			var myDoughnut = new Chart(document.getElementById("propiedades").getContext("2d")).Doughnut(propiedadesdata,propiedadesoptions);

			var zonasdata = [
				{
					value: 40,
					color:"#808080"
				},
				{
					value : 25,
					color : "#B3B3B3"
				},
				{
					value : 20,
					color : "#E6E6E6"
				},
				{
					value : 15,
					color : "#ffffff"
				}
			
			];
			var zonasoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#808080",
				segmentStrokeWidth : 1,

			}
			var myDoughnut = new Chart(document.getElementById("zonas").getContext("2d")).Doughnut(zonasdata,zonasoptions);


			var line1data = {
				//labels : ["Jul.","Ago."],
				labels : $("#tabla1 p").text().split("&"),
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						//data : [60]
						data: $("#tabla1 c").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(34,177,76,0.5)",
						strokeColor : "rgba(34,177,76,1)",
						pointColor : "rgba(34,177,76,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla1 o").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(153,217,234,1)",
						pointColor : "rgba(153,217,234,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla1 t").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(142, 213, 87,1)",
						pointColor : "rgba(142, 213, 87,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla1 b").text().split("&").slice(0, -1),
					}
				]
			}


			var line2data = {
				//labels : ["Jul.","Ago."],
				labels : $("#tabla2 p").text().split("&"),
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						//data : [60]
						data: $("#tabla2 c").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(34,177,76,0.5)",
						strokeColor : "rgba(34,177,76,1)",
						pointColor : "rgba(34,177,76,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla2 o").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(153,217,234,1)",
						pointColor : "rgba(153,217,234,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla2 t").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(142, 213, 87,1)",
						pointColor : "rgba(142, 213, 87,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla2 b").text().split("&").slice(0, -1),
					}
				]
			}

			var line3data = {
				//labels : ["Jul.","Ago."],
				labels : $("#tabla3 p").text().split("&"),
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						//data : [60]
						data: $("#tabla3 c").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(34,177,76,0.5)",
						strokeColor : "rgba(34,177,76,1)",
						pointColor : "rgba(34,177,76,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla3 o").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(153,217,234,1)",
						pointColor : "rgba(153,217,234,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla3 t").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(142, 213, 87,1)",
						pointColor : "rgba(142, 213, 87,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla3 b").text().split("&").slice(0, -1),
					}
				]
			}

			var line4data = {
				//labels : ["Jul.","Ago."],
				labels : $("#tabla4 p").text().split("&"),
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						//data : [60]
						data: $("#tabla4 c").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(34,177,76,0.5)",
						strokeColor : "rgba(34,177,76,1)",
						pointColor : "rgba(34,177,76,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla4 o").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(153,217,234,1)",
						pointColor : "rgba(153,217,234,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla4 t").text().split("&").slice(0, -1),
					},
					{
						fillColor : "rgba(153,217,234,0.5)",
						strokeColor : "rgba(142, 213, 87,1)",
						pointColor : "rgba(142, 213, 87,1)",
						pointStrokeColor : "#fff",
						data: $("#tabla4 b").text().split("&").slice(0, -1),
					}
				]
			}


			


			var line2options = {
				bezierCurve : false,
				datasetFill : false,
			}
				

			var linedata = {
				labels : ["Jul.","Ago."],
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						data : [60,0]
					}
				]
			}
			var lineoptions = {
				bezierCurve : false,
				datasetFill : false,
			}
				
				
				