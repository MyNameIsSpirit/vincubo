var geocoder;
var map;
var markers = [];
var markerCluster;

var opt = {
  autoOpen: false,
  modal: true,
  title: '',
  height: 400,
  position: 'center',
  draggable: false,
  open: function(event,ui){
        $('.noOverlayDialog').next('div').css( {'opacity':0.0} );

        $dialogOverlay = $(".ui-widget-overlay").last();
            $dialogOverlay.unbind();
            $dialogOverlay.click(function(){
                $("#advertInfo").dialog("close");
            });

      },
};

var optDialog = {
    url: '/login',
    autoOpen: false,
    modal: true,
    width: 330,
    heigth: 600,
      title: '',
      draggable: false,
      dialogClass: 'dlgfixed',
      position: 'center',
      open:function(){
            $dialogOverlay = $(".ui-widget-overlay").last();
            $dialogOverlay.unbind();
            $dialogOverlay.click(function(){
                $("#targetDiv").dialog("close");
                $("body").css("overflow","");
                //$(".dlgfixed").center(false);
            });
        }
  };

function initialize(callback) {
  geocoder = new google.maps.Geocoder();
  var mapOptions = {
    zoom: 9,
    minZoom: 9,
    maxZoom: 25,
    center: new google.maps.LatLng(13.82913525527803, -88.84561441484375)
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  callback(false);
}

function initializeAlone() {
  geocoder = new google.maps.Geocoder();
  var mapOptions = {
    zoom: 9,
    minZoom: 9,
    maxZoom: 25,
    center: new google.maps.LatLng(13.82913525527803, -88.84561441484375)
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  //callback(false);
}

function clearOverlays() {
  for (var i = 0; i < markers.length; i++ ) {
    if(markers[i] != undefined)
      markers[i].setMap(null);
  }
  markers.length = 0;
}

function codeAddress(latitude, longitude, address ) {
  clearOverlays();
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          icon: "/images/marker1.png"	  
      });
  	  var lat = marker.getPosition().lat();
  	  var lng = marker.getPosition().lng();
      $(latitude).val(lat);
      $(longitude).val(lng);
    }
    markers.push(marker);
  });
}

function codeAddressByCoordinates(latitude, longitude, ids, type){
  clearOverlays();
  try{
    markerCluster.clearMarkers();
  }catch(e){
  }

  for(var i = 0; i < latitude.length; i++ ){
    var icon = "/images/marker1.png";
    var mcicon = "/images/sellmarker.png";
    if(type[i] == "Alquiler"){
      icon = "/images/markerGreen.png"
      
    }

    if($("#negotationtype").val() == "Alquiler"  ){
      mcicon = "/images/rentmarker.png";
      icon = "/images/markerGreen.png"
    }

    var myLatlng = new google.maps.LatLng(latitude[i], longitude[i]);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: '',
        id: ids[i],
        icon: icon
    });
    markers.push(marker);
    google.maps.event.addListener(marker, 'click', function() {
      var temp = []; 
      temp.push(this);
      showInfo(temp); 
    });
  }

  mcOptions = {
    styles: 
    [
      {
        height: 60, url: mcicon, width: 57, textColor: 'white'
      },
      {
        height: 60, url: mcicon, width: 55, textColor: 'white'
      },
      {
        height: 60, url: mcicon, width: 60, textColor: 'white'
      },
      {
        height: 60, url: mcicon, width: 60, textColor: 'white'
      },
      {
        height: 60, url: mcicon, width: 25, textColor: 'white'
      }
      ]
    }

  markerCluster = new MarkerClusterer(map, markers, mcOptions);
  markerCluster.resetViewport();
  markerCluster.redraw();

  google.maps.event.addListener(markerCluster, 'clusterclick', function (mCluster) {
    showInfo(mCluster.getMarkers());
  });
}


function showInfo(currentMarkers){
  var ids = [];
  currentMarkers.forEach(function(entry){
    ids.push(entry.id);
  });
  var dta = JSON.stringify(ids).replace('[', '').replace(']', '');

  
  if(ids.length > 1){
    $.ajax({
      type: 'GET',
      data: {adverts : dta},
      url: '/detail/',
      success: function(data){
        $("#advertInfo").empty();
        $('#advertInfo').html(data);
        $("#advertInfo").dialog("open");
        $(".money").digits();
        if(  get_browser().toLowerCase().indexOf("firefox") > -1 ){
          $("#advertInfo").css("left","0px");
        }
        $("#advertInfo").css("max-height","400");
        $(".ui-dialog").width("390");
        $("#advertInfo").width("360");
        $("#advertInfo").css("overflow-x","scroll")
      }
    });
  }else{

    if($(".login.asClient").text().indexOf("Iniciar") == -1 ){
      $.ajax({
        type: 'GET',
        url: '/detail/detail/' + ids[0],
        success: function(data){
          $("#advertInfo").dialog("open");
          $("#advertInfo").empty();
          $('#advertInfo').html(data);
          $(".ui-dialog").width("380");
          $("#advertInfo").width("370");
          $(".money").digits();
          $("#advertInfo").css("left","0");
          $("#advertInfo").dialog('option', 'position', 'center');
          $(".ui-dialog").width("310");
          $("#advertInfo").width("310");
          if ($(this).hasClass("asClient") )
            $("#user_phone").hide();
          else
            $("#user_phone").show();
        }
      });
    }else{
      $("#targetDiv").dialog("open"); 
      $(".ui-dialog-content.ui-widget-content").css("overflow","hidden");
      $("#tabs li").width("136");
      $(".ui-tabs-anchor").width("92");
      $("#user_phone").hide();
    }


  }

}

function get_browser(){
       var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
       if(/trident/i.test(M[1])){
           tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
           return 'IE';
       }
       if(M[1]==='Chrome'){
           tem=ua.match(/\bOPR\/(\d+)/)
           if(tem!=null)   {return 'Opera'}
       }
       M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
       if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
       return M[0];
   }

$(function(){
  $("#advertInfo").dialog(opt);
  $("#targetDiv").dialog(optDialog);
  /*$(document.body).on("click", ".ui-widget-overlay", function()
    {
        $.each($(".ui-dialog"), function()
        {
            var $dialog;
            $dialog = $("#advertInfo");
            if($dialog.dialog("option", "modal"))
            {
                $("#advertInfo").dialog("close");
                $("#dialog").dialog("close");
            }
        });
    });*/


  $(document).delegate('a.login', 'click', function() {
    //$("#targetDiv").dialog("open"); 
    $("#targetDiv").dialog("open"); 
    var scroll_pos=(0);          
    //$('html, body').animate({scrollTop:(scroll_pos)}, '0', function(){
      window.scrollTo(0,0);
      $("#targetDiv").dialog({ position: 'center' });
    //});
    //$("#targetDiv").dialog({ position: 'top' });

    $(".ui-dialog-content.ui-widget-content").css("overflow","hidden");
    $("#tabs li").width("136");
    $(".ui-tabs-anchor").width("92");
    //$("body").css("overflow","hidden");
    $("#user_phone").hide();
    //console.log("->Ocultar");

    if ($(this).hasClass("asClient") )
        $("#user_phone").hide();
      else
        $("#user_phone").show();

  });



});


