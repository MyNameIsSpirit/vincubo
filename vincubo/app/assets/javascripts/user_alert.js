/**
 * Created by edwin on 6/16/14.
 */
function createAlert() {
    $.ajax({
        type: "POST",
        url: "/useralert/create",
        data: generateCriteria(),
        dataType: 'json'
    }).done(function(data){
        //alertSaved();
        //alertChecked();
        $.notify("Las notificaciones por correo fueron agregadas.", "success");
        //$("#notifyme").hide();
        $("#notifyme").css("background-color","#A2A2A2")
    })
}

function removeAlert(idNot){
    $.ajax({
        type: "POST",
        url: "/useralert/destroy",
        data: {idUserAlert:idNot},
        dataType:'json'
    }).done(function(data){
        alertRemoved(idNot);
    })
}

function checkAlert(criteria){
    $.ajax({
        type: "POST",
        url: "/useralert/check",
        data: criteria,
        dataType: 'json'
    }).done(function(data){
        alertChecked();
    })
}

function alertChecked(){

}
function alertSaved(){

}

function alertRemoved(idUserAlert){
    $.notify("Las notificacion fue removida.", "success");
    $("#user_alert-"+idUserAlert).hide();

}
function hashCriteria(criteria){

}
function generateCriteria(){
    var url = "/useralert/create";
    var data={
        "town_id": $("#town_id").val(),
        "negotationtype": $("#negotationtype").val(),
        "state_id": $("#state_id").val(),
        "pricefrom": $("#pricefrom").val(),
        "priceto": $("#priceto").val(),
        "housetype": $("#housetype").val(),
        "bathrooms": ( ($(".j.selected").attr("value")) != "" ? $(".j.selected").attr("value") : 0 ),
        "rooms":  (($(".i.selected").attr("value")) != "" ? $(".i.selected").attr("value") : 0 ),
        "fromAreamts":  $("#fromAreamts").val(),
        "toAreamts":  $("#toAreamts").val(),
        "fromAreavar":  $("#fromAreavar").val(),
        "toAreavar":  $("#toAreavar").val()
    }
    return data
}