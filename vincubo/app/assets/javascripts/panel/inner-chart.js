
$(function(){
	fillData();
})

function fillData(){

var abm = JSON.parse( $("#abm").text().replace(/=>/g,":" ));
var rbm = JSON.parse( $("#rbm").text().replace(/=>/g,":" ));
var sbm = JSON.parse( $("#sbm").text().replace(/=>/g,":" ));
var hto = JSON.parse( $("#hto").text().replace(/=>/g,":" ));
var htt = JSON.parse( $("#htt").text().replace(/=>/g,":" ));
var rbmtotal = JSON.parse( $("#rbmtotal").text().replace(/=>/g,":" ));
var sbmtotal = JSON.parse( $("#sbmtotal").text().replace(/=>/g,":" ));




var basecolors = ["#808080","#B3B3B3","#E6E6E6","#ffffff"];
//var mueblesdata = [];
//$(abm).each(function(index,pair){
//	mueblesdata.push( { value: pair["value"], color: basecolors[index] }  );
//});
var mueblesdata = [];

$(abm).each(function(index,pair){
	mueblesdata.push( { value: Number(pair["value"]), color: basecolors[index] }  );
});

console.log(mueblesdata);
			var mueblesoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#808080",
				segmentStrokeWidth : 1,
			}
			var myDoughnut = new Chart(document.getElementById("muebles").getContext("2d")).Doughnut(mueblesdata,mueblesoptions);


var rbm = JSON.parse( $("#rbm").text().replace(/=>/g,":" ));
var sbm = JSON.parse( $("#sbm").text().replace(/=>/g,":" ));
			var propiedadesdata = [
				{
					value: Number($(sbmtotal)[0]["total"]),
					color:"#ffffff"
				},
				{
					value : Number($(rbmtotal)[0]["total"]),
					color : "#FF7F5E"
				}
			
			];
			var propiedadesoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#FF7F5E",
				segmentStrokeWidth : 1,
			}
			var myDoughnut = new Chart(document.getElementById("propiedades").getContext("2d")).Doughnut(propiedadesdata,propiedadesoptions);

			var zonasdata = [
				{
					value: 25,
					color:"#808080"
				},
				{
					value :25,
					color : "#B3B3B3"
				},
				{
					value : 25,
					color : "#E6E6E6"
				},
				{
					value : 25,
					color : "#ffffff"
				}
			
			];
			var zonasoptions = {
				segmentShowStroke : true,
				segmentStrokeColor : "#808080",
				segmentStrokeWidth : 1,

			}
			var myDoughnut = new Chart(document.getElementById("zonas").getContext("2d")).Doughnut(zonasdata,zonasoptions);



var labels = [];
var data = [];
$(sbm).each(function(index,pair){
	labels.push( pair["month"] );
	data.push( Number(pair["value"]) );
});
labels = labels.reverse();
data = data.reverse();

			var line2data = {
				labels : labels,
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						data : data
					}
				]
			}
			var line2options = {
				bezierCurve : false,
				datasetFill : false,
			}


var labels = [];
var data = [];
$(rbm).each(function(index,pair){
	labels.push( pair["month"] );
	data.push( Number(pair["value"]) );
});
labels = labels.reverse();
data = data.reverse();

			var line3data = {
				labels : labels,
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						data : data
					}
				]
			}
			var line3options = {
				bezierCurve : false,
				datasetFill : false,
			}


				

			var linedata = {
				labels : ["Jun.","Jul."],
				datasets : [
					{
						fillColor : "rgba(255,163,45,0.5)",
						strokeColor : "rgba(255,163,45,1)",
						pointColor : "rgba(255,163,45,1)",
						pointStrokeColor : "#fff",
						data : [15,30]
					}
				]
			}
			var lineoptions = {
				bezierCurve : false,
				datasetFill : false,
			}
				

			var myLine2 = new Chart(document.getElementById("line2grafico").getContext("2d")).Line(line2data,line2options);
			var myLine3 = new Chart(document.getElementById("line2grafico2").getContext("2d")).Line(line3data,line3options);
			//var myLine2 = new Chart(document.getElementById("line2grafico3").getContext("2d")).Line(line2data,line2options);
			//var myLine2 = new Chart(document.getElementById("line2grafico4").getContext("2d")).Line(line2data,line2options);
				
}