var tempAdvertHash = 0;

$(function(){
	
	var opt = {
		url: '/login',
		autoOpen: false,
		modal: true,
		width: 330,
		heigth: 600,
	    title: '',
	    draggable: false,
	    dialogClass: 'dlgfixed',
	    position: 'center',
	    open:function(){
            $dialogOverlay = $(".ui-widget-overlay").last();
            $dialogOverlay.unbind();
            $dialogOverlay.click(function(){
                $("#targetDiv").dialog("close");
                $("body").css("overflow","");
                //$(".dlgfixed").center(false);
            });
        }
	};

	$("#targetDiv").bind('clickoutside',function(){
	    $("#targetDiv").dialog('close');
	    $("#user_phone").show();
	    //$(".ui-dialog-content.ui-widget-content").css("overflow","");
	});

	$(".ui-widget-overlay").on("click",function(){
		//alert("click");
	});

	$("#targetDiv").load("/login/dialog");
	$("#targetDiv").dialog(opt);

	$(document).on("click", '.login',function(e){
		$("#targetDiv").dialog("open"); 
		//$(".dlgfixed").center(false);
		$(".ui-dialog-content.ui-widget-content").css("overflow","");
		$("#tabs li").width("136");
		$(".ui-tabs-anchor").width("92");
		
		if( $(this).attr("id") != undefined)
			tempAdvertHash = $(this).attr("id").split("-")[1]  ;
		
		if ($(this).hasClass("asClient") )
	    	$("#user_phone").hide();
	    else
	    	$("#user_phone").show();
	});

	$(".ui-widget-overlay").on("click",function(){
		$("#targetDiv").dialog("close"); 
		$("#user_phone").show();
		$("#container").dialog("close");
	});

	$(".ui-dialog-title").hide()

	$(".logo").click(function(){
		window.location = "/";
	});

	$(".goback").on("click", function(){
		history.back();
	});

	$.fn.digits = function(){ 
	    return this.each(function(){ 
	        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
	    })
	}

	$(".money").digits();

	$(document).delegate('.registerArea', 'click', function() {


		FB.login(function(response) {

        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            //console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me', function(response) {


              $.ajax({
            type: 'GET',
            url: '/profile/fbRegister?name=' + response.first_name + '&lastname=' + response.last_name + '&fbid=' + response.id + '&email=' + response.email ,
            dataType: 'json',
            success: function(done){
              //Inicia sucess

              $("#publishspace").removeClass("login").attr("href","/administration/new");
                  $("#myspaces").removeClass("login").attr("href","/administration/spaces");

                  $("#principal").text("Cerrar Sesión");
                  $("#principal").removeClass("houseDetail");
                  $("#targetDiv").dialog("close");
                  $(".bold.knowmore").removeClass("login").addClass("displayAdvert");
                  $("a.login.asClient").removeClass("login").removeClass("asClient").addClass("houseDetail");
                  $("#notifyme").removeClass("login")
                  
                  $("#principal").attr("href","/login/logout");
                  $("#principal").removeAttr('class');
                  $.ajax({
                        type: 'GET',
                        url: '/detail/detail/' + tempAdvertHash ,
                        success: function(data){
                          $("#dialog").html(data);
                          $("#dialog").dialog("open");
                          $(".ui-dialog-titlebar").hide();
                      }
                    });

                  $(document).on("click", '.displayAdvert',function(e){
                    e.preventDefault();
                    var id = $(this).attr("id").split("-")[1];
                    $.ajax({
                        type: 'GET',
                        url: '/detail/detail/' + id ,
                        success: function(data){
                          $("#dialog").html(data);
                          $("#dialog").dialog("open");
                          $(".ui-dialog-titlebar").hide();
                      }
                    });
                  });

                  $(document).on("click", '.houseDetail',function(e){
                    e.preventDefault();
                    var id = $(this).attr("id").split("-")[1];
                    $.ajax({
                        type: 'GET',
                        url: '/detail/detail/' + id ,
                        success: function(data){
                          $("#dialog").html(data);
                          $("#dialog").dialog("open");
                          $(".ui-dialog-titlebar").hide();
                      }
                    });
                  });

              //Finaliza sucess


            }
          });

                            
            });

        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'publish_stream,email'
    });
	})

});


function updateQueryString(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

function getUrlParameter(name)
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return null;
  else
    return results[1];
}








// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    //console.log('statusChangeCallback');
    //console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      //testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      //document.getElementById('status').innerHTML = 'Please log ' +
        //'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      //document.getElementById('status').innerHTML = 'Please log ' +
        //'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '585520521558163',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {

    //console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
    	$.ajax({
            type: 'GET',
            url: '/profile/fbRegister?name=' + response.first_name + '&lastname=' + response.last_name + '&fbid=' + response.id + '&email=' + response.email ,
            dataType: 'json',
            success: function(done){
            	//Inicia sucess

            	$("#publishspace").removeClass("login").attr("href","/administration/new");
                  $("#myspaces").removeClass("login").attr("href","/administration/spaces");

                  $("#principal").text("Cerrar Sesión");
                  $("#principal").removeClass("houseDetail");
                  $("#targetDiv").dialog("close");
                  $(".bold.knowmore").removeClass("login").addClass("displayAdvert");
                  $("a.login.asClient").removeClass("login").removeClass("asClient").addClass("houseDetail");
                  $("#notifyme").removeClass("login")
                  
                  $("#principal").attr("href","/login/logout");
                  $("#principal").removeAttr('class');
                  $.ajax({
                        type: 'GET',
                        url: '/detail/detail/' + tempAdvertHash ,
                        success: function(data){
                          $("#dialog").html(data);
                          $("#dialog").dialog("open");
                          $(".ui-dialog-titlebar").hide();
                      }
                    });

                  $(document).on("click", '.displayAdvert',function(e){
                    e.preventDefault();
                    var id = $(this).attr("id").split("-")[1];
                    $.ajax({
                        type: 'GET',
                        url: '/detail/detail/' + id ,
                        success: function(data){
                          $("#dialog").html(data);
                          $("#dialog").dialog("open");
                          $(".ui-dialog-titlebar").hide();
                      }
                    });
                  });

                  $(document).on("click", '.houseDetail',function(e){
                    e.preventDefault();
                    var id = $(this).attr("id").split("-")[1];
                    $.ajax({
                        type: 'GET',
                        url: '/detail/detail/' + id ,
                        success: function(data){
                          $("#dialog").html(data);
                          $("#dialog").dialog("open");
                          $(".ui-dialog-titlebar").hide();
                      }
                    });
                  });

            	//Finaliza sucess


            }
        });
    });
  }

