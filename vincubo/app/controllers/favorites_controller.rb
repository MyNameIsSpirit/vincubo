class FavoritesController < ApplicationController
	config.log_level = :warn
	Rails.logger.level = 0

	layout "user_layout"

	def index
		if (session[:user] == nil )
      		redirect_to :controller => "login", :action => "index"
    	else
    		@user = User.find( cookies[:id] )
    		@favorites = @user.favorite_adverts
    		logger.info ">> favorites"
    		logger.info @favorites
    	end
	end

	def new
		id = params[:id]
		@user = User.find( cookies[:id] )
		@advert = Advert.find(id)
		@user.favorite_adverts.push(@advert)
		logger.info "processed succesfully"
		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :message => "message"}}
	    end
	end

	def destroy
		id = params[:id]
		@user = User.find( cookies[:id] )
		@advert = Advert.find(id)
		@user.favorite_adverts.delete(@advert)
		logger.info "removed succesfully"
		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :message => "message"}}
	    end
	end

end
