class ManageController < ApplicationController

  layout "admin_layout"

	def index
		@states = State.all
		#@adverts = Advert.where(:ispublished => true)
    negotationtype = params[:negotationtype]
    state_id = params[:state_id]
    town_id = 0

    if( params[:town_id] != nil )
      town_id = Integer(params[:town_id])
    end
    townid = params[:town]
    price = params[:price]
    rooms = params[:rooms]
    bathrooms = params[:bathrooms]
    pricefrom = params[:pricefrom]
    priceto = params[:priceto]
    housetype = params[:housetype]
    page = params[:page]
    zone = params[:zone]
    orderby = params[:orderby]
    
    where = ""

    if(negotationtype == "Alquiler")
      if (where != "")
        where += " AND "
      end
      where += " negotationtype = 'Alquiler' "
    else
      if (where != "")
        where += " AND "
      end
      where += " negotationtype = 'Venta' "
    end


    if( zone != nil && zone != "" && zone != "0" )
      if (where != "")
        where += " AND "
      end
      where += " zone_id = " + zone.to_s + " "
    elsif (town_id > 0)
      if (where != "")
        where += " AND "
      end
      where += " town_id = " + town_id.to_s + " "
    elsif(state_id != "" && state_id != nil)
      if (where != "")
        where += " AND "
      end
      where += " towns.state_id = '"+ state_id + "' "
    end
    

    if(rooms != nil && rooms != "undefined")
      if (where != "")
        where += " AND "
      end
      where += " rooms >= " + rooms + " "
    end
    if(bathrooms != nil && bathrooms != "undefined")
      if (where != "")
        where += " AND "
      end
      where += " bathroom >= " + bathrooms + " "
    end
    if(pricefrom != "" && pricefrom != nil)
      if (where != "")
        where += " AND "
      end
      where += " price >= " + pricefrom + " "
    end
    if(priceto != "" && priceto != nil)
      if (where != "")
        where += " AND "
      end
      where += " price <= " + priceto + " "
    end

    if(housetype != "" && housetype != nil)
      if (where != "")
        where += " AND "
      end
      where += " housetype = '" + housetype + "' "
    end

    orderCriteria = "price "
    if(orderby == nil || orderby == "" || orderby == "precio")
      orderCriteria = "price "
    elsif(orderby != nil && orderby == "time" )
      orderCriteria = "created_at "
    elsif(orderby != nil && orderby == "tiempo" )
      orderCriteria = "created_at "
    else
      orderCriteria = "price "
    end

    logger.info "-"
    logger.info "-"
    logger.info "-"
    logger.info "-"
    where += " AND ispublished != false "
    logger.info where
    #logger.info ">>>>>>>" + orderCriteria
    #@total = Advert.joins(:town).where(where).count()
    @adverts = Advert.joins(:town).where(where)
    @total = @adverts.count
    logger.info "<-"
    logger.info @total
    logger.info "->"
    
    if(page == nil)
      page =1;
    end
    #@adverts = @adverts.order(orderCriteria +' DESC').paginate(:page => page, :per_page => 10)
    #@total = Advert.count
    @state = State.first
    @town = Town.first

    @state = State.first
    @town = Town.first
    
	end

	def edit
		@advert = Advert.find(params[:id])

	    6.times { 
	        @advert.assets.build
	    } 
	    
	    @photoadvert = Photoadvert.new
	    @state = State.first
	    @town = Town.first

      @towns = @advert.town.state.towns
      @zones = @advert.town.zones
	end


	def update
		@advert = Advert.find(params[:id])
      assets = asset_params
      deleted_assets=delete_asset_params

		#deleting old images

      if(deleted_assets != nil)
        deleted_assets.keys.map do |deleted_asset_key|
          asset_element = Integer(deleted_asset_key)
          if(deleted_assets[deleted_asset_key] == "true")
            @advert.assets[asset_element].destroy
          end
        end
      end

      if(assets != nil)
        assets.map do |assetParam|
          newAsset = Asset.new
          asset_number = Integer(assetParam[0])
          newAsset.asset = assetParam[1][:asset]
          if(@advert.assets[asset_number] && @advert.assets[asset_number].asset.exists?)
            @advert.assets[asset_number].asset = newAsset.asset
          else
            @advert.assets << newAsset
          end

        end
      end
  		if @advert.save
    		redirect_to action: "index"
  		else
    		render 'edit'
  		end
	end


	def delete
		#@advert = Advert.find(params[:id])
		Advert.destroy(params[:id])
		respond_to do |format|
	      format.html
	      format.json { render :json => { :result => true }}
	    end
	end

  def users
    @users = User.all
  end

  def report
    @adverts = Report.all
  end

  def editprofile
    @user = User.find(params[:id])
  end

  def updateprofile
    @user = User.find(user_params[:id])

    if @user.update(user_params)
      redirect_to :controller => "manage", :action => "index"
    else
      render 'edit'
    end
  end

  def states
    @states = State.all
  end

  def zones
    @states = State.all.order('name ASC')
  end

  def addzone
    @town = Town.find(params[:town_id])
    zonename = params[:zone]

    @zone = Zone.new
    @zone.town = @town
    @zone.zonename = zonename
    @zone.save
    respond_to do |format|
        format.html
        format.json { render :json => { :result => true }}
    end
  end

  private
      def user_params
        params.require(:user).permit(:id, :name, :lastname, :email, :job, :about, :reference, :phone, :image, :register_params)
      end

end
