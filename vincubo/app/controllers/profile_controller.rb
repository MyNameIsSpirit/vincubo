class ProfileController < ApplicationController

	config.log_level = :warn
	Rails.logger.level = 0

	def index
	end

	def new
		logger.info "Processing the request New..."
		@user = User.new
	end
 
	def create
  		logger.info "Processing the request Create..."
  		@user = User.new( user_params )
  		@user.save
  		redirect_to :controller => "administration", :action => "index"
  	end

  	def edit
		@user = User.find(cookies[:id])
	end

	def update
		@user = User.find(cookies[:id])

		if @user.update(user_params)
			redirect_to :controller => "administration", :action => "index"
		else
			render 'edit'
		end
	end

  	def show
    	@user = User.find(params[:id])
  	end

  	def register
  		logger.info "Reigstrando usuario"
  		@user = User.new( user_params )
  		@user.save
  		if( @user.phone == "" )
  			@user.update_attribute(:isclient, true)
  		else
  			@user.update_attribute(:isclient, false)
  		end

  		@result = User.where(email: @user.email, password: @user.password).pluck(:id).take(1)
		session[:user] = @result
		cookies[:id] = { :value => @result, :expires => Time.now + 360000}

  		redirect_to :controller => "administration", :action => "index"
  	end


  	def registerUser
  		@user = User.new
  		#params.require(:user).permit(:name, :lastname, :email, :job, :password, :about, :reference, :phone, :mobile, :image, :register_params)
  		@user.name = params[:name]
  		@user.lastname = params[:lastname]
  		@user.email = params[:email]
  		@user.password = params[:password]
  		if( params[:phone] == "" )
  			@user.isclient = true
        #@user.phone = params[:phone]
  		else
  			@user.isclient = false
        @user.phone = params[:phone]
  		end

  		@user.save

      if @user.isclient == true
        WelcomeMailer.consumer_registration(@user).deliver
      else
        WelcomeMailer.user_registration(@user).deliver
      end

  		session[:user] = @user
		  cookies[:id] = { :value => @user.id, :expires => Time.now + 360000}
		  respond_to do |format|
	      format.html
	      format.json { render :json => { :result => @user }}
	    end

  	end



  def fbRegister

    fbid = params[:fbid]
    if(User.where(:fbid => fbid).count == 0)

      @user = User.new
      @user.name = params[:name]
      @user.lastname = params[:lastname]
      @user.fbid = params[:fbid]
      @user.email = params[:email]
      @user.isclient = true
      @user.save

      WelcomeMailer.consumer_registration(@user).deliver
    end

    @userlogged = User.where(:fbid => fbid).first

      session[:user] = @userlogged
      cookies[:id] = { :value => @userlogged.id, :expires => Time.now + 360000}
      respond_to do |format|
        format.html
        format.json { render :json => { :result => @userlogged }}
      end


  end

	private
  		def user_params
  			params.require(:user).permit(:name, :lastname, :email, :job, :password, :about, :reference, :phone, :mobile, :image, :register_params)
  		end

      def fb_params
        params.require(:user).permit(:name, :lastname, :email, :fbid,  )
      end

	   	def asset_params
	      params.require(:asset).permit(:image)
	    end

	    def register_params
	    	params.require(:user).permit(:email, :password, :name, :lastname, :phone, :isclient)
	    end

end
