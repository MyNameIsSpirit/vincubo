class LoginController < ApplicationController

	config.log_level = :warn
	Rails.logger.level = 0
	
	def index
		@user = User.new
	end

	def login
		logger.info "Evaluating login"
		@user = User.new(login_params)
		if ( User.where(email: @user.email, password: @user.password).pluck(:id).count == 0)
			redirect_to :controller => "welcome", :action => "index"
		else
			@result = User.where(email: @user.email, password: @user.password).pluck(:id).take(1)
			session[:user] = @result
			cookies[:id] = { :value => @result, :expires => Time.now + 360000}
			redirect_to :controller => "administration", :action => "index"
		end
	end

	def startlogin
		logger.info "Evaluating login"
		
		@result = User.where(email: params[:email], password: params[:password]).pluck(:id).take(1)
		session[:user] = @result
		cookies[:id] = { :value => @result, :expires => Time.now + 360000}
		#redirect_to :controller => "administration", :action => "index"
		
		respond_to do |format|
	      format.html
	      format.json { render :json => { :result => @result }}
	    end
	end

	def logout
		session[:user] = nil
		session.clear
		reset_session
		session = {}

		cookies.delete :id
		redirect_to :controller => "welcome", :action => "index"
  end
  def send_confirmation
    @user = User.where(:email => params[:email]).first
    @user.token = (0...25).map { ('a'..'z').to_a[rand(26)] }.join
    @user.save
    link = "/login/reset_password?token=" + @user.token
    UserMailer.password_change_confirmation(@user).deliver
    respond_to do |format|
      format.html
      format.json { render :json => { :result => 0 }}
    end
  end
  def confirm_password_reset
    @user = User.where(:token => params[:token]).first if params[:token] != nil && params[:token] != ''
  end
  def reset_password
    @user = User.where(:token => params[:token]).first
    @user.password = params[:password]
    @user.token = ""

    @user.save
    @success = true && @user != nil
  end

	private
		def login_params
			params.require(:user).permit(:email, :password)
		end
end
