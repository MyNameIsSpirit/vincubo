class AdvertController < ApplicationController

	layout "search_layout"
	config.log_level = :warn
	Rails.logger.level = 0


	def index
		
		negotationtype = params[:negotationtype]
		state_id = params[:state_id]
		town_id = 0

		if( params[:town_id] != nil )
			town_id = Integer(params[:town_id])
		end
		townid = params[:town]
		price = params[:price]
		rooms = params[:rooms]
		bathrooms = params[:bathrooms]
		pricefrom = params[:pricefrom]
		priceto = params[:priceto]
		housetype = params[:housetype]
		page = params[:page]
		zone = params[:zone]
		orderby = params[:orderby]
		
		where = ""

		if(negotationtype == "Alquiler")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Alquiler' "
		else
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Venta' "
		end


		if( zone != nil && zone != "" && zone != "0" )
			if (where != "")
				where += " AND "
			end
			where += " zone_id = " + zone.to_s + " "
		elsif (town_id > 0)
			if (where != "")
				where += " AND "
			end
			where += " town_id = " + town_id.to_s + " "
		elsif(state_id != "" && state_id != nil)
			if (where != "")
				where += " AND "
			end
			where += " towns.state_id = '"+ state_id + "' "
		end
		

		if(rooms != nil && rooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " rooms >= " + rooms + " "
		end
		if(bathrooms != nil && bathrooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " bathroom >= " + bathrooms + " "
		end
		if(pricefrom != "" && pricefrom != nil)
			if (where != "")
				where += " AND "
			end
			where += " price >= " + pricefrom + " "
		end
		if(priceto != "" && priceto != nil)
			if (where != "")
				where += " AND "
			end
			where += " price <= " + priceto + " "
		end

		if(housetype != "" && housetype != nil)
			if (where != "")
				where += " AND "
			end
			where += " housetype = '" + housetype + "' "
		end

		orderCriteria = "price "
		if(orderby == nil || orderby == "" || orderby == "precio")
			orderCriteria = "price "
		elsif(orderby != nil && orderby == "time" )
			orderCriteria = "created_at "
		elsif(orderby != nil && orderby == "tiempo" )
			orderCriteria = "created_at "
		else
			orderCriteria = "price "
		end

		where += " AND ispublished = true "
		logger.info ">>>>>>>" + orderCriteria
		#@total = Advert.joins(:town).where(where).count()
		@adverts = Advert.joins(:town).where(where)
		@total = @adverts.count
		
		if(page == nil)
			page =1;
		end
		@adverts = @adverts.order(orderCriteria +' DESC').paginate(:page => page, :per_page => 10)
		#@total = Advert.count
		@state = State.first
      	@town = Town.first
      	@page = page
	end

	def map
		townid = params[:town]
		@state = State.first
      	@town = Town.first
      	#@adverts = Advert.where(" ispublished = true ")
		if (townid != nil)
			#@adverts = Advert.order('price DESC')
			@adverts = Advert.where("town_id = :t", {:t => townid} )
		else
			@adverts = Advert.all
		end
	end

	def load
		negotationtype = params[:negotationtype]
		state_id = params[:state_id]
		town_id = 0

		if( params[:town_id] != nil )
			town_id = Integer(params[:town_id])
		end
		townid = params[:town]
		price = params[:price]
		rooms = params[:rooms]
		bathrooms = params[:bathrooms]
		pricefrom = params[:pricefrom]
		priceto = params[:priceto]
		housetype = params[:housetype]
		page = params[:page]
		zone = params[:zone]

		where = ""

		#Evaluate negotationtype
		if(negotationtype == "Venta")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Venta' "
		end
		if(negotationtype == "Alquiler")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Alquiler' "
		end

		logger.info  "<<<<<<<<"
		logger.info zone
		logger.info town_id
		logger.info state_id


		logger.info  ">>>>>>>"

		if( zone != nil && zone != "" && zone != "0" )
			if (where != "")
				where += " AND "
			end
			where += " zone_id = " + zone.to_s + " "
		elsif (town_id > 0)
			if (where != "")
				where += " AND "
			end
			where += " town_id = " + town_id.to_s + " "
		elsif(state_id != "" && state_id != nil)
			if (where != "")
				where += " AND "
			end
			where += " towns.state_id = '"+ state_id + "' "
		end




		if( zone != nil && zone != "" && zone != "0" )
			if (where != "")
				where += " AND "
			end
			where += " zone_id = " + zone.to_s + " "
		end

		if(rooms != nil && rooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " rooms >= " + rooms + " "
		end
		if(bathrooms != nil && bathrooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " bathroom >= " + bathrooms + " "
		end
		if(pricefrom != nil && pricefrom != "")
			if (where != "")
				where += " AND "
			end
			where += " price >= " + pricefrom + " "
		end
		if(priceto!= nil && priceto != "")
			if (where != "")
				where += " AND "
			end
			where += " price <= " + priceto + " "
		end
		if(housetype != nil && housetype != "")
			if (where != "")
				where += " AND "
			end
			where += " housetype = '" + housetype + "' "
		end

		if (where != "")
			where += " AND ispublished = true "
		else
			where += " ispublished = true "
		end


		@adverts = Advert.order('price DESC').joins(:town).where(where).all;

	    respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :result => @adverts }}
	    end
	end

	def info
		logger.info params[:adverts]
		ids = params[:adverts].split(',')

		@advert = Advert.where(:id => ids)
		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :result => @advert }}
	    end
	end

	def custom
	end


end


