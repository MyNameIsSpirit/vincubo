class DetailController < ApplicationController

	config.log_level = :warn
	Rails.logger.level = 0

	def index
		ids = params[:adverts].split(',')
		@entries = Advert.where(:id => ids)
	end

	def detail
		@user = User.find( cookies[:id] )
		@entry = Advert.find(params[:id])
	end

	def list
		negotationtype = params[:negotationtype]
		state_id = params[:state_id]
		town_id = 0

		if( params[:town_id] != nil )
			town_id = Integer(params[:town_id])
		end

		price = params[:price]
		rooms = params[:rooms]
		bathrooms = params[:bathrooms]
		pricefrom = params[:pricefrom]
		priceto = params[:priceto]
		housetype = params[:housetype]
		where = ""

		#Evaluate negotationtype
		if(negotationtype == "Venta")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Venta' "
		end
		if(negotationtype == "Alquiler")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Alquiler' "
		end

		if(state_id != "" && state_id != nil)
			if (where != "")
				where += " AND "
			end
			where += " towns.state_id = '"+ state_id + "' "
		end

		if (town_id > 0)
			if (where != "")
				where += " AND "
			end
			where += " town_id = " + town_id.to_s + " "
		end

		if(rooms != nil && rooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " rooms >= " + rooms + " "
		end
		if(bathrooms != nil && bathrooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " bathroom >= " + bathrooms + " "
		end
		if(pricefrom != "")
			if (where != "")
				where += " AND "
			end
			where += " price >= " + pricefrom + " "
		end
		if(priceto != "")
			if (where != "")
				where += " AND "
			end
			where += " price <= " + priceto + " "
		end
		if(housetype != "")
			if (where != "")
				where += " AND "
			end
			where += " housetype = '" + housetype + "' "
		end

		#@total = Advert.joins(:town).where(where).count()
		@adverts = Advert.joins(:town).where(where)
		@adverts = @adverts.paginate(:page => 1, :per_page => 20)
	    #respond_to do |format|
	    #  format.html # contact.html.erb
	    #  format.json { render :json => { :result => @adverts, :total => total}}
	    #end
	    #WillPaginate.per_page = 10
	    #@ads = Get.paginate :page => 1, :order => 'created_at DESC'
	end

	def denounce
    id = params[:id]
    @report = Report.new
    @report.message = params[:message]
    if cookies[:id]
      @report.user = User.find( cookies[:id] )
    end
    @report.advert = Advert.find(id)
    
    @report.save
    #@user = User.find( cookies[:id] )
    #@advert = Advert.find(id)
    #@user.report_adverts.push(@advert)
    #logger.info "processed succesfully"
    respond_to do |format|
      format.html # contact.html.erb
      format.json { render :json => { :message => "message"}}
    end

  end

	def remove
		id = params[:id]
		@advert = Advert.find(id)
	end

	def report
		id = params[:id]
		@advert = Advert.find(id)
	end

	def unpublish
		id = params[:id]
		@advert = Advert.find(id)
		Advert.update(@advert.id, :ispublished => false)
		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :message => @advert.id}}
	    end
	end


	def notification
		negotationtype = params[:negotationtype]
		state_id = params[:state_id]
		town_id = 0

		if( params[:town_id] != nil )
			town_id = Integer(params[:town_id])
		end
		townid = params[:town]
		price = params[:price]
		rooms = params[:rooms]
		bathrooms = params[:bathrooms]
		pricefrom = params[:pricefrom]
		priceto = params[:priceto]
		housetype = params[:housetype]
		#page = params[:page]

		url = request.original_url

		@notification = Notification.new
		@notification.negotationtype = negotationtype
		@notification.priceto = priceto
		@notification.rooms = pricefrom
		@notification.bathrooms = bathrooms
		@notification.housetype = housetype
		@notification.url = url
		if(state_id > "0")
			@notification.state = State.find(state_id)
		end
		if(town_id.to_s > "0")
			@notification.town = Town.find(town_id)
		end

		@notification.user = User.find( cookies[:id] )
		@notification.save

		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :message => "message"}}
	    end

	end


	def zonesbystate
		state_id = params[:state_id]
		@zones = Zone.joins(:town).where("towns.state_id=?",state_id).order('zonename ASC')
		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :data => @zones}}
	    end
	end

	def zonesbytown

		town_id = params[:town_id]
		@zones = Zone.where(:town_id=>town_id).order('zonename ASC')
		respond_to do |format|
	      format.html # contact.html.erb
	      format.json { render :json => { :data => @zones}}
	    end

	end



end
