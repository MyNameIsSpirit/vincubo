class PanelController < ApplicationController
	skip_before_action :verify_authenticity_token
	config.log_level = :warn
	Rails.logger.level = 0
	#layout 'public_layout', :only => :read

	def index
		#@adverts = Advert.take(10).skip(1)
		page = params[:page]
		if(page == nil)
			page =1;
		end
		@adverts = Advert.where(:ispublished => false ).paginate(:page => page, :per_page => 5)
		@states = State.all
		
	end


	def publish
		
		@advert = Advert.find(params[:aid])

		page = params[:page]
		if(page == nil)
			page = 1
		end


		address = Town.find(params["town_id"]).state.name + " " + Town.find(params["town_id"]).name

		@advert.town = Town.find(params["town_id"])
		if( params["zone_id"] == nil || params["zone_id"] == "" )
			logger.info "--->>>"
		else
			@advert.zone = Zone.find(params["zone_id"])
			address = address + " " + Zone.find(params["zone_id"]).zonename
		end

		location = "El Salvador " + address
      	lo = Geocoder.search(location)

      	if(lo != nil && lo.count > 0 && lo[0].data != nil && lo[0].data["geometry"] != nil && lo[0].data["geometry"]["bounds"] != nil )
      		lng = lo[0].data["geometry"]["bounds"]["northeast"]["lng"]
			lat = lo[0].data["geometry"]["bounds"]["northeast"]["lat"]
			
      		@advert.latitude = lat
      		@advert.longitude = lng
     	end

		@advert.description = advert_params[:description]
		@advert.phone = advert_params[:phone]
		@advert.housetype = advert_params[:housetype]
		@advert.rooms = advert_params[:rooms] 
		@advert.ispublished = true
		@advert.price = advert_params[:price] 
		@advert.bathroom = advert_params[:bathroom]
		@advert.adverttype = advert_params[:adverttype]
		@advert.negotationtype = advert_params[:negotationtype]
		@advert.address = advert_params[:address]
		@advert.parking = advert_params[:parking]
		@advert.local = advert_params[:local]
		@advert.save

		
		redirect_to :controller => "panel", :action => "index", :page => page
	end

	def advert_params
  		params.require(:advert).permit(:aid, :id, :title, :features, :description, :areadescription, :rooms, :address, :local, :aditionaladdress, :negotationtype, :bathroom, :price, :housetype, :subtype, :source, :originaldate, :image_file_name, :image_content_type, :image_file_size, :image_file_updated_at, :area, :availability, :parking, :construction, :areasize, :furnished, :pantry , :security , :garden , :nearbymalls , :kitchen , :preservation , :pool , :jacuzzi , :level , :basicservice , :cistern, :water , :heater , :air , :telephoneline  , :visitorparking , :mezzanine , :recreation , :study , :terrace , :cellar , :parkland , :image , :town_id, :latitude, :longitude, :youtube1, :youtube2, :assets, :street, :adverttype, :phone,:zone_id)
  	end

  	def state_params
      params.require(:state).permit(:id, :name)
    end


  	def chart 
  		@state = State.new
  		@states = State.all

  		#state = params[:state]
  		#pricemin = params[:pricemin].gsub! '$', ''
  		#pricemax = params[:pricemax].gsub! '$', ''
  		#m2min = params[:m2min].gsub! 'm', ''
  		#m2max = params[:m2max].gsub! 'm', ''
  		#v2min = params[:v2min].gsub! 'm2', ''
  		#v2max = params[:v2max].gsub! 'm2', ''
  		#rooms = params[:rooms]
  		#bathrooms = params[:bathrooms]
  		#parkings = params[:parkings]

  		#where =  " price >= " + pricemin + " AND price <= " + pricemax + " AND rooms >= " + rooms + " AND bathroom >= " + bathrooms + " "

  		#queries = []

  		#query = "select to_char(originaldate,'Mon') as month, count(originaldate) as value  from adverts where " +  where + " group by originaldate;"
  		#queries[0] = query
  		#@advertsbymonth = ActiveRecord::Base.connection.execute(query)

  		#query = "select to_char(originaldate,'Mon') as month, count(originaldate) as value  from adverts where negotationtype = 'Alquiler' AND " + where + "  group by originaldate;"
  		queries[1] = query
		#@rentbymonth = ActiveRecord::Base.connection.execute(query)

		query = "select to_char(originaldate,'Mon') as month, count(originaldate) as value  from adverts where negotationtype = 'Venta' AND " + where + "  group by originaldate;"
		queries[2] = query
		#@sellbymonth = ActiveRecord::Base.connection.execute(query)

		query = "select count(*) from adverts where housetype = 'Oficina/Clinica' AND " + where + " ;"
		queries[3] = query
		#@htoffice = ActiveRecord::Base.connection.execute(query)

		query = "select count(*) from adverts where housetype = 'Apartamento' AND " + where + " ;"
		queries[4] = query
		#@htapartment = ActiveRecord::Base.connection.execute(query)

		query = "select count(*) from adverts where housetype = 'Terreno' AND " + where + " ;" 
		queries[5] = query
		#@htt = 

		query = "select count(*) as total from adverts where negotationtype = 'Alquiler' AND " + where + " ;"
  		queries[6] = query
		#@rentbymonth = ActiveRecord::Base.connection.execute(query)

		query = "select count(*) as total from adverts where negotationtype = 'Venta' AND " + where + " "
		queries[7] = query

		queries.each do |q|
			#Pegar el where
		end



		@advertsbymonth = Advert.connection.select_all(queries[0])
		@rentbymonth = Advert.connection.select_all(queries[1])
		@sellbymonth = Advert.connection.select_all(queries[2])
		@htoffice = Advert.connection.select_all(queries[3])
		@htapartment = Advert.connection.select_all(queries[4])
		@htt = Advert.connection.select_all(queries[5])
		@rbmtotal = Advert.connection.select_all(queries[6])
		@sbmtotal = Advert.connection.select_all(queries[7])

  	end


  	def read
  		#render :layout => 'public_layout'
  	end


  	def upload

  		#file_data = params[:uploaded_file]

  		#name = "issues.xls" #params['uploaded_file'].original_filename
  		#name = "issues.xls"
  		#name = params[:upload]['datafile'].original_filename
		#directory = "public/data"

		# create the file path
		#path = File.join(directory, name)
		#File.open(path, "wb") { |f| f.write(params[:upload][:file].read) }

		require 'fileutils'
		require 'iconv'
		require 'roo'
		
		@advertsNoIncluded = Array.new
		#tmp = params[:upload][:file].tempfile
		#file = File.join("public", params[:upload][:file].original_filename)
		#FileUtils.cp tmp.path, file
		#oo = Roo::Spreadsheet.new(file.path, {})
		
		#name = params[:upload][:file].original_filename
		#directory = "public/data"
		#path = File.join(directory, name)
		#file = params[:upload][:file]
		name = params[:upload][:file].original_filename
	    #directory = "public/data"
	    directory = "/tmp"
	    path = File.join(directory, name)
	    File.open(path, "wb") { |f| f.write(params[:upload][:file].read) }

	  	book = Spreadsheet.open(path)
	  	
		sheet1 = book.worksheet('Campos Finales') # can use an index or worksheet name
		firstrow = true
		sheet1.each do |row|

			if firstrow == false


			  location = "El Salvador " + row[5]  + " " + row[7] + " " + row[4].to_s
			  lo = Geocoder.search(location)
			  
			  if(lo != nil && lo.count > 0 && lo[0].data != nil && lo[0].data["geometry"] != nil && lo[0].data["geometry"]["bounds"] != nil )
			  		
			  		lo[0]
			  		lo[0].data["geometry"]
			  		lo[0].data["geometry"]["bounds"]
			  		lo[0].data["geometry"]["bounds"]["northeast"]

				  lng = lo[0].data["geometry"]["bounds"]["northeast"]["lng"]
				  lat = lo[0].data["geometry"]["bounds"]["northeast"]["lat"]

				  #break if row[0].nil? # if first cell empty
				  puts row.join(',') # looks like it calls "to_s" on each cell's Value
				  @advert = Advert.new
				  @advert.originalsite = row[1].to_s
				  @advert.ispublished = true
				  @advert.originalsource = row[2].to_s
				  @advert.address = row[4].to_s
				  @advert.originaldate = Date.strptime( row[3].to_s).strftime("%yyyy-%m-%d")
				  #@advert.state = State.where(:name => row[7].value.to_s).first
				  @advert.zone = Zone.where(:zonename => row[5]).first
				  @advert.town = Town.where(:name => row[6]).first
				  #row[6].to_s -> municipio
				  #row[7].to_s -> departamento

				  texto = ""
				  if(row[23].to_s )
				  	texto += row[23].to_i.to_s +  " habitacion(es)"
				  end
				  if(row[24].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += row[24].to_i.to_s + " baños"
				  end
				  if(row[25].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += row[25].to_i.to_s + " parqueo(s) "
				  end
				  if(row[26].to_s || row[26].to_i.to_s == "0")
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += row[26].to_i.to_s + " piso(s)"
				  end
				  if(row[27].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " amueblado"
				  end
				  if(row[28].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " con área de servicio"
				  end
				  if(row[29].to_s || row[29].to_i.to_s != 0 )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += row[29].to_i.to_s + " sala(s)"
				  end
				  if(row[30].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " comedor"
				  end
				  if(row[31].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " cocina"
				  end
				  if(row[32].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " pantry"
				  end
				  if(row[33].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " seguridad"
				  end
				  if(row[34].to_s )
				  	if(texto != "")
				  		texto += ", "
				  	end
				  	texto += " jardin"
				  end

				  if(texto != "")
				  	texto = "Características adicionales: " + texto
				  end

					logger.info "~~~------>"
				  logger.info row[26].to_s
				  logger.info "<=====~~~"


				  @advert.housetype = row[9].to_s
				  @advert.negotationtype = row[10].to_s
				  @advert.price = row[12].to_i
				  @advert.phone = row[17].to_s
				  @advert.areamts = row[22].to_s
				  @advert.areavrs = row[23].to_s
				  @advert.rooms = row[24].to_i
				  @advert.bathroom = row[25].to_i
				  @advert.description = row[35].to_s + " " + texto
				  @advert.adverttype = 3
				  @advert.fromexcel = true
				  @advert.latitude = lat
				  @advert.longitude = lng
				  @advert.ispublished = true
				  @advert.save
				else
					@advertsNoIncluded.push( "El Salvador, " + row[7].to_s + "," + row[6].to_s + "," + row[5].to_s + " " + row[4].to_s + " - Precio: " +  row[12].to_s + " - Telefono: " + row[16].to_s  )
				end

			else
				firstrow = false
			end

			#else
			#	@advertsNoIncluded.push( "El Salvador, " + row[7].to_s + "," + row[6].to_s + "," + row[5].to_s + " - Precio: " +  row[12].to_s + " - Telefono: " + row[16].to_s  )
			#end
			
		end
	  end


	  def deleteAdvert
	  	id = params[:id]
	  	Advert.find(id).destroy
	  	respond_to do |format|
	      format.html
	      format.json { render :json => { :result => true }}
	    end
	  end

	
end
















