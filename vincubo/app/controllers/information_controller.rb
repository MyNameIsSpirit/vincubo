class InformationController < ApplicationController

	config.log_level = :warn
	Rails.logger.level = 0

	def new
		if (session[:user] == nil )
	      	redirect_to :controller => "login", :action => "index"
	    else
	    	id = params[:id]
	    	@advert = Advert.find(id)
	    	@user = @advert.user
	    end
	end

	def create
	    @advert = Advert.find(1)
	    @user = @advert.user
	    #redirect_to :controller => "information", :action => "create"
	end

	private
  	def advert_params
  		params.require(:advert).permit(:id)
  	end

end
