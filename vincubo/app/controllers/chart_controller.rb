class ChartController < ApplicationController

	def index


		#Construir WHERE
		#where = "  created_at >= current_date - interval '7 days' "

		queries = []

		#Obtener total de propiedades dentro del filtro
		queries[0] = "select count(*) as total from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and ispublished = true"
		queries[1] = "select count(*) as total from adverts where housetype = 'Oficina' OR housetype = 'Oficina/Clinica' OR housetype = 'Local Comercial' OR housetype = 'alquiler temporario - vacaciones' and ispublished = true"
		queries[2] = "select count(*) as total from adverts where housetype = 'Terreno' and ispublished = true"
		queries[3] = "select count(*) as total from adverts where housetype = 'Nave Industrial' OR housetype = 'Bodega' and ispublished = true"
		queries[4] = "select count(*) as total from adverts where ispublished = true"

		queries[5] = "select count(*) as total from adverts where negotationtype = 'Venta' and ispublished = true"
		queries[6] = "select count(*) as total from adverts where negotationtype = 'Alquiler' and ispublished = true"

		queries[7] = "select zonename as zone, count(zone_id) as total from adverts inner join zones on zones.id = adverts.zone_id where ispublished = true group by zone_id, zonename order by total desc limit 4"

		#Grafico 1 y 2
		queries[8] = "select to_char(created_at,'Mon') as mon,
       	extract(year from created_at) as yyyy,
       	(select count(*) from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and negotationtype = 'Venta' and ispublished = true  ) as Casa,
       	(select count(*) from adverts where housetype = 'Terreno' and negotationtype = 'Venta' and ispublished = true  ) as Terreno,
       	(select count(*) from adverts where housetype = 'Local Comercial' OR housetype = 'Oficina/Clinina' and negotationtype = 'Venta' and ispublished = true  ) as Oficina,
		(select count(*) from adverts where housetype = 'Bodga' OR housetype = 'Nave Industrial' and negotationtype = 'Venta' and ispublished = true  ) as Bodega
		from adverts
		where negotationtype = 'Venta' and ispublished = true
		group by 1,2 order by 2, 1 limit 6;"
		
		queries[9] = "select to_char(created_at,'Mon') as mon,
       	extract(year from created_at) as yyyy,
       	(select count(*) from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and negotationtype = 'Alquiler' and ispublished = true  ) as Casa,
       	(select count(*) from adverts where housetype = 'Terreno' and negotationtype = 'Alquiler' and ispublished = true  ) as Terreno,
       	(select count(*) from adverts where housetype = 'Local Comercial' OR housetype = 'Oficina/Clinina' and negotationtype = 'Alquiler' and ispublished = true  ) as Oficina,
		(select count(*) from adverts where housetype = 'Bodga' OR housetype = 'Nave Industrial' and negotationtype = 'Alquiler' and ispublished = true  ) as Bodega
		from adverts
		where negotationtype = 'Alquiler' and ispublished = true
		group by 1,2 order by 2, 1 limit 6;"

		queries[10] = "select to_char(created_at,'Mon') as mon,
       	extract(year from created_at) as yyyy,
       	(select avg(price) from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and negotationtype = 'Venta' and ispublished = true  ) as Casa,
       	(select avg(price) from adverts where housetype = 'Terreno' and negotationtype = 'Venta' and ispublished = true  ) as Terreno,
       	(select avg(price) from adverts where housetype = 'Local Comercial' OR housetype = 'Oficina/Clinina' and negotationtype = 'Venta' and ispublished = true  ) as Oficina,
       	(select avg(price) from adverts where housetype = 'Bodga' OR housetype = 'Nave Industrial' and negotationtype = 'Venta' and ispublished = true  ) as Bodega
		from adverts
		where negotationtype = 'Venta' and ispublished = true
		group by 1,2 order by 2, 1 limit 6;"

		queries[11] ="select to_char(created_at,'Mon') as mon,
       	extract(year from created_at) as yyyy,
       	(select avg(price) from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and negotationtype = 'Alquiler' and ispublished = true  ) as Casa,
       	(select avg(price) from adverts where housetype = 'Terreno' and negotationtype = 'Alquiler' and ispublished = true  ) as Terreno,
       	(select avg(price) from adverts where housetype = 'Local Comercial' OR housetype = 'Oficina/Clinina' and negotationtype = 'Alquiler' and ispublished = true  ) as Oficina,
       	(select avg(price) from adverts where housetype = 'Bodga' OR housetype = 'Nave Industrial' and negotationtype = 'Alquiler' and ispublished = true  ) as Bodega
		from adverts
		where negotationtype = 'Alquiler' and ispublished = true
		group by 1,2 order by 2, 1 limit 6;"

		zone1 = (params[:zone1] == nil) ? "1" : params[:zone1].to_s
		zone2 = (params[:zone2] == nil) ? "1" : params[:zone2].to_s
		queries[12] = "select avg(price) as price, count(*) total, 2 as num from adverts where zone_id = " + zone1
		queries[13] = "select avg(price) as price, count(*) total, 2 as num from adverts where zone_id = " + zone2

		@query = []
		queries.each do |q|
			@query.push(ActiveRecord::Base.connection.execute(q))
		end

		@query[0] = (@query[0].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[1] = (@query[1].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[2] = (@query[2].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[3] = (@query[3].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)

		@query[5] = (@query[5].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[6] = (@query[6].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		

		#Fin de datos generales

		state = params[:state_id]
		town = params[:town_id]
		zone = params[:zone_id]

		if(state == nil || state == 'undefined' || state == "")
			state = 1
		end
		if(town == nil || town == 'undefined' || town == "")
			town = 1
		end
		if(zone == nil || zone == 'undefined' || zone == "")
			zone = Town.find(town)
		end


		@advert = Advert.new
		@states = State.all.order('name ASC')
		@state = State.find(state)
		@town = Town.find(town)
		@zone = Zone.find(zone)

		@zones = Zone.all.order('zonename ASC')

	end

	def mapa
	end

	def tabla

		queries = []

		#Obtener total de propiedades dentro del filtro
		queries[0] = "select count(*) as total from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and ispublished = true"
		queries[1] = "select count(*) as total from adverts where housetype = 'Oficina' OR housetype = 'Oficina/Clinica' OR housetype = 'Local Comercial' OR housetype = 'alquiler temporario - vacaciones' and ispublished = true"
		queries[2] = "select count(*) as total from adverts where housetype = 'Terreno' and ispublished = true"
		queries[3] = "select count(*) as total from adverts where housetype = 'Nave Industrial' OR housetype = 'Bodega' and ispublished = true"
		queries[4] = "select count(*) as total from adverts where ispublished = true"

		queries[5] = "select count(*) as total from adverts where negotationtype = 'Venta' and ispublished = true"
		queries[6] = "select count(*) as total from adverts where negotationtype = 'Alquiler' and ispublished = true"

		queries[7] = "select zonename as zone, count(zone_id) as total from adverts inner join zones on zones.id = adverts.zone_id where ispublished = true group by zone_id, zonename order by total desc limit 4"


		negotationtype = params[:negotationtype]
		state_id = params[:state_id]
		town_id = 0

		if( params[:town_id] != nil )
			town_id = Integer(params[:town_id])
		end
		townid = params[:town]
		price = params[:price]
		rooms = params[:rooms]
		bathrooms = params[:bathrooms]
		pricefrom = params[:pricefrom]
		priceto = params[:priceto]
		housetype = params[:housetype]
		page = params[:page]
		zone = params[:zone]
		orderby = params[:orderby]
		
		where = ""

		if(negotationtype == "Alquiler")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Alquiler' "
		else
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Venta' "
		end


		if( zone != nil && zone != "" && zone != "0" )
			if (where != "")
				where += " AND "
			end
			where += " zone_id = " + zone.to_s + " "
		elsif (town_id > 0)
			if (where != "")
				where += " AND "
			end
			where += " town_id = " + town_id.to_s + " "
		elsif(state_id != "" && state_id != nil)
			if (where != "")
				where += " AND "
			end
			where += " towns.state_id = '"+ state_id + "' "
		end
		

		if(rooms != nil && rooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " rooms <= " + rooms + " "
		end
		if(bathrooms != nil && bathrooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " bathroom <= " + bathrooms + " "
		end
		if(pricefrom != "" && pricefrom != nil)
			if (where != "")
				where += " AND "
			end
			where += " price >= " + pricefrom + " "
		end
		if(priceto != "" && priceto != nil)
			if (where != "")
				where += " AND "
			end
			where += " price <= " + priceto + " "
		end

		if(housetype != "" && housetype != nil)
			if (where != "")
				where += " AND "
			end
			where += " housetype = '" + housetype + "' "
		end

		orderCriteria = "price "
		

		where += " AND ispublished = true "
		where += " AND adverts.created_at >= current_date - interval '7 days'"
		#@total = Advert.joins(:town).where(where).count()
		@adverts = Advert.joins(:town).where(where)
		@total = @adverts.count
		
		if(page == nil)
			page =1;
		end
		@adverts = @adverts.order(orderCriteria +' DESC').paginate(:page => page, :per_page => 10)
		





		#AVG operation
		where = ""

		if(negotationtype == "Alquiler")
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Alquiler' "
		else
			if (where != "")
				where += " AND "
			end
			where += " negotationtype = 'Venta' "
		end


		if( zone != nil && zone != "" && zone != "0" )
			if (where != "")
				where += " AND "
			end
			where += " zone_id = " + zone.to_s + " "
		elsif (town_id > 0)
			if (where != "")
				where += " AND "
			end
			where += " town_id = " + town_id.to_s + " "
		elsif(state_id != "" && state_id != nil)
			if (where != "")
				where += " AND "
			end
			where += " towns.state_id = '"+ state_id + "' "
		end
		

		if(rooms != nil && rooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " rooms <= " + rooms + " "
		end
		if(bathrooms != nil && bathrooms != "undefined")
			if (where != "")
				where += " AND "
			end
			where += " bathroom <= " + bathrooms + " "
		end
		if(pricefrom != "" && pricefrom != nil)
			if (where != "")
				where += " AND "
			end
			where += " price >= " + pricefrom + " "
		end
		if(priceto != "" && priceto != nil)
			if (where != "")
				where += " AND "
			end
			where += " price <= " + priceto + " "
		end

		if(housetype != "" && housetype != nil)
			if (where != "")
				where += " AND "
			end
			where += " housetype = '" + housetype + "' "
		end

		orderCriteria = "price "
		

		where += " AND ispublished = true "
		where += " AND adverts.created_at >= current_date - interval '7 days'"
		q = "Select avg(price) as avg from adverts where " + where
		@qtotal = ActiveRecord::Base.connection.execute(q)
		#AVG ENDS


		@query = []
		queries.each do |q|
			@query.push(ActiveRecord::Base.connection.execute(q))
		end

		@query[0] = (@query[0].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[1] = (@query[1].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[2] = (@query[2].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[3] = (@query[3].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)

		@query[5] = (@query[5].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[6] = (@query[6].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		


		state = params[:state_id]
		town = params[:town_id]
		zone = params[:zone_id]

		@advert = Advert.new
		if(state == nil || state == 'undefined' || state == "")
			state = 1
		end
		if(town == nil || town == 'undefined' || town == "")
			town = 1
		end
		if(zone == nil || zone == 'undefined' || zone == "")
			zone = Town.find(town)
		end


		@advert = Advert.new
		@states = State.all.order('name ASC')
		@state = State.find(state)
		@town = Town.find(town)
		@zone = Zone.find(zone)
	end

	def grafica

		queries = []

		#Obtener total de propiedades dentro del filtro
		queries[0] = "select count(*) as total from adverts where housetype = 'Casa' OR housetype = 'Apartamento' and ispublished = true"
		queries[1] = "select count(*) as total from adverts where housetype = 'Oficina' OR housetype = 'Oficina/Clinica' OR housetype = 'Local Comercial' OR housetype = 'alquiler temporario - vacaciones' and ispublished = true"
		queries[2] = "select count(*) as total from adverts where housetype = 'Terreno' and ispublished = true"
		queries[3] = "select count(*) as total from adverts where housetype = 'Nave Industrial' OR housetype = 'Bodega' and ispublished = true"
		queries[4] = "select count(*) as total from adverts where ispublished = true"

		queries[5] = "select count(*) as total from adverts where negotationtype = 'Venta' and ispublished = true"
		queries[6] = "select count(*) as total from adverts where negotationtype = 'Alquiler' and ispublished = true"

		queries[7] = "select zonename as zone, count(zone_id) as total from adverts inner join zones on zones.id = adverts.zone_id where ispublished = true group by zone_id, zonename order by total desc limit 4"

		@query = []
		queries.each do |q|
			@query.push(ActiveRecord::Base.connection.execute(q))
		end

		@query[0] = (@query[0].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[1] = (@query[1].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[2] = (@query[2].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[3] = (@query[3].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)

		@query[5] = (@query[5].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		@query[6] = (@query[6].first["total"].to_i) * 100 / (@query[4].first["total"].to_i)
		


		state = params[:state_id]
		town = params[:town_id]
		zone = params[:zone_id]

		@advert = Advert.new
		if(state == nil || state == 'undefined' || state == "")
			state = 1
		end
		if(town == nil || town == 'undefined' || town == "")
			town = 1
		end
		if(zone == nil || zone == 'undefined' || zone == "")
			zone = Town.find(town)
		end


		@advert = Advert.new
		@states = State.all.order('name ASC')
		@state = State.find(state)
		@town = Town.find(town)
		@zone = Zone.find(zone)

	end
end
