class AdministrationController < ApplicationController

	config.log_level = :warn
	Rails.logger.level = 0

  layout "user_layout"
  #layout "public_layout", :only => [ :show ]

	def index
    logger.info "Processing the request index..."
    if (session[:user] == nil )
      redirect_to :controller => "login", :action => "index"
    else
      @user = User.find( cookies[:id] )

      type = params[:type]
      if(type != nil && type == "Alquiler")
        @adverts = @user.advert.where("negotationtype = 'Alquiler' AND ispublished = true")
      else
        @adverts = @user.advert.where("negotationtype = 'Venta'  AND ispublished = true")
      end
      #UserMailer.registration_confirmation(@user).deliver
      logger.info session[:user]
    end
	end

	def new
    if (session[:user] == nil )
      redirect_to :controller => "login", :action => "index"
    else
      logger.info "Processing the request New..."

      @user = User.find( cookies[:id] )
      if(@user.isclient)
        redirect_to :controller => "administration", :action => "spaces"
      end

      @advert = Advert.new

      6.times { 
        @advert.assets.build
      } 

      @photoadvert = Photoadvert.new
      
      @state = State.first
      @town = Town.first
      @zone = Zone.first

    end
	end
 
  def create
  		logger.info "Processing the request Create..."
      assets = asset_params
  		@user = User.find( cookies[:id] )

      @u = @user.advert
      @advert = @u.create(advert_params)

      if(assets != nil)
        assets.map do |assetParam|
          newAsset = Asset.new
          newAsset.asset = assetParam[1][:asset]
          @advert.assets << newAsset
        end
      end

      @advert.adverttype = 1
      @advert.ispublished = true;
      @advert.town = Town.find( town_params[:id] )
      @advert.zone = Zone.find( zone_params[:id] ) if params[:zone]
      @advert.fromexcel = false
      @advert.originaldate = Time.now 

      zonename = ""
      if(@advert.zone != nil)
        zonename = Zone.find( zone_params[:id] ).zonename
      end

      #location = "El Salvador " + Town.find( town_params[:id] ).state.name  + " " + Town.find( town_params[:id] ).name + " " + zonename
      #lo = Geocoder.search(location)

      #if(lo != nil && lo.count > 0 && lo[0].data != nil && lo[0].data["geometry"] != nil && lo[0].data["geometry"]["bounds"] != nil )
      #  @advert.latitude = lat
      #  @advert.longitude = lng
      #end


      @advert.save

  		redirect_to action: "spaces", controller: "administration"
  end

  def edit
    if (session[:user] == nil )
      redirect_to :controller => "login", :action => "index"
    else
      @advert = Advert.find(params[:id])

      6.times { 
        @advert.assets.build
      } 
      @user = User.find( cookies[:id] )
      @photoadvert = Photoadvert.new
      @state = State.first
      @town = Town.first

      render 'edit'
    end
	end

	def update
  		@advert = Advert.find(params[:id])
      assets = asset_params
      deleted_assets=delete_asset_params

      @advert.update(youtube1: advert_params[:youtube1])
      @advert.update(address: advert_params[:address])
      @advert.update(local: advert_params[:address])
      @advert.update(description: advert_params[:description])
      @advert.update(housetype: advert_params[:housetype])
      @advert.update(negotationtype: advert_params[:negotationtype])
      @advert.update(price: advert_params[:price])
      @advert.update(rooms: advert_params[:rooms])
      @advert.update(bathroom: advert_params[:bathroom])
      @advert.update(areamts: advert_params[:areamts])
      @advert.update(areavrs: advert_params[:areavrs])
      @advert.update(street: advert_params[:street])



#deleting old images

      if(deleted_assets != nil)
        deleted_assets.keys.map do |deleted_asset_key|
          asset_element = Integer(deleted_asset_key)
          if(deleted_assets[deleted_asset_key] == "true")
            @advert.assets[asset_element].destroy
          end
        end
      end

      if(assets != nil)
        assets.map do |assetParam|
          newAsset = Asset.new
          asset_number = Integer(assetParam[0])
          newAsset.asset = assetParam[1][:asset]
          if(@advert.assets[asset_number] && @advert.assets[asset_number].asset.exists?)
            @advert.assets[asset_number].asset = newAsset.asset
          else
            @advert.assets << newAsset
          end

        end
      end

      

  		if @advert.save
    		redirect_to action: "index"
  		else
    		render 'edit'
  		end
	end

  def spaces
    if (session[:user] == nil )
      redirect_to :controller => "login", :action => "index"
    else
      @user = User.find( cookies[:id] )
      @adverts = @user.advert.where(:ispublished => true)
      @advertsHistory = @user.advert.where(:ispublished => false)
    end
  end

  def show
    @advert = Advert.find(params[:id])
    @user = @advert.user
    if(cookies[:id] != nil)
      @loggeduser = User.find( cookies[:id] )
    else
      @loggeduser = nil
    end
  end

  def load
    id = params[:id]
    @towns = State.find(id).towns.order('name ASC')

    respond_to do |format|
      format.html # contact.html.erb
      format.json { render :json => { :result => @towns.order('name') }}
    end
  end

  def checkIfEmailExist
    email = params[:email]
    @result = User.where("email = '" + email + "'").count()
    respond_to do |format|
      format.html
      format.json { render :json => { :result => @result }}
    end
  end

  def login
    email = params[:email]
    password = params[:password]
    @result = User.where("email = '" + email + "' AND password = '" + password + "' ").count()
    respond_to do |format|
      format.html
      format.json { render :json => { :result => @result }}
    end
  end


  def editBasicInfo
    name = params[:name]
    lastname = params[:lastname]
    job = params[:job]

    @user = User.find( cookies[:id] )
    User.update(@user.id, :name => name, :lastname => lastname, :job => job)
    respond_to do |format|
      format.html
      format.json { render :json => { :result => true }}
    end
  end

  def editAbout
    @user = User.find( cookies[:id] )
    about = params[:about]
    User.update(@user.id, :about => about)
    respond_to do |format|
      format.html
      format.json { render :json => { :result => true }}
    end
  end

  def editReference
    @user = User.find( cookies[:id] )
    reference = params[:reference]
    User.update(@user.id, :reference => reference)
    respond_to do |format|
      format.html
      format.json { render :json => { :result => true }}
    end
  end

  def updatePhotos
    @user = User.find( cookies[:id] )
    @user.image = image_params[:image]
    @user.save
    redirect_to :controller => "administration", :action => "index"
  end

  


	private
  	def advert_params
  		params.require(:advert).permit(:title, :features, :description, :areadescription, :rooms, :address, :local, :aditionaladdress, :negotationtype, :bathroom, :price, :housetype, :subtype, :source, :originaldate, :image_file_name, :image_content_type, :image_file_size, :image_file_updated_at, :area, :availability, :parking, :construction, :areasize, :areamts, :areavrs, :furnished, :pantry , :security , :garden , :nearbymalls , :kitchen , :preservation , :pool , :jacuzzi , :level , :basicservice , :cistern, :water , :heater , :air , :telephoneline  , :visitorparking , :mezzanine , :recreation , :study , :terrace , :cellar , :parkland , :image , :town, :latitude, :longitude, :youtube1, :youtube2, :assets, :street)
  	end

    def state_params
      params.require(:state).permit(:id, :name)
    end

    def town_params
      params.require(:town).permit(:id, :name)
    end

    def zone_params
      params.require(:zone).permit(:id, :name) if params[:zone]
    end

    def photoadvert_params
      params.require(:photoadvert).permit(:comment, :image)
    end

    def asset_params
      params.require(:assets).permit! if params[:assets]
    end
    def delete_asset_params
      params.require(:delete_asset).permit! if params[:delete_asset]
    end

    def image_params
      params.require(:user).permit(:image)
    end

end
