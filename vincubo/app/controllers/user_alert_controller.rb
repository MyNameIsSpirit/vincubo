#encoding: utf-8
class UserAlertController < ApplicationController

  config.log_level = :warn
  Rails.logger.level = 0
  skip_before_filter :verify_authenticity_token, :only => [:create,:destroy]


  def create

    logger.info "Generarndo componentes =)"

    user_alert = UserAlert.new
    conditions = []
    if params[:town_id] && params[:town_id] != ''
      name = 'Ciudad: '+ Town.find(params[:town_id]).name
      conditions.push AlertCondition.new(:name => name,:condition => ' town_id = ? ',:value => params[:town_id] )
    end
    if params[:negotationtype] && params[:negotationtype] != ''
      name = 'Tipo: '+params[:negotationtype]
      conditions.push AlertCondition.new(:name => name,:condition => ' negotationtype = ? ',:value => params[:negotationtype] )
    end
    if params[:state_id] && params[:state_id] != ''
      name = 'Municipio: '+State.find(params[:state_id]).name
      conditions.push AlertCondition.new(:name => name,:condition => ' towns.state_id = ? ',:value => params[:state_id] )
    end
    if params[:price] && params[:price] != ''
      name = 'Precio: '+params[:price]
      conditions.push AlertCondition.new(:name => name,:condition => ' price = ? ',:value => params[:price] )
    end
    if params[:rooms] && params[:rooms] != ''
      name = 'Habitaciones: '+params[:rooms]
      conditions.push AlertCondition.new(:name => name,:condition => ' rooms >= ? ',:value => params[:rooms] )
    end
    if params[:bathrooms] && params[:bathrooms] != ''
      name = 'Baños: '+params[:bathrooms]
      conditions.push AlertCondition.new(:name => name,:condition => ' bathroom >= ? ',:value => params[:bathrooms] )
    end
    if params[:pricefrom] && params[:pricefrom] != ''
      name = 'Precio minimo: '+params[:pricefrom]
      conditions.push AlertCondition.new(:name => name, :condition => ' price >= ? ',:value => params[:pricefrom] )
    end
    if params[:priceto] && params[:priceto] != ''
      name = 'Precio maximo: '+params[:priceto]
      conditions.push AlertCondition.new(:name => name,:condition => ' price <= ? ',:value => params[:priceto] )
    end
    if params[:housetype] && params[:housetype] != ''
      name = 'Tipo de Espacio:' +params[:housetype]
      conditions.push AlertCondition.new(:name => name,:condition => ' housetype = ? ',:value => params[:housetype] )
    end
    if params[:fromAreamts] && params[:fromAreamts] != ''
      conditions.push AlertCondition.new(:name => 'Minimo de area construida: '+params[:fromAreamts],:condition => ' areamts >= ? ',:value => params[:fromAreamts] )
    end
    if params[:toAreamts] && params[:toAreamts] != ''
      conditions.push AlertCondition.new(:name => 'Maximo de area construida: '+params[:toAreamts],:condition => ' areamts <= ? ',:value => params[:toAreamts] )
    end
    if params[:fromAreavar] && params[:fromAreavar] != ''
      conditions.push AlertCondition.new(:name => 'Minimo terreno: '+params[:fromAreavar],:condition => ' areavrs >= ? ',:value => params[:fromAreavar] )
    end
    if params[:toAreavar] && params[:toAreavar] != ''
      conditions.push AlertCondition.new(:name => 'Maximo terreno: '+params[:toAreavar],:condition => ' areavrs <= ? ',:value => params[:toAreavar] )
    end
    #zona
    if (session[:user] == nil )
      redirect_to :controller => "login", :action => "index"
    else
      @user = User.find( cookies[:id] )
      user_alert.alert_conditions.concat(conditions)
      user_alert.user = @user
      user_alert.save
    end
    #redirect_to  controller: 'advert' , status: 302 , params: params
    respond_to do |format|
      format.html
      format.json { render :json => { :result => true }}
    end
  end
  def new

  end
  def edit

  end
  def update

  end
  def delete

  end
  def destroy
    UserAlert.delete(params[:idUserAlert])
    respond_to do |format|
      format.html
      format.json { render :json => { :result => true }}
    end
  end
end
