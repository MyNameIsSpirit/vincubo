class UserAlert < ActiveRecord::Base
  belongs_to :user
  has_many :alert_conditions

  def adverts
    where = build_where
    Advert.joins(:town).where(where[0],*where[1]).where(:ispublished => true)
  end
  def UserAlert.deliver_updates()
    user_alerts = UserAlert.all
    user_alerts.each do |ua|
      adverts = ua.adverts
      if adverts.count >0
        puts 'Delivering: ' + ua.user.email
        AlertMailer.mail_newsletter(ua.user,adverts.take(5)).deliver
      end
    end
  end
  #def UserAlert.exists

  #end


  def build_where
    where = ''
    parameters = []
    alert_conditions.each_with_index do |ac,index|
      where +=  ' AND ' unless index ==0
      where += ac.condition
      parameters.append ac.value if ac.value
      parameters.append ac.secondary_value if ac.secondary_value
    end
    [where,parameters]
  end
end
