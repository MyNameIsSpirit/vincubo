class Asset < ActiveRecord::Base
	belongs_to :advert
	has_attached_file :asset, :styles => { :large => "640x480", :medium => "300x300", :thumb => "100x100"}, :default_url => '/images/logo.png'
  validates_attachment :asset, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
end
