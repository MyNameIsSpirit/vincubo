class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :original_message, :class_name => "Message"

  #scope :sent, where(:sent => true)
  #scope :received, where(:sent => false)

  def send_message(from, recipients)
    if(!recipients.kind_of?(Array))
      recipients = [recipients]
    end
    recipients.each do |recipient|
      msg =  Message.new
      msg.subject = self.subject
      msg.body = self.body
      msg.sent = false
      msg.user = recipient
      msg.original_message = self
      msg.save
    end
    self.update_attributes :user => from, :sent => true
  end

end

