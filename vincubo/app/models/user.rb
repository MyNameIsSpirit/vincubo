class User < ActiveRecord::Base
	has_attached_file :image
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/gif", "image/png"], :styles => {:large => "600x400", :medium => "300x300>", :thumb => "200x100>" }, :default_url => "/images/:style/missing.png"

	has_many :advert
	has_many :notification
  has_many :favorites
	has_many :favorite_adverts, through: :favorites, source: :advert
  has_many :messages
  has_many :user_alerts


end
