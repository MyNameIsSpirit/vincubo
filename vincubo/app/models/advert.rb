class Advert < ActiveRecord::Base
	has_attached_file :image
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/gif", "image/png"], :styles => {:large => "600x400", :medium => "300x300>", :thumb => "200x100>" }, :default_url => "/images/:style/missing.png"

	  belongs_to :user
    has_many :photoadvert
  	has_many :favorites
  	has_many :favorited_by, through: :favorites, source: :user
  	belongs_to :town
    belongs_to :zone

    #attr_accessible :assets
    has_many :assets
    accepts_nested_attributes_for :assets
  	#accepts_nested_attributes_for :town

  def Advert.get_random(number)
    count = Advert.where(:ispublished => true).count
    if (number>count)
      number = count

    end
    Advert.where(:ispublished => true).offset(rand(count-number)).sample(number)
  end
  def short_description
    desc="" + housetype + " | " + rooms.to_s + " Habitaciones"
  end
end
