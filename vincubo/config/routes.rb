Rails.application.routes.draw do

  #resources :administration

  get 'welcome/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'
  get 'welcome/aboutus' => 'welcome#aboutus'
  get 'welcome/faq' => 'welcome#faq'

  get 'useralert/create' => 'user_alert#create'
  post 'useralert/create' => 'user_alert#create'
  patch 'useralert/create' => 'user_alert#create'
  post 'useralert/destroy' => 'user_alert#destroy'


  get 'administration' => 'administration#index'
  get 'administration/new' => 'administration#new'
  post 'administration/new' => 'administration#create'
  patch 'administration/new' => 'administration#create'
  get 'administration/:id/edit' => 'administration#edit'
  patch 'administration/:id/update' => 'administration#update'
  post 'administration/:id/update' => 'administration#update'
  get 'administration/spaces' => 'administration#spaces'
  get 'administration/load/:id' => 'administration#load'
  get 'administration/checkIfEmailExist' => 'administration#checkIfEmailExist'
  get 'administration/login' => 'administration#login'
  get 'administration/editBasicInfo' => 'administration#editBasicInfo'
  get 'administration/editAbout'=> 'administration#editAbout'
  get 'administration/editReference' => 'administration#editReference'
  patch 'administration/updatePhotos' => 'administration#updatePhotos'
  get 'administration/:id' => 'administration#show'

  get 'profile' => 'profile#index'
  get 'profile/new' => 'profile#new'
  post 'profile/new' => 'profile#create'
  get 'profile/edit' => 'profile#edit'
  patch 'profile/update' => 'profile#update'
  post 'profile/update' => 'profile#update'
  get 'profile/registerUser' => 'profile#registerUser'
  get 'profile/fbRegister' => 'profile#fbRegister'
  get 'profile/:id' => 'profile#show'
  post 'profile/register' => 'profile#register'

  get 'login/confirm_password_reset' => 'login#confirm_password_reset'
  post 'login/reset_password' => 'login#reset_password'
  get 'login/send_confirmation' => 'login#send_confirmation'
  get 'login' => 'login#index'
  post 'login/login' => 'login#login'
  get 'login/logout' => 'login#logout'
  get 'login/dialog' => 'login#dialog'
  get 'login/startlogin' => 'login#startlogin'

  get 'favorites' => 'favorites#index'
  get 'favorites/new' => 'favorites#new'
  get 'favorites/destroy' => 'favorites#destroy'

  get 'information/new/:id' => 'information#new'
  patch '/information/create' => 'information#create'

  get 'advert' => 'advert#index'
  patch 'advert' => 'advert#index'
  get 'advert/map' => 'advert#map'
  get 'advert/load' => 'advert#load'
  get 'advert/info' => 'advert#info'
  get 'advert/custom' => 'advert#custom'
  get 'detail/denounce' => 'detail#denounce'

  get 'detail' => 'detail#index'
  get 'detail/detail/:id' => 'detail#detail'
  get 'detail/list' => 'detail#list'
  get 'detail/unpublish/:id' => 'detail#unpublish'
  get 'detail/remove/:id' => 'detail#remove'
  get 'detail/report/:id' => 'detail#report'
  get 'detail/notification' => 'detail#notification'
  get 'detail/zonesbystate' => 'detail#zonesbystate'
  get 'detail/zonesbytown' => 'detail#zonesbytown'
  
  get 'panel' => 'panel#index'
  post 'panel/publish/' => 'panel#publish'
  patch 'panel/publish/' => 'panel#publish'
  get 'panel/chart' => 'panel#chart'
  post 'panel/upload' => 'panel#upload'
  get 'panel/read' => 'panel#read'
  get 'panel/deleteAdvert' => 'panel#deleteAdvert'

  get 'manage' => 'manage#index'
  get 'manage/edit/:id' => 'manage#edit'
  post 'manage/update' => 'manage#update'
  get '/manage/delete/:id' => 'manage#delete'
  get '/manage/users' => 'manage#users'
  get '/manage/report' => 'manage#report'
  get 'manage/editprofile'  => 'manage#editprofile'
  post 'manage/updateprofile' => 'manage#updateprofile'
  get '/manage/zones' => 'manage#zones'
  get '/manage/addzone' => 'manage#addzone'

  get 'chart' => 'chart#index'
  get 'chart/mapa' => 'chart#mapa'
  get 'chart/tabla' => 'chart#tabla'
  get 'chart/grafica' => 'chart#grafica'

  get 'admin' => 'admin#index'

  resources :uploads

  #get 'information/create' => 'information#create'
  
  #edit
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
